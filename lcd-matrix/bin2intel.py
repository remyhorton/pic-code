#!/usr/bin/env python3
#  bin2intel.py - Utility to convert binary file into Intel hex format
#  Takes input as filename and output is printed to stdout. Resultant
#  output can then be loaded  into gpsim.
#
#  Copyright (C) Remy Horton, 2023.
#  SPDX-License-Identifier: BSD-3-Clause

import sys

def calcCheck(listValues):
    return (((sum(listValues) & 0xff) ^ 0xff) + 1) & 0xff

fpInput = open(sys.argv[1],'rb')
dataInput = fpInput.read()
fpInput.close()

for offset in range(0, len(dataInput), 0x20):
    chunk = dataInput[offset:offset+0x20]
    lenChunk = len(chunk)
    record = [lenChunk]
    record.append( (offset & 0xff00) >> 8 )
    record.append( offset & 0xff )
    record.append( 0x00 )
    record.extend( chunk )
    record.append( calcCheck(record) )
    print(':', end='')
    for value in record:
        print("{0:02x}".format(value), end='')
    print()
print(":00000001FF")

