;   setup.asm - Initialisation code
;   Copyright (C) Remy Horton, 2023.
;   SPDX-License-Identifier: BSD-3-Clause

setupClock:
    BANKSEL OSCCON
    MOVLW   b'01111010'     ; 16MHz
;    MOVLW   b'01101010'     ; 4MHz
    MOVWF   OSCCON
    ; Timer1
    BANKSEL T1CON
    MOVLW   b'00110001'
    MOVWF   T1CON
    RETURN

setupPins:
    MOVLW   b'00000011'
    BANKSEL TRISC
    MOVWF   TRISC
    BANKSEL ANSELA
    CLRF    ANSELA
    BANKSEL ANSELC
    CLRF    ANSELC
    RETURN

setupI2C:
    MOVLW   3
    BANKSEL SSP1ADD
    MOVWF   SSP1ADD
    MOVLW   b'00101000'
    MOVWF   SSP1CON1
    RETURN
