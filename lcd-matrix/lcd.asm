;   lcd.asm - SSD0232 LCD display routines
;
;   Very much a case of getting something working
;   rather than using the best method. Suspect many
;   of the commands are not implemented in gpsim.
;
;   Copyright (C) Remy Horton, 2019,2020,2023.
;   SPDX-License-Identifier: BSD-3-Clause

#define REG_E       5
#define REG_DC      4
#define REG_CS      3
#define REG_RES     2

pulseCmd:
    MOVWF   temp
    CALL    i2cWaitIdle
    CALL    i2cStartWait
    MOVLW   0x4e
    CALL    i2cSendWait
    MOVFW   temp
    CALL    i2cSendWait
    CALL    i2cStopWait
    BCF     PORTC,REG_E
    BSF     PORTC,REG_E
    RETURN

drawsetup:
    BANKSEL PORTC
    CLRF    PORTC
    ; Enable is falling-edge
    BSF     PORTC,REG_E
; Do reset
    BSF     PORTC,REG_RES
    BCF     PORTC,REG_RES
    BSF     PORTC,REG_RES
; Clear screen
    MOVLW   17
    MOVWF   count
    BSF     PORTC,REG_DC
    MOVLW   0xff
    MOVWF   count2
    MOVLW   0
    CALL    pulseCmd
    DECFSZ  count2,1
    GOTO    $-2
    DECFSZ  count,1
    GOTO    $-7
    BCF     PORTC,REG_DC
    CALL    setScreen
; Set horizontal LCD pixel increment
    MOVLW   0xa0
    CALL    pulseCmd
    MOVLW   b'00000100'
    CALL    pulseCmd
    RETURN

setScreen:
; Limit columns to 0..59 (60 total)
    MOVLW   0x15
    CALL    pulseCmd
    MOVLW   0x0
    CALL    pulseCmd
    MOVLW   60
    CALL    pulseCmd
; Limit rows to 14
    MOVLW   0x75
    CALL    pulseCmd
    MOVLW   0x0
    CALL    pulseCmd
    MOVLW   0x0+13
    CALL    pulseCmd
    RETURN

drawpixels:
    BANKSEL PORTC
    BCF     PORTC,REG_DC
    CALL    setScreen
    BSF     PORTC,REG_DC
    MOVLW   buffer
    MOVWF   FSR0L
    MOVLW   60
    MOVWF   count

drawloop:
    MOVLW   0x00
    BTFSC   INDF0,0
    MOVLW   0xf0
    CALL    pulseCmd
    MOVLW   0x00
    CALL    pulseCmd
    MOVLW   0x00
    BTFSC   INDF0,1
    MOVLW   0xf0
    CALL    pulseCmd
    MOVLW   0x00
    CALL    pulseCmd
    MOVLW   0x00
    BTFSC   INDF0,2
    MOVLW   0xf0
    CALL    pulseCmd
    MOVLW   0x00
    CALL    pulseCmd
    MOVLW   0x00
    BTFSC   INDF0,3
    MOVLW   0xf0
    CALL    pulseCmd
    MOVLW   0x00
    CALL    pulseCmd
    MOVLW   0x00
    BTFSC   INDF0,4
    MOVLW   0xf0
    CALL    pulseCmd
    MOVLW   0x00
    CALL    pulseCmd
    MOVLW   0x00
    BTFSC   INDF0,5
    MOVLW   0xf0
    CALL    pulseCmd
    MOVLW   0x00
    CALL    pulseCmd
    MOVLW   0x00
    BTFSC   INDF0,6
    MOVLW   0xf0
    CALL    pulseCmd
    MOVLW   0x00
    CALL    pulseCmd
    INCF    FSR0L
    DECFSZ  count,1
    GOTO    drawloop
    RETURN

