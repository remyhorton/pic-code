;   ssd0232.asm - SS0323 Graphical LCD control
;   Copyright (C) Remy Horton, 2023.
;   SPDX-License-Identifier: BSD-3-Clause

PROCESSOR 16f1823
#include <p16f1823.inc>
    RADIX dec
    CONFIG FOSC=INTOSC
    CONFIG WDTE=OFF
    CONFIG MCLRE=ON
    CONFIG LVP=OFF
    CONFIG PWRTE=OFF, CP=OFF
    CONFIG BOREN=OFF
    CONFIG CLKOUTEN=OFF
    CONFIG IESO=OFF
    CONFIG FCMEN=OFF
    CONFIG WRT=OFF
    CONFIG STVREN=ON, BORV=LO

#define INST_DATA   0
#define INST_SHDN   1
#define INST_SHUP   2
#define INST_WAIT   3
#define INST_LOAD   4
#define INST_DONE   5
#define INST_HALT   6


BANK0DATA       UDATA   0x20
buffer          RES     59
rightcolumn     RES     1

SHAREDDATA      UDATA   0x70
temp            RES     1
count           RES     1
count2          RES     1
eepromaddr      RES     1
instruction     RES     1
opcode          RES     1
param           RES     1

; Entrypoints
STARTUP     CODE    0x0000
    CALL    setupClock
    CALL    setupPins
    CALL    setupI2C
    GOTO    begin
INTERRUPT   CODE    0x0004
    BANKSEL IOCAF
    BCF     IOCAF,5
    RETFIE
MAINCODE    CODE    0x0008

#include "setup.asm"
#include "i2c.asm"
#include "lcd.asm"

begin:
    CALL    drawsetup
; Fill buffer
    MOVLW   60
    MOVWF   count
    CLRF    FSR0H
    MOVLW   buffer
    MOVWF   FSR0L
    MOVLW   0x60
    MOVLW   0x00
    MOVWF   INDF0
    INCF    FSR0L,1
    DECFSZ  count,1
    GOTO    $-5
; EEPROM setup
    CLRF    eepromaddr
    CALL    drawpixels

mainloop:
; Reset Timer1
    BANKSEL TMR1H
    CLRF    TMR1H
    CLRF    TMR1L
    BANKSEL PIR1
    BCF     PIR1,0
; Wait on Timer1
    BANKSEL PIR1
    BTFSS   PIR1,0
    GOTO    $-1
    CALL    fetcheeprom
    CALL    decodeinst
mainloop_bypass:
    BTFSC   opcode,INST_DATA
    GOTO    exec_data
    BTFSC   opcode,INST_SHDN
    GOTO    exec_shdn
    BTFSC   opcode,INST_SHUP
    GOTO    exec_shup
    BTFSC   opcode,INST_LOAD
    GOTO    exec_load
    BTFSC   opcode,INST_WAIT
    GOTO    exec_wait
    BTFSC   opcode,INST_HALT
    GOTO    $
finishedinst:
    CALL    drawpixels
    GOTO    mainloop

exec_data:
    CALL    shiftleft
    BANKSEL rightcolumn
    MOVFW   param
    MOVWF   rightcolumn
    GOTO    finishedinst

exec_shdn:
    CALL    shiftdown
    MOVLW   buffer
    MOVWF   FSR0L
exec_shdn_more:
    BANKSEL param
    BTFSC   param,0
    BSF     INDF0,0
    INCF    FSR0L
    BTFSC   param,1
    BSF     INDF0,0
    INCF    FSR0L
    BTFSC   param,2
    BSF     INDF0,0
    INCF    FSR0L
    BTFSC   param,3
    BSF     INDF0,0
    INCF    FSR0L
    BTFSC   param,4
    BSF     INDF0,0
    INCF    FSR0L
    CALL    fetcheeprom
    CALL    decodeinst
    ; If next instruction not ShDn, break out
    BTFSC   opcode,INST_SHDN
    GOTO    exec_shdn_more
    CALL    drawpixels
    GOTO    mainloop_bypass
    
exec_shup:
    CALL    shiftup
    MOVLW   buffer
    MOVWF   FSR0L
exec_shup_more:
    BANKSEL param
    BTFSC   param,0
    BSF     INDF0,6
    INCF    FSR0L
    BTFSC   param,1
    BSF     INDF0,6
    INCF    FSR0L
    BTFSC   param,2
    BSF     INDF0,6
    INCF    FSR0L
    BTFSC   param,3
    BSF     INDF0,6
    INCF    FSR0L
    BTFSC   param,4
    BSF     INDF0,6
    INCF    FSR0L
    CALL    fetcheeprom
    CALL    decodeinst
    ; If next instruction not ShUp, break out
    BTFSC   opcode,INST_SHUP
    GOTO    exec_shup_more
    CALL    drawpixels
    GOTO    mainloop_bypass

exec_load:
    CALL    fetcheeprom
    CALL    decodeinst
    BTFSC   opcode,INST_DONE
    GOTO    mainloop
    CALL    shiftleft
    BANKSEL rightcolumn
    MOVFW   param
    MOVWF   rightcolumn
    GOTO    finishedinst

exec_wait:
    BANKSEL TMR1H
    CLRF    TMR1H
    CLRF    TMR1L
    BANKSEL PIR1
    BCF     PIR1,0
    BANKSEL PIR1
    BTFSS   PIR1,0
    GOTO    $-1
    DECFSZ  param
    GOTO    exec_wait
    GOTO    finishedinst

decodeinst:
    CLRF    opcode
    CLRF    param
    ; Data
    BTFSC   instruction,7
    GOTO    $+5
    BSF     opcode,INST_DATA
    MOVFW   instruction
    MOVWF   param
    RETURN
    ; Shift Down
    MOVLW   b'11100000'
    ANDWF   instruction,0
    SUBLW   b'10000000'
    BTFSS   STATUS,2
    GOTO    $+7
    BSF     opcode,INST_SHDN
    MOVFW   instruction
    MOVWF   param
    MOVLW   b'00011111'
    ANDWF   param,1
    RETURN
    ; Shift Up
    MOVLW   b'11100000'
    ANDWF   instruction,0
    SUBLW   b'10100000'
    BTFSS   STATUS,2
    GOTO    $+7
    BSF     opcode,INST_SHUP
    MOVFW   instruction
    MOVWF   param
    MOVLW   b'00011111'
    ANDWF   param,1
    RETURN
    ; Wait
    MOVLW   b'11100000'
    ANDWF   instruction,0
    SUBLW   b'11000000'
    BTFSS   STATUS,2
    GOTO    $+7
    BSF     opcode,INST_WAIT
    MOVFW   instruction
    MOVWF   param
    MOVLW   b'00011111'
    ANDWF   param,1
    RETURN
    ; Load
    MOVFW   instruction
    SUBLW   b'11111101'
    BTFSS   STATUS,2
    GOTO    $+3
    BSF     opcode,INST_LOAD
    RETURN
    ; Done
    MOVFW   instruction
    SUBLW   b'11111110'
    BTFSS   STATUS,2
    GOTO    $+3
    BSF     opcode,INST_DONE
    RETURN
    ; Halt
    MOVFW   instruction
    SUBLW   b'11111111'
    BTFSS   STATUS,2
    GOTO    $+3
    BSF     opcode,INST_HALT
    RETURN
    ; Bad instruction.
    RETURN

fetcheeprom:
    BANKSEL EEADRL
    MOVFW   eepromaddr
    MOVWF   EEADRL
    BCF     EECON1,6
    BCF     EECON1,7
    BSF     EECON1,0
    MOVFW   EEDATL
    INCF    eepromaddr,1
    MOVWF   instruction
    RETURN


shiftleft:
    MOVLW   59
    MOVWF   count
    CLRF    FSR0H
    CLRF    FSR1H
    MOVLW   buffer
    MOVWF   FSR0L
    MOVWF   FSR1L
    INCF    FSR1L
    MOVFW   INDF1
    MOVWF   INDF0
    INCF    FSR0L,1
    INCF    FSR1L,1
    DECFSZ  count,1
    GOTO    $-5
    CLRF    INDF0
    RETURN

shiftdown:
    MOVLW   60
    MOVWF   count
    CLRF    FSR0H
    MOVLW   buffer
    MOVWF   FSR0L
    LSLF    INDF0,1
    BCF     INDF0,7
    INCF    FSR0L
    DECFSZ  count,1
    GOTO    $-4
    RETURN

shiftup:
    MOVLW   60
    MOVWF   count
    CLRF    FSR0H
    MOVLW   buffer
    MOVWF   FSR0L
    LSRF    INDF0,1
    INCF    FSR0L
    DECFSZ  count,1
    GOTO    $-3
    RETURN

    END

