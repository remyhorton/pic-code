#define NO_BIT_DEFINES
#include "pic16f88.h"


/* Microcontroller configuration
    _FOSC_INTOSCIO  -> Completely internal clocking
    _WDT_OFF        -> No watchdog timer
    _MCLR_OFF       -> Pull-down on Pin 4 (RA5) not required
    _LVP_OFF        -> Allows Pin 9 (RB3) to be used for I/O
*/
__code short __at (_CONFIG1) cfg0 =
	_FOSC_INTOSCIO & _WDT_OFF & _MCLR_OFF & _LVP_OFF
    & _PWRTE_OFF & _CP_OFF & _CPD_OFF;


/* Segment bit-masks */
#define SEG_A   0x01
#define SEG_M   0x02
#define SEG_K   0x04
#define SEG_H   0x08
#define SEG_U   0x10
#define SEG_S   0x20
#define SEG_T   0x40
#define SEG_G   0x80
#define SEG_F   0x01
#define SEG_E   0x02
#define SEG_B   0x04
#define SEG_D   0x08
#define SEG_R   0x10
#define SEG_P   0x20
#define SEG_C   0x40
#define SEG_N   0x80


/* Segment lighting tables */
const unsigned char LoBitsNum[10] = {
    SEG_A|SEG_H|SEG_T|SEG_G,
    0,
    SEG_A|SEG_U|SEG_G,
    SEG_A|SEG_U,
    SEG_H|SEG_U,
    SEG_A|SEG_H|SEG_U,
    SEG_A|SEG_H|SEG_U|SEG_G,
    SEG_A,
    SEG_A|SEG_H|SEG_U|SEG_G,
    SEG_A|SEG_H|SEG_U
};
const unsigned char HiBitsNum[10] = {
    SEG_F|SEG_E|SEG_D|SEG_C|SEG_N|SEG_B,
    SEG_D|SEG_C,
    SEG_F|SEG_E|SEG_P|SEG_C|SEG_B,
    SEG_F|SEG_E|SEG_D|SEG_P|SEG_C|SEG_B,
    SEG_D|SEG_P|SEG_C,
    SEG_F|SEG_E|SEG_R|SEG_B,
    SEG_F|SEG_E|SEG_D|SEG_P|SEG_B,
    SEG_D|SEG_C|SEG_B,
    SEG_F|SEG_E|SEG_D|SEG_P|SEG_C|SEG_B,
    SEG_F|SEG_E|SEG_D|SEG_P|SEG_C|SEG_B
};
const unsigned char LoBitsLet[26] = {
    SEG_A|SEG_H|SEG_G|SEG_U,
    SEG_A|SEG_M|SEG_S,
    SEG_A|SEG_H|SEG_G,
    SEG_A|SEG_M|SEG_S,
    SEG_A|SEG_H|SEG_U|SEG_G,
    SEG_A|SEG_H|SEG_U|SEG_G,
    SEG_A|SEG_H|SEG_G,
    SEG_H|SEG_U|SEG_G,
    SEG_A|SEG_M|SEG_S,
    SEG_G,
    SEG_H|SEG_U|SEG_G,
    SEG_H|SEG_G,
    SEG_K|SEG_H|SEG_G,
    SEG_K|SEG_K|SEG_G,
    SEG_A|SEG_H|SEG_G,
    SEG_A|SEG_H|SEG_U|SEG_G,
    SEG_A|SEG_H|SEG_G,
    SEG_A|SEG_H|SEG_U|SEG_G,
    SEG_A|SEG_H|SEG_U,
    SEG_A|SEG_M|SEG_S,
    SEG_H|SEG_G,
    SEG_H|SEG_T|SEG_G,
    SEG_H|SEG_T|SEG_G,
    SEG_K|SEG_T,
    SEG_H|SEG_U|SEG_S,
    SEG_A|SEG_T
};
const unsigned char HiBitsLet[26] = {
    SEG_B|SEG_C|SEG_D|SEG_P,
    SEG_B|SEG_C|SEG_D|SEG_E|SEG_P|SEG_F,
    SEG_F|SEG_E|SEG_B,
    SEG_F|SEG_E|SEG_D|SEG_C|SEG_B,
    SEG_F|SEG_E|SEG_P|SEG_B,
    SEG_P|SEG_B,
    SEG_F|SEG_E|SEG_D|SEG_P|SEG_B,
    SEG_D|SEG_P|SEG_C,
    SEG_F|SEG_E|SEG_B,
    SEG_F|SEG_E|SEG_D|SEG_C,
    SEG_R|SEG_N,
    SEG_F|SEG_E,
    SEG_D|SEG_C|SEG_N,
    SEG_D|SEG_R|SEG_C,
    SEG_F|SEG_E|SEG_D|SEG_C|SEG_B,
    SEG_P|SEG_C|SEG_B,
    SEG_F|SEG_E|SEG_D|SEG_R|SEG_C|SEG_B,
    SEG_R|SEG_P|SEG_C|SEG_B,
    SEG_E|SEG_D|SEG_P|SEG_B,
    SEG_B,
    SEG_F|SEG_E|SEG_D|SEG_C,
    SEG_N,
    SEG_D|SEG_R|SEG_C,
    SEG_R|SEG_N,
    SEG_P|SEG_C,
    SEG_F|SEG_E|SEG_N|SEG_B,
};


/* Segment lighting mask.
   For each segment (array entry) the bit-mask indicates which of the
   LED chips should be enabled. Only the 4 bottom bits are used.
*/
unsigned char segs[17];


void clearSegs(void)
{
unsigned char idx;
for(idx=0; idx<17; idx++)
    segs[idx] = 0;
}

void setSeg(unsigned char chip, unsigned char letter)
{
unsigned char idx;
unsigned char mask;
unsigned char iLoBits = 0;
unsigned char iHiBits = 0;

/* Clear chip bits on all segments */
mask = 0b1000 >> chip;
for(idx=0; idx<17; idx++)
    segs[idx] &= ~mask;

/* Work out bits to set based on character */
if(letter >= 'a' && letter <= 'z')
    {
    iLoBits = LoBitsLet[letter - 'a'];
    iHiBits = HiBitsLet[letter - 'a'];
    segs[10] |= mask; /* Hack for dot being out of band */
    }
else if(letter >= 'A' && letter <= 'Z')
    {
    iLoBits = LoBitsLet[letter - 'A'];
    iHiBits = HiBitsLet[letter - 'A'];
    }
else if(letter >= '0' && letter <= '9')
    {
    iLoBits = LoBitsNum[letter - '0'];
    iHiBits = HiBitsNum[letter - '0'];
    }
else if(letter == '.')
    segs[10] |= mask;
else
    return;

/* Set chip bits in segments */
if(iLoBits & SEG_A)    segs[0] |= mask;
if(iLoBits & SEG_M)    segs[1] |= mask;
if(iLoBits & SEG_K)    segs[2] |= mask;
if(iLoBits & SEG_H)    segs[3] |= mask;
if(iLoBits & SEG_U)    segs[4] |= mask;
if(iLoBits & SEG_S)    segs[5] |= mask;
if(iLoBits & SEG_T)    segs[6] |= mask;
if(iLoBits & SEG_G)    segs[7] |= mask;
if(iHiBits & SEG_F)    segs[8] |= mask;
if(iHiBits & SEG_E)    segs[9] |= mask;
if(iHiBits & SEG_B)    segs[16] |= mask; /* Awkward exception */
if(iHiBits & SEG_D)    segs[11] |= mask;
if(iHiBits & SEG_R)    segs[12] |= mask;
if(iHiBits & SEG_P)    segs[13] |= mask;
if(iHiBits & SEG_C)    segs[14] |= mask;
if(iHiBits & SEG_N)    segs[15] |= mask;
/* segs[10] is dot, handled already */
}

void lightSegs(unsigned char idxSeg)
{
unsigned char iChipBits = 0;

/* Decide which LED chip needs enabling */
if( segs[idxSeg] & 0b1)
    iChipBits |= 0b10000000;
if( segs[idxSeg] & 0b10)
    iChipBits |= 0b01000000;
if( segs[idxSeg] & 0b100)
    iChipBits |= 0b00100000;
if( segs[idxSeg] & 0b1000)
    iChipBits |= 0b00001000;

/* Turn off LED chip */
PORTB &= ~0b11101000;

/* Set which segment is to be powered */
if(     idxSeg == 0)    PORTA = 0;
else if(idxSeg == 1)    PORTA = 0b00000001;
else if(idxSeg == 2)    PORTA = 0b00000010;
else if(idxSeg == 3)    PORTA = 0b00000011;
else if(idxSeg == 4)    PORTA = 0b00000100;
else if(idxSeg == 5)    PORTA = 0b00000101;
else if(idxSeg == 6)    PORTA = 0b00000110;
else if(idxSeg == 7)    PORTA = 0b00000111;
else if(idxSeg == 8)    PORTA = 0b01000000;
else if(idxSeg == 9)    PORTA = 0b01000001;
else if(idxSeg == 10)   PORTA = 0b01000010;
else if(idxSeg == 11)   PORTA = 0b01000011;
else if(idxSeg == 12)   PORTA = 0b01000100;
else if(idxSeg == 13)   PORTA = 0b01000101;
else if(idxSeg == 14)   PORTA = 0b01000110;
else if(idxSeg == 15)   PORTA = 0b01000111;
else                    PORTA = 0b10000000;

/* Enable on appropriate LED chip */
PORTB |= iChipBits;
}


void clockSetup(void)
{
OSCCON = 0b01111000; /* Fosc = 8Mhz */
while( ! (OSCCON & 0b100) );
}

void pinSetup(void)
{
PORTA = 0;
PORTB = 0;
TRISA = 0b00100000; /* All but MCLR output */
TRISB = 0b00010010; /* I2C input, rest out */
ANSEL=0;
}


void i2cSetup(void)
{
/* Upper nibble xx1x => Enable MSSP
   Lower nibble 0110 => I2C Slave */
SSPCON = 0b00110110;
SSPADD = 0x02;
}

void i2cSlaveWrite(unsigned char bite)
{
SSPCON &= ~0x80;
SSPBUF = bite;
while(SSPCON & 0x80)
    {
    /* Collision. Clear flag and retry */
    SSPCON &= ~0x80;
    SSPBUF = bite;
    }
}


void setTimeout(const unsigned char iTics)
{
/* With Fosc=8Mhz and default 1:1 prescale, each iTics
   (upper nibble) should be 128uS */
T1CON &= ~0b1;
TMR1H=255 - iTics;
TMR1L=0;
PIR1 &= ~0b1;
PIE1 |= 0b1;
T1CON |= 0b1;
}



void main(void)
{
unsigned char idxChip = 0;
unsigned char idxSeg = 0;
unsigned char valByte;
unsigned char idxWrite = 0;
unsigned char idxBuffer;
unsigned char buffer[4];

pinSetup();
clockSetup();
i2cSetup();

setSeg(0, 'R');
setSeg(1, 'E');
setSeg(2, 'M');
setSeg(3, 'y');

while(1)
    {
    lightSegs(idxSeg);
    idxSeg++;
    if(idxSeg == 17)
        idxSeg = 0;

    setTimeout(5);
    while( ! (PIR1 & 1) )
        {
        /* Handle incoming I2C transactions */
        if( PIR1 & 0x08 ) // I2C Interrupt
            {
            if( SSPCON & 0b1000000)
                {
                /* Handle overflow.
                   Should not happen, but included for completeness */
                valByte = SSPCON;
                SSPCON &= ~0b1000000;
                PIR1 &= ~0x08;
                SSPCON |= 0b10000;
                }

            if(SSPSTAT & 0b100)
                {
                /* I2C read. For demonstration purposes this sends
                   back the buffer contents from the last write. */
                PIR1 &= ~0x08;
                if(! (SSPSTAT & 0b100000))
                    {
                    valByte = SSPBUF;
                    idxBuffer = 0;
                    }
                else if(idxBuffer < 3)
                    idxBuffer++;
                while(SSPSTAT & 0b1);
                i2cSlaveWrite(buffer[idxBuffer]);
                //i2cEndStretch();
                SSPCON |= 0b10000;
                continue;
                }

            /* Read bytes while bus is busy */
            while(SSPSTAT & 0b1)
                {
                valByte = SSPBUF;
                if(SSPSTAT & 0b100000)
                    {
                    /* Data byte. Add to buffer */
                    if(idxWrite < 4)
                        {
                        buffer[idxWrite] = valByte;
                        idxWrite++;
                        }
                    }
                else
                    {
                    /* Address byte. Reset offset */
                    idxWrite = 0;
                    idxBuffer = 0;
                    }
                PIR1 &= ~0x08;
                }
            }
        }

    /* Update segments */
    if( idxWrite > 0 )
        while( idxBuffer < idxWrite )
            {
            setSeg(idxBuffer,buffer[idxBuffer]);
            idxBuffer++;
            }
    }
}
