;   relay.asm - Power relay control
;   Copyright (C) Remy Horton, 2020.
;   SPDX-License-Identifier: BSD-3-Clause

PROCESSOR 12f1840
#include <p12f1840.inc>
    RADIX dec
    CONFIG FOSC=INTOSC, WDTE=OFF
    CONFIG MCLRE=ON
    CONFIG LVP=OFF

SHAREDDATE      UDATA   0x70
cyclenum        RES     1
cyclelen        RES     1
cycleon         RES     1
byte            RES     1


; Entrypoints
STARTUP     CODE    0x0000
    GOTO    entry
INTERRUPT   CODE    0x0004
    GOTO    interrupt


interrupt:
    ; Check position in cycle
    MOVFW   cyclelen
    SUBWF   cyclenum,0
    BTFSC   STATUS,0   ; C
    GOTO    cyclefinished   ; C == 0 means  W <= f
    ; Check part of cycle
    MOVFW   cycleon
    SUBWF   cyclenum,0
    BTFSS   STATUS,0
    CALL    triggerpulse
    INCF    cyclenum,1
    GOTO    intreturn
cyclefinished:
    CLRF   cyclenum
intreturn:
    ; Clear interrupts
    BANKSEL IOCAF
    CLRF    IOCAF
    BANKSEL PIE1
    BCF     PIE1,0
    RETFIE

triggerpulse:
    ; Delay to allow for skew
;    MOVLW   205
;    BANKSEL TMR0
;    MOVWF   TMR0
;    BANKSEL INTCON
;    BCF     INTCON,2
;    BTFSS   INTCON,2
;    GOTO    $-1
    ; Pulse on
    BANKSEL PORTA
    BSF     PORTA,4
    ; Pulse width
    MOVLW   640
    MOVWF   TMR0
    BANKSEL INTCON
    BCF     INTCON,2
    BTFSS   INTCON,2
    GOTO    $-1
    ; Pulse off
    BANKSEL PORTA
    BCF     PORTA,4


;    MOVLW   30
;    MOVWF   TMR0
;    BANKSEL INTCON
;    BCF     INTCON,2
;    BTFSS   INTCON,2
;    GOTO    $-1


    RETURN

entry:
    ; Clock setup
    BANKSEL OSCCON
    MOVLW   b'01101000' ; 4MHz (1uS cycles)
    MOVWF   OSCCON
    BANKSEL OSCSTAT
    BTFSS   OSCSTAT,4   ; HF IntOsc Ready
    GOTO    $-1
    BTFSS   OSCSTAT,3   ; HF IntOsc locked
    GOTO    $-1
    BTFSS   OSCSTAT,0   ; HF IntOsc stable
    GOTO    $-1

    ; Timer0 setup
    MOVLW   b'01000001'
    MOVLW   b'01000110'
    MOVLW   b'01000001' ; 001 = 1:4
    BANKSEL OPTION_REG
    MOVWF   OPTION_REG

    ; Pin setup
    BANKSEL APFCON
    BCF     APFCON,2    ; RA0 -> TxD
    BSF     APFCON,7    ; RA5 -> RxD
    MOVLW   b'11101110'
    BANKSEL TRISA
    MOVWF   TRISA
    MOVLW   b'11000000'
    BANKSEL ANSELA
    MOVWF   ANSELA
    ; 

    MOVLW   b'00000100'
    BANKSEL IOCAP
    MOVWF   IOCAP
    BANKSEL IOCAN
    CLRF    IOCAN
    BANKSEL IOCAF
    CLRF    IOCAF
    BANKSEL INTCON
    BSF     INTCON,7    ; GIE
    BSF     INTCON,3    ; IOCIE
    
    ; RS232 setup
    BANKSEL BAUDCON
    BSF     BAUDCON,3   ; BRG16=1
    MOVLW   103          ; BAUD=9600 (4M/4*(103+1))
;    MOVLW   12          ; BAUD=9600 (500000/4*(12+1))
    CLRF    SPBRGH
    MOVWF   SPBRGL
    BCF     TXSTA,4     ; SYNC=0
    BSF     TXSTA,2     ; BRGH1
    BSF     TXSTA,5     ; TXEN=1
    BSF     RCSTA,4     ; CREN=1
    BSF     RCSTA,7     ; SPEN=1


    BANKSEL TXSTA
    BTFSS   TXSTA,1
    GOTO    $-1
    MOVLW   'O'
    BANKSEL TXREG
    MOVWF   TXREG
    BANKSEL TXSTA
    BTFSS   TXSTA,1
    GOTO    $-1
    MOVLW   'k'
    BANKSEL TXREG
    MOVWF   TXREG


mainloop:
    CLRF    cyclenum
    MOVLW   7
    MOVWF   cyclelen
    MOVLW   4
    MOVWF   cycleon
;    BANKSEL PORTA
;    BSF     PORTA,4

    GOTO    $


    BANKSEL PORTA
    ; Wait for pulse phase
    BSF     PORTA,4
    BTFSS   PORTA,2
    GOTO    $-1



    ; Make sure pulse finished
    BANKSEL PORTA
    BTFSC   PORTA,2
    GOTO    $-1





    GOTO    mainloop






    ; Busy wait for RS232
    BANKSEL PIR1
    BTFSS   PIR1,5
    GOTO    mainloop
    BCF     PIR1,5
    ; Read byte
    BANKSEL RCREG
    MOVFW   RCREG
    MOVWF   byte
    ; Check if 0
    SUBLW   '0'
    BTFSC   STATUS,2
    GOTO    off
    ; Check if 1
    MOVFW   byte
    SUBLW   '1'
    BTFSC   STATUS,2
    GOTO    on
    ; Neither 0 or 1
    GOTO    mainloop
on:
    BANKSEL PORTA
    BSF     PORTA,2
    GOTO    mainloop

off:
    BANKSEL PORTA
    BCF     PORTA,2
    GOTO    mainloop

    END
