;   debounce.asm - Input button debouncing
;   Copyright (C) Remy Horton, 2019.
;   SPDX-License-Identifier: BSD-3-Clause


; Active-low converted to active-hi
inputDebounce:
    ; Check whether time to next sample
    BANKSEL INTCON
    BTFSS   INTCON,2
    RETURN
    ; Shift up old samples
    BANKSEL buttonBits
    BCF     STATUS,0    ; C bit
    RLF     (buttonBits + 0),1
    BCF     STATUS,0
    RLF     (buttonBits + 1),1
    BCF     STATUS,0    ; C bit
    RLF     (buttonBits + 2),1
    BCF     STATUS,0
    RLF     (buttonBits + 3),1
    ; Sample ports
    BANKSEL PORTC
    BTFSC   PORTC,2
    BSF     (buttonBits + 0),0
    BTFSC   PORTC,3
    BSF     (buttonBits + 1),0
    BTFSC   PORTC,4
    BSF     (buttonBits + 2),0
    BTFSC   PORTC,5
    BSF     (buttonBits + 3),0
    ; Check button 2 samples
    MOVFW   (buttonBits + 0)
    XORLW   0xff
    BTFSC   STATUS,2
    BCF     buttonState,0
    MOVFW   (buttonBits + 0)
    ANDLW   0xff
    BTFSC   STATUS,2
    BSF     buttonState,0
    ; Check button 3 samples
    MOVFW   (buttonBits + 1)
    XORLW   0xff
    BTFSC   STATUS,2
    BCF     buttonState,1
    MOVFW   (buttonBits + 1)
    ANDLW   0xff
    BTFSC   STATUS,2
    BSF     buttonState,1
    ; Check button 4 samples
    MOVFW   (buttonBits + 2)
    XORLW   0xff
    BTFSC   STATUS,2
    BCF     buttonState,2
    MOVFW   (buttonBits + 2)
    ANDLW   0xff
    BTFSC   STATUS,2
    BSF     buttonState,2
    ; Check button 5 samples
    MOVFW   (buttonBits + 3)
    XORLW   0xff
    BTFSC   STATUS,2
    BCF     buttonState,3
    MOVFW   (buttonBits + 3)
    ANDLW   0xff
    BTFSC   STATUS,2
    BSF     buttonState,3
    ; Check for changes
    MOVFW   buttonState
    XORWF   buttonState+1,0
    BTFSC   STATUS,2    ; Z bit
    RETURN
    ; Something has changed...
    MOVWF   buttonChange
    MOVFW   buttonState
    MOVWF   buttonNow
    MOVWF   buttonState+1
    RETURN

