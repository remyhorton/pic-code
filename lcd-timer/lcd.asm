;   lcd.asm - LCD control functions
;   Copyright (C) Remy Horton, 2019.
;   SPDX-License-Identifier: BSD-3-Clause


lcdInit:
    CLRF    timeSecs
    CLRF    timeMins
    MOVLW   0x08
    MOVWF   lcdDots
    ; Enable display
    CALL    i2cWaitIdle
    CALL    i2cStartWait
    MOVLW   0x70
    CALL    i2cSendWait
    MOVLW   0x49
    CALL    i2cSendWait
    CALL    i2cStopWait
    ; Set middle colon on Bank 1
    CALL    i2cWaitIdle
    CALL    i2cStartWait
    MOVLW   0x70
    CALL    i2cSendWait
    MOVLW   0xfa
    CALL    i2cSendWait
    MOVLW   0x00
    CALL    i2cSendWait
    CALL    i2cSendWait
    CALL    i2cSendWait
    CALL    i2cSendWait
    MOVLW   1
    CALL    i2cSendWait
    CALL    i2cStopWait
    ; Set input to Bank 0
    CALL    i2cWaitIdle
    CALL    i2cStartWait
    MOVLW   0x70
    CALL    i2cSendWait
    MOVLW   0x78
    CALL    i2cSendWait
    CALL    i2cStopWait
    RETURN
   

lcdSetBlink:
    CALL    i2cWaitIdle
    CALL    i2cStartWait
    MOVLW   0x70
    CALL    i2cSendWait
    MOVLW   0x76
    CALL    i2cSendWait
    CALL    i2cStopWait
    RETURN


lcdClearBlink:
    CALL    i2cWaitIdle
    CALL    i2cStartWait
    MOVLW   0x70
    CALL    i2cSendWait
    MOVLW   0x70
    CALL    i2cSendWait
    CALL    i2cStopWait
    RETURN


lcdUpdate:
    CLRF    lcdMins+1
    CLRF    lcdMins
    CLRF    lcdSecs+1
    CLRF    lcdSecs
    ; Calc minute digits
    MOVFW   timeMins
    MOVWF   lcdMins
    MOVLW   10
    SUBWF   lcdMins,0
    BTFSS   STATUS,0  ; Skip if noBorrow ( W <= F )
    GOTO    $+5
    MOVWF   lcdMins
    MOVLW   1
    ADDWF   lcdMins+1,1
    GOTO    $-7
    ; Calc second digits
    MOVFW   timeSecs
    MOVWF   lcdSecs
    MOVLW   10
    SUBWF   lcdSecs,0
    BTFSS   STATUS,0  ; Skip if noBorrow ( W <= F )
    GOTO    $+5
    MOVWF   lcdSecs
    MOVLW   1
    ADDWF   lcdSecs+1,1
    GOTO    $-7

    CALL    i2cWaitIdle
    CALL    i2cStartWait
    MOVLW   0x70
    CALL    i2cSendWait
    MOVLW   0x00
    CALL    i2cSendWait

    MOVFW   lcdMins+1
    CALL    lcdGetSegments
    BTFSC   lcdDots,0
    IORLW   1
    CALL    i2cSendWait

    MOVFW   lcdMins
    CALL    lcdGetSegments
    BTFSC   lcdDots,1
    IORLW   1
    CALL    i2cSendWait

    MOVFW   lcdSecs+1
    CALL    lcdGetSegments
    BTFSC   lcdDots,2
    IORLW   1
    CALL    i2cSendWait

    MOVFW   lcdSecs
    CALL    lcdGetSegments
    BTFSC   lcdDots,3
    IORLW   1
    CALL    i2cSendWait

    CALL    i2cStopWait
    RETURN


lcdGetSegments
    BRW
    RETLW   0xf6
    RETLW   0xc0
    RETLW   0x6e
    RETLW   0xea
    RETLW   0xd8
    RETLW   0xba
    RETLW   0xbe
    RETLW   0xe0
    RETLW   0xfe
    RETLW   0xf8

