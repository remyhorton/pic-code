;   setup.asm - Initialisation code
;   Copyright (C) Remy Horton, 2019.
;   SPDX-License-Identifier: BSD-3-Clause

setupClock:
    BANKSEL OSCCON
;    MOVLW   b'01101000' ; 4MHz
    MOVLW   b'01100000' ; 2MHz
    MOVWF   OSCCON
    RETURN

setupPins:
    BANKSEL ANSELA
    CLRF    ANSELA
    CLRF    ANSELC
    BANKSEL TRISC
    BSF     TRISC,0 ; SDA
    BSF     TRISC,1 ; SCL
    BCF     TRISA,2
    RETURN

setupI2C:
    BANKSEL SSP1ADD
    MOVLW   0x09
    MOVWF   SSP1ADD
    MOVLW   b'00101000'
    MOVWF   SSP1CON1
    CLRF    SSP1CON2
    RETURN

setupTimers:
    ; Timer0 (used for de-bouncing)
    BANKSEL OPTION_REG
;    MOVLW   b'01010100' ; P164
    MOVLW   b'01010011' ; 011 -> 1:16
    MOVWF   OPTION_REG
    ; Timer1 (used for timing)
    BANKSEL T1CON
;    MOVLW   b'00110001'
    MOVLW   b'10001101'
    MOVWF   T1CON
    RETURN

setupInterrupts:
    BANKSEL INTCON
;    BSF PIE1,0  ; TMR1
    BSF INTCON,6 ; PEIE
    BSF INTCON,7 ; GIE
    RETURN
