;   timer.asm - LCD timer (main code)
;   Copyright (C) Remy Horton, 2018.
;   SPDX-License-Identifier: BSD-3-Clause


PROCESSOR 16f1823
#include <p16f1823.inc>
    RADIX dec
    ; Configuration directives
    ; https://gputils.sourceforge.io/code-examples/configurations.html
    ; https://gputils.sourceforge.io/html-help/PIC16F1828-conf.html
    CONFIG FOSC=INTOSC, WDTE=OFF
    CONFIG MCLRE=ON
    CONFIG LVP=OFF

; Data memory
B1DATA          UDATA   0x20
buttonState     RES     2
buttonChange    RES     1
buttonNow       RES     1
buttonBits      RES     4
stateTimer      RES     1
temp            RES     1
lcdDots         RES     1
lcdMins         RES     2
lcdSecs         RES     2
timeMins        RES     1
timeSecs        RES     1


; Entrypoints
STARTUP     CODE    0x0000
    GOTO    entry
INTERRUPT   CODE    0x0004
    GOTO    interruptTic


; Sub-modules
include "setup.asm"
include "debounce.asm"
include "i2c.asm"
include "lcd.asm"


entry:
    CALL    setupClock
    CALL    setupPins
    CALL    setupI2C
    CALL    setupTimers
    CALL    setupInterrupts
    MOVLB   0   ; Select Bank 0
    ;   Clear data memory
    BCF     PORTA,2
    CLRF    buttonState
    CLRF    buttonState+1
    CLRF    buttonChange
    CLRF    buttonNow
    CLRF    buttonBits
    CLRF    buttonBits+1
    CLRF    buttonBits+2
    CLRF    buttonBits+3
    CLRF    stateTimer
    ; Setup LCD display
    CALL    lcdInit
    MOVLW   0
    MOVWF   timeMins
    MOVLW   0
    MOVWF   timeSecs
    CALL    lcdUpdate
    GOTO    main


incMinute:
    MOVLW   1
    ADDWF   timeMins,1
    MOVLW   100
    SUBWF   timeMins,0
    BTFSC   STATUS,0
    CLRF    timeMins
    RETURN


incSecond:
    MOVLW   1
    ADDWF   timeSecs,1
    MOVLW   60
    SUBWF   timeSecs,0
    BTFSC   STATUS,0
    CLRF    timeSecs
    RETURN


procTic:
    CALL    setTicTimer
    ; Process seconds
    MOVLW   1
    SUBWF   timeSecs,1
    BTFSC   STATUS,0    ; C
    RETURN
    ; Were no seconds left. Process minutes
    MOVLW   1
    SUBWF   timeMins,1
    BTFSS   STATUS,0    ; C
    GOTO    procTicOver
    ; Still time left
    MOVLW   59
    MOVWF   timeSecs
    RETURN
procTicOver
    ; At this point timeSecs/timeMins will be 0xff. Clear them
    ; otherwise a later call to lcdGetSegments will screw up..
    CLRF    timeMins
    CLRF    timeSecs
    BCF     T1CON,0
    BSF     PORTA,2
    MOVLW   0x20
    MOVWF   stateTimer
    RETURN


setTicTimer:
    BCF     T1CON,0
    CLRF    TMR1L
    MOVLW   0x7f
    MOVWF   TMR1H
    BCF     PIR1,0
    BSF     T1CON,0
    RETURN


setRepeatTimer:
    BCF     T1CON,0
    MOVWF   temp
    MOVLW   0xff
    MOVWF   TMR1H
    MOVFW   temp
    SUBWF   TMR1H,1
    CLRF    TMR1L
    BCF     PIR1,0
    BSF     T1CON,0
    RETURN
    

goStateIfBtn   macro   bit,target
    local   skip
    BTFSS   buttonNow,bit
    GOTO    skip
    BCF     buttonNow,bit
    MOVLW   target
    MOVWF   stateTimer
    GOTO    main
skip
    endm

goStateAnyBtn   macro   target
    local   skip
    MOVF    buttonNow,1
    BTFSC   STATUS,2
    GOTO    skip
    CLRF    buttonNow
    MOVLW   target
    MOVWF   stateTimer
skip
    endm


interruptTic: 
    CALL    setTicTimer
    BCF     PIR1,0
    MOVLB   0 
    ; Process seconds
    MOVLW   1
    SUBWF   timeSecs,1
    BTFSC   STATUS,0    ; C
    GOTO    interruptUpdate
    ; Were no seconds left. Process minutes
    MOVLW   1
    SUBWF   timeMins,1
    BTFSS   STATUS,0    ; C
    GOTO    interruptTicOver
    ; Still time left
    MOVLW   59
    MOVWF   timeSecs
interruptUpdate:
    CALL    lcdUpdate
    RETFIE
interruptTicOver
    BANKSEL PIE1
    BCF     PIE1,0
    MOVLB   0
    CLRF    timeMins
    CLRF    timeSecs
    BCF     T1CON,0
    BSF     PORTA,2
    MOVLW   0x20
    MOVWF   stateTimer
    RETFIE


; Timer state bits:
; 01 0: Adjust
; 02 1:
; 04 2:
; 08 3: 
; 10 4: Repeat
; 20 5: Alarm
; 40 6: StartCountdown
; 80 7: Countdown
main:
    CALL    inputDebounce
    MOVLB   0
    ; Jump to state handlers
    BTFSC   stateTimer,7
    GOTO    procCountdown
    BTFSC   stateTimer,6
    GOTO    procStart
    BTFSC   stateTimer,5
    GOTO    procAlarm
    BTFSC   stateTimer,0
    GOTO    procAdjust
    GOTO    procIdle
procStart
    MOVLW   0x80
    MOVWF   stateTimer
    CALL    setTicTimer
    ; Enable Timer1 interrupts
;;    BANKSEL PIE1
;;    BSF     PIE1,0
;;    MOVLB   0 
    GOTO    main
procCountdown
    BTFSS   PIR1,0  ; Timer1
    GOTO    $+4
    CALL    procTic
    CALL    lcdUpdate    
    GOTO    main
    ; Any button press stops countdown
    MOVF    buttonNow,1
    BTFSC   STATUS,2
    GOTO    main
    CLRF    buttonNow
    CLRF    stateTimer
    ; Disable Timer1 interrupt
;;    BANKSEL PIE1
;;    BCF     PIE1,0
;;    MOVLB   0 
    GOTO    main
procAlarm
    MOVF    buttonNow,1
    BTFSC   STATUS,2
    GOTO    main
    BCF     PORTA,2
    CLRF    stateTimer
    CLRF    buttonNow
    GOTO    main
procIdle
    CALL    lcdClearBlink
    goStateIfBtn    0,0x40
    goStateIfBtn    1,0x01
    GOTO     main
procAdjust
    goStateIfBtn    1,0x00
    CALL    lcdUpdate
    ; Hack to reset to 00:00
    BTFSS   buttonNow,0
    GOTO    $+3
    CLRF    timeMins
    CLRF    timeSecs
    ; Check for no buttons pressed
    BTFSC   buttonNow,2
    GOTO    procAdjust_Initial
    BTFSC   buttonNow,3
    GOTO    procAdjust_Initial
    BCF     stateTimer,4 ; Clear repeat
    CALL    lcdSetBlink
    GOTO    main
procAdjust_Initial
    BTFSC   stateTimer,4
    GOTO    procAdjust_Repeat
    CALL    lcdClearBlink
    BTFSC   buttonNow,2
    CALL    incMinute
    BTFSC   buttonNow,3
    CALL    incSecond
    MOVLW   0x4f
    CALL    setRepeatTimer
    BSF     stateTimer,4 ; Set repeat
    GOTO    main
procAdjust_Repeat
    BTFSS   PIR1,0  ; Timer1
    GOTO    main
    BTFSC   buttonNow,2
    CALL    incMinute
    BTFSC   buttonNow,3
    CALL    incSecond
    MOVLW   0x0c
    CALL    setRepeatTimer
    GOTO    main

    END

