/*
   I2C two-register slave
   Copyright (C) Remy Horton, 2018.
   SPDX-License-Identifier: BSD-3-Clause
*/

#define NO_BIT_DEFINES
#include "pic16f1828.h"

//#define I2C_ADDR_HOLD 1

__code short __at (_CONFIG1) cfg0 =
	_FOSC_INTOSC & _WDTE_OFF & _MCLRE_ON & _PWRTE_OFF & _CP_OFF & _CPD_OFF;

unsigned char i2cByte;
unsigned char i2cAddr[2];
unsigned char i2cAddrCnt = 0;

void clockSetup(void)
{
//OSCCON = 0b01011010; // 1MHz
//OSCCON = 0b01100010; // 2MHz
  OSCCON = 0b01101010; // 4MHz
//OSCCON = 0b01110010; // 8MHz
//OSCCON = 0b01111010; // 16MHz
}

void pinSetup(void)
{
TRISA = 0b00001000;
TRISB = 0b01010000;
TRISC = 0b00000000;
ANSELA=0;
ANSELB=0;
ANSELC=0;
}


void i2cSetupSlave(void)
{
SSP1CON1 = 0b00100110; // I2C slave, 7-bit address, no S/P interrupts
SSP1CON2 = 0b1;   // Enable clock stretching
SSP1CON3 = 0b1;   // Enable data-hold
SSP1ADD  = 0x20;  // Slave address
#ifdef I2C_ADDR_HOLD
SSP1CON3 |= 0b10; // Enable address-hold
#endif
}


void i2cSlaveWrite(unsigned char bite)
{
unsigned char junk;
SSP1CON &= ~0x80;
SSP1BUF = bite;
while(SSP1CON & 0x80)
    { /* Write collision */
    junk = SSP1BUF;
    SSP1CON &= ~0x80;
    SSP1BUF = bite;
    while(1);
    }
}


void i2cOverflow(void)
{
if(SSP1CON & 0x40)
    {
    i2cByte = SSP1BUF;
    SSP1CON1 &= ~0x40;
    PIR1 &= ~0x08;
    SSP1CON1 |= 0b10000;
    while(1);
    }
}


void i2cSend(void)
{
PIR1 &= ~0x08;
if(! (SSP1STAT & 0x20))
    { /* Address byte received */
    i2cByte = SSP1BUF;
#ifdef I2C_ADDR_HOLD
    if( SSP1CON3 & 0x80 )
        { /* Acknowledge sequence */
        SSP1CON2 &= ~0x20;
        SSP1CON1 |= 0x10;
        return;
        }
#endif
    }
else if( (SSP1CON & 0x40) )
    {
    // NACK from Master. Don't send any more data.
    SSP1CON1 |= 0x10;
    return;
    }
i2cSlaveWrite(i2cAddr[0] + i2cAddr[1]);
SSP1CON1 |= 0x10;
}

void i2cRecv(void)
{
PIR1 &= ~0x08;
i2cByte = SSP1BUF;
if(! (SSP1STAT & 0x20))
    {
    i2cAddrCnt = 0;
    // This should have no effect with AHEN disabled,
    // but it is required to recover when DHEN is used
    // to NACK incoming data.
    SSP1CON2 &= ~0x20;
    }
else if(i2cAddrCnt < 2)
    {
    i2cAddr[i2cAddrCnt++] = i2cByte;
    if(i2cByte == 255)
        SSP1CON2 |= 0x20;
    else
        SSP1CON2 &= ~0x20;
    }
else
    {
    SSP1CON2 &= ~0x20;
    }
SSP1CON1 |= 0x10;
}


void main(void)
{
pinSetup();
i2cSetupSlave();

while(1)
    {
    if( PIR1 & 0x08 )
        { /* I2C interrupt */
        i2cOverflow();

        if( SSP1STAT & 0x04 )
            {
            i2cSend();
            continue;
            }

        while( SSP1STAT & 0b1 )
            {  /* Busy flag set */
            i2cRecv();
            }
        }
    }
    /* Non-I2C logic here */
}

