/*
   I2C firmware routines
   Copyright (C) Remy Horton, 2018.
   SPDX-License-Identifier: BSD-3-Clause
*/

/* I2C setup */
void i2cSetup(void)
{
SSP1ADD = 0x27; /* 16M/(4*(0x27+1)) = 16M/160 =  100Khz. p262 */
SSP1CON1 |= 0b00101000; /* Enable, 1000=I2C master */
//SSP1CON2 = 0;
//SSPEN=1;
}


void i2cWaitIdle(void)
{
while( (SSP1CON2 & 0b00011111) || (SSP1STAT & 0b100) );
}

void i2cWaitPending(void)
{
/* Reading SSP1IF immediately after writing SSP1BUF may
 * give incorrect value. Need to wait a cycle or two..
 */
__asm nop nop __endasm;
while( ! (PIR1 & 0x08) );
//while( ! SSP1IF );
}

#ifdef NO_BIT_DEFINES
void i2cStartWait(void)
{
PIR1 &= ~0x08;
SSP1CON2 |= 0x01;
while( ! (PIR1 & 0x08) );
}

void i2cSendWait(const int bite)
{
PIR1 &= ~0x08;
SSP1CON2 &= ~0x40;
SSP1BUF = bite;
__asm
    nop
    nop
__endasm;
while( ! (PIR1 & 0x08) );
}

void i2cRestartWait(void)
{
PIR1 &= ~0x08;
SSP1CON2 |= 0x02;
while( ! (PIR1 & 0x08) );
}


void i2cStopWait(void)
{
PIR1 &= ~0x08;
SSP1CON2 |= 0x04;
while( ! (PIR1 & 0x08) );
}

#else /* NO_BIT_DEFINES */

void i2cStart(void)
{
SSP1IF=0;
SEN=1;
}

void i2cRestart(void)
{
SSP1IF=0;
RSEN=1;
}

void i2cSend(const unsigned char bite)
{
SSP1IF=0;
ACKSTAT=0;
SSP1BUF = bite;
}

void i2cStop(void)
{
SSP1IF=0;
PEN=1;
}

void i2cStartWait(void)
{
i2cStart();
i2cWaitPending();
}
void i2cRestartWait(void)
{
i2cRestart();
i2cWaitPending();
}
void i2cSendWait(const int bite)
{
i2cSend(bite);
i2cWaitPending();
// ACKSTAT=1 -> NACK from slave
}
void i2cStopWait(void)
{
i2cStop();
i2cWaitPending();
}

#endif


unsigned char i2cRecvAck(void)
{
unsigned char iValue;
PIR1 &= ~0x08;
SSP1CON2 |= 0x08;
i2cWaitPending();
PIR1 &= ~0x08;
iValue = SSPBUF;
SSP1CON2 &= ~0x20;
SSP1CON2 |= 0x10;
//ACKDT=0; //set is NACK
//ACKEN=1;
i2cWaitPending();
return iValue;
}

unsigned char i2cRecvNack(void)
{
unsigned char iValue;
PIR1 &= ~0x08;
SSP1CON2 |= 0x08;
i2cWaitPending();
PIR1 &= ~0x08;
iValue = SSPBUF;
SSP1CON2 |= 0x30;
i2cWaitPending();
PIR1 &= ~0x08;
return iValue;
}


