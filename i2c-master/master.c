/*
   RS232-I2C master firmware
   Copyright (C) Remy Horton, 2018.
   SPDX-License-Identifier: BSD-3-Clause
*/

#define NO_BIT_DEFINES
#include "pic12f1822.h"

/* Configuration word 1 */
__code short __at (_CONFIG1) cfg0 =
	_FOSC_INTOSC
    & _WDTE_OFF  // No watchdog timer
    & _MCLRE_ON  // Reset button enabled
    & _PWRTE_OFF // No power-up timer
    & _CP_OFF & _CPD_OFF; // No code/data protection

/* Configuration word 2
   _LVP_OFF is required by _MCLRE_OFF (Section 7.3 p75) */
__code short __at (_CONFIG2) cfg1 =
    _LVP_OFF;   // Low-voltage programming off


/* SDCC does not support compiling multiple source files */
#include "i2c.c"
#include "rs232.c"

#define MAX_DATA 60
//#define USE_GPSIM


/* Set input/output pins
   RA0: 0 Use as output
   RA1: 1 I2C SCL
   RA2: 1 I2C SDA
   RA3: 1 MCLR (has to be input)
   RA4: 0 RS232 Tx
   RA5: 1 RS232 Rx
*/
void pinSetup(void)
{
TRISA = 0b00101110;
ANSELA = 0;
}


unsigned char bufSerial[MAX_DATA + 5];
unsigned char cntSerial = 0;
unsigned char valByte;

void rs232Flush(void)
{
PIR2 &= ~0x2;
PR2 = 30;
T2CON = 0b00000111; /* 62.5kHz */
while(1)
    {
    while( !(PIR1 & 0x22) ); // ! (RCIF|TMR2IF)
    if ( PIR1 & 0x20 )
        {
        valByte = RCREG;
        PIR2 &= 0x20;
        TMR2 = 0;
        continue;
        }
    break;
    }
T2CON &= 0x04;
cntSerial = 0;
}


void cmdError(unsigned char code)
{
#ifdef USE_GPSIM
rs232WriteHex(0x00);
rs232WriteByte(' ');
rs232WriteHex(code);
rs232WriteBytes("\n", 1);
#else
rs232WriteByte(0x00);
rs232WriteByte(code);
#endif
}


void main(void)
{
unsigned char idxByte;
unsigned char cntRecv;

/* 16Mhz clock freq */
OSCCON = 0b01111010;

pinSetup();
rs232Setup();
i2cSetup();

// addr mode [addr1] [addr2] cnt [data]

rs232Flush();
PORTA &= ~0x01;
PIR1 &= ~0x20;
while(1)
    {
    //FIXME: Add RS232 timeout??
    while(! (PIR1 & 0x20) ); //RCIF
    valByte = RCREG;

#ifdef USE_GPSIM
    valByte -= '0';
#endif
    PIR1 &= ~0x20;
    bufSerial[cntSerial++] = valByte;

    if( cntSerial < 2 )
        continue;
    if( bufSerial[1] > 2 )
        {
        cmdError(1);
        rs232Flush();
        continue;
        }
    if( cntSerial < 3 + bufSerial[1] )
        continue;
    if( bufSerial[bufSerial[1]+2] > MAX_DATA )
        {
        cmdError(bufSerial[1] + 2);
        rs232Flush();
        continue;
        }
    if( ! (bufSerial[0] & 0x01)
       && cntSerial < 3 + bufSerial[1] + bufSerial[2+bufSerial[1]])
        continue;

#ifdef USE_GPSIM
    if( bufSerial[0] & 0x01 )
        bufSerial[0] = 63;
    else
        bufSerial[0] = 78;
#endif

    if( bufSerial[0] & 0x01 )
        { /* I2C Read */

        PORTA |= 0x01;

        /* Start */
        i2cWaitIdle();
        i2cStartWait();

        if (bufSerial[1] > 0)
            {
            /* Send I2C address (writing) */
            i2cSendWait(bufSerial[0] & 0xfe);
            if(SSP1CON2 & 0x40)
                {
                i2cStopWait();
                cmdError(0);
                goto end;
                }

            /* Send register address(es) */
            for(idxByte=0; idxByte < bufSerial[1]; idxByte++)
                {
                i2cSendWait( bufSerial[2+idxByte] );
                if(SSP1CON2 & 0x40)
                    {
                    i2cStopWait();
                    cmdError(2+idxByte);
                    goto end;
                    }
                }

            /* Restart */
            i2cRestartWait();
            }

        /* Send I2C address */
        i2cSendWait(bufSerial[0]);
        if(SSP1CON2 & 0x40)
            {
            i2cStopWait();
            /* I2C reads with register address(es) failing here means
               something much more serious than just incorrect device
               address, as the device address would have previously
               been acked earlier in the transaction. Hence the error
               code that is out of line with the other error codes */
            if (bufSerial[1] > 0)
                cmdError(0xff);
            else
                cmdError(0);
            goto end;
            }

        /* Receive data */
        cntRecv = bufSerial[ 2+bufSerial[1] ];
        for(idxByte=0; idxByte < cntRecv; idxByte++)
            {
            PIR1 &= ~0x08; // Clear SSP1IF
            i2cWaitIdle();
            SSPCON2 |= 0x08; //RCEN=1;
            i2cWaitPending();
            bufSerial[cntSerial + idxByte] = SSPBUF;
            if( idxByte == cntRecv - 1 )
                SSP1CON2 |= 0x20;  // ACKDT=1
            else
                SSP1CON2 &= ~0x20; // ACKDT=0
            SSP1CON2 |= 0x10;      // ACKEN=1
            i2cWaitIdle();
            }

        /* Finish */
        i2cStopWait();
#ifdef USE_GPSIM
        rs232WriteHex(0xff);
        for(idxByte=0; idxByte < cntRecv; idxByte++)
            {
            rs232WriteByte(' ');
            rs232WriteHex(bufSerial[cntSerial+idxByte]);
            }
        rs232WriteBytes("\n", 1);
#else
        rs232WriteByte(0x01);
        rs232WriteByte(cntRecv);
        for(idxByte=0; idxByte < cntRecv; idxByte++)
            rs232WriteByte(bufSerial[cntSerial+idxByte]);
#endif
        PORTA &= ~0x01;
        }
    else
        { /* I2C Write */

        PORTA |= 0x01;

        /* Start */
        i2cWaitIdle();
        i2cStartWait();

        /* Send I2C address */
        i2cSendWait(bufSerial[0]);
        if(SSP1CON2 & 0x40)
            {
            i2cStopWait();
            cmdError(0);
            goto end;
            }

        /* Send register address, if any */
        for(idxByte=0; idxByte < bufSerial[1]; idxByte++)
            {
            i2cSendWait( bufSerial[2+idxByte] );
            if(SSP1CON2 & 0x40)
                {
                i2cStopWait();
                cmdError(2+idxByte);
                goto end;
                }
            }

        /* Send data */
        for(idxByte=3+bufSerial[1]; idxByte < cntSerial; idxByte++)
            {
            i2cSendWait( bufSerial[idxByte] );
            if(SSP1CON2 & 0x40)
                {
                i2cStopWait();
                cmdError(idxByte);
                goto end;
                }
            }

        /* Finish */
        i2cStopWait();
#ifdef USE_GPSIM
        rs232WriteBytes("ff fe\n", 6);
#else
        rs232WriteByte(0x01);
        rs232WriteByte(cntSerial);
#endif
        PORTA &= ~0x01;
        }
end:
    cntSerial = 0;
    }
}

