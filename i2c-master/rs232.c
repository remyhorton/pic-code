/*
   RS232 firmware routines
   Copyright (C) Remy Horton, 2018.
   SPDX-License-Identifier: BSD-3-Clause
*/

/* RS232 setup
   With BRG16=1 and BRGH=1 the BAUD rate is Fosc/4(SPBRG+1)
   (details on P281 of data-sheet) */
void rs232Setup(void)
{
BAUDCON |= 0b0001000; // BRG16=1
SPBRGH=0;  SPBRGL=68; // BAUD=57600 (16M/4*(68+1) = 57971)
APFCON |= 0b10000100; // RXDTSEL=1 TXCKSEL=1
TXSTA &= ~0b00010000; // SYNC=0
TXSTA |=  0b00100100; // TXEN=1 BRGH=1
RCSTA |=  0b10010000; // SPEN=1 CREN=1
}


void rs232WriteByte(const char bite)
{
TXREG = bite;
while(! (PIR1 & 0b00010000));
}

void rs232WriteBytes(char bites[], int len)
{
int idxBite;
for(idxBite=0; idxBite<len; idxBite++)
    rs232WriteByte(bites[idxBite]);
}


void rs232WriteHex(const int hex)
{
int hi = hex >> 4;
int lo = hex & 0x0f;
if( hi )
    {
    if( hi < 10 )
        rs232WriteByte( hi + '0' );
    else
        rs232WriteByte( hi + 'a' - 10);
    }
if( lo < 10 )
    rs232WriteByte( lo + '0');
else
    rs232WriteByte( lo + 'a' - 10);
}
