#!/usr/bin/env python

import serial,sys

if len(sys.argv) < 2:
    print "USAGE: {0} <tty> <addr> [outgoing bytes]".format(
            sys.argv[0]
            )
    sys.exit(1)
try:
    addrI2C = int(sys.argv[2],16)
except ValueError:
    print "Error: Invalid I2C address"
    sys.exit(1)
i2c = serial.Serial(port=sys.argv[1],baudrate=57600)

packet = b''
for octet in sys.argv[2:]:
    bite = chr(int(octet,16))
    packet += bite
print "Sending {0} bytes..".format(len(packet))
cntSentBytes = i2c.write(packet)

result,code = i2c.read(2)
valResult = ord(result)
valCode = ord(code)
if valResult == 0:
    print "Failed (code {0})".format(ord(code))
else:
    if addrI2C & 0x01:
        print "Success ({0} bytes sent, {1} replies):".format(
            cntSentBytes, valCode),
        bites = i2c.read(valCode)
        for bite in bites:
            print hex(ord(bite)),
    else:
        print "Success ({0} bytes sent)".format(cntSentBytes)
print

