;   relay.asm - Power relay control
;   Copyright (C) Remy Horton, 2020.
;   SPDX-License-Identifier: BSD-3-Clause

PROCESSOR 12f1840
#include <p12f1840.inc>
    RADIX dec
    CONFIG FOSC=INTOSC, WDTE=OFF
    CONFIG MCLRE=ON
    CONFIG LVP=OFF

SHAREDDATE      UDATA   0x70
byte            RES     1

; Entrypoints
STARTUP     CODE    0x0000
    BANKSEL OSCCON
;    MOVLW   b'01011000'
    MOVLW   b'01010000'
    MOVWF   OSCCON
    ; Pin setup
    BANKSEL APFCON
    BSF     APFCON,2    ; RA4 -> TxD
    BSF     APFCON,7    ; RA5 -> RxD
    MOVLW   b'11100011'
    BANKSEL TRISA
    MOVWF   TRISA
    MOVLW   b'11101011'
    BANKSEL ANSELA
    MOVWF   ANSELA
    ; RS232 setup
    BANKSEL BAUDCON
    BSF     BAUDCON,3   ; BRG16=1
;    MOVLW   25          ; BAUD=9600 (1M/4*(25+1))
    MOVLW   12          ; BAUD=9600 (500000/4*(12+1))
    CLRF    SPBRGH
    MOVWF   SPBRGL
    BCF     TXSTA,4     ; SYNC=0
    BSF     TXSTA,2     ; BRGH1
    BSF     TXSTA,5     ; TXEN=1
    BSF     RCSTA,4     ; CREN=1
    BSF     RCSTA,7     ; SPEN=1

mainloop:
    ; Busy wait for RS232
    BANKSEL PIR1
    BTFSS   PIR1,5
    GOTO    mainloop
    BCF     PIR1,5
    ; Read byte
    BANKSEL RCREG
    MOVFW   RCREG
    MOVWF   byte
    ; Check if 0
    SUBLW   '0'
    BTFSC   STATUS,2
    GOTO    off
    ; Check if 1
    MOVFW   byte
    SUBLW   '1'
    BTFSC   STATUS,2
    GOTO    on
    ; Neither 0 or 1
    GOTO    mainloop
on:
    BANKSEL PORTA
    BCF     PORTA,2
    GOTO    mainloop

off:
    BANKSEL PORTA
    BSF     PORTA,2
    GOTO    mainloop

    END
