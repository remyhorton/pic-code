;   setup.asm - Initialisation code
;   Copyright (C) Remy Horton, 2020.
;   SPDX-License-Identifier: BSD-3-Clause

setupPins:
    BANKSEL APFCON
    BSF     APFCON,2    ; RA4 -> TxD
    BSF     APFCON,7    ; RA5 -> RxD
    MOVLW   b'11100011'
    BANKSEL TRISA
    MOVWF   TRISA
    MOVLW   b'11100011'
    BANKSEL ANSELA
    MOVWF   ANSELA
    RETURN

setupRS232:
    BANKSEL BAUDCON     ; All I2C register on same bank
    BSF     BAUDCON,3   ; BRG16=1
    MOVLW   13          ; BAUD=9600 (500000/4*(13+1))
    CLRF    SPBRGH
    MOVWF   SPBRGL
    BCF     TXSTA,4     ; SYNC=0
    BSF     TXSTA,2     ; BRGH1
    BSF     TXSTA,5     ; TXEN=1
    BSF     RCSTA,4     ; CREN=1
    BSF     RCSTA,7     ; SPEN=1
    RETURN

