/* matrix.c - LED Matrix control firmware
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * (C) Remy Horton, 2018
 */
#define NO_BIT_DEFINES
#include "pic16f886.h"

__code short __at (_CONFIG1) cfg0 =
    _FOSC_INTRC_NOCLKOUT & _WDTE_OFF & _MCLRE_OFF & _LVP_OFF;

unsigned char ledCol1[8];
unsigned char ledCol2[8];
unsigned char bufCols[15];


void clockSetup(void)
{
//clk stat   xcccsss
//OSCCON = 0b01110001; // 8MHz
//  OSCCON = 0b01100001; // 4MHz
//OSCCON = 0b01010001; // 2MHz
//OSCCON = 0b01000001; // 1MHz
  OSCCON = 0b00110001; // 500kHz
}

void pinSetup(void)
{
PORTC = 0;          // Force 74HC238 to all-rows-off
TRISC = 0b11111000; // 0-2 output; 3-4 I2C; 5-7 unused
TRISA = 0b00000000; // All output
TRISB = 0b10000000; // 0-6 output; 7 unused
//TRISA = 0b111000;   // 3-5 unused; 6-7 not present
//TRISB = 0b01010000; // 4 & 6 are I2C
//TRISC = 0b00000000; // all output
ANSEL = 0;
ANSELH = 0;
}

void timerSetup(void)
{
/* Timer2 setup (~2.5ms) */
//          xSSSSEPP
//T2CON = 0b00100100; // 1:5
//T2CON = 0b00010100; // 1:3
////  T2CON = 0b00011100; // 1:4
//T2CON = 0b00001100; // 1:2
T2CON = 0b00000100; // 1:1
}


void i2cSetupSlave(void)
{
// 00    Flags
// 1     Enable I2C
// 1     Enable clock (no stretch)
// 0110  I2C slave with 7-but address, no start/stop interrupts
SSPCON = 0b00110110;
SSPCON2 = 0b1; // stretching
SSPADD = 0x02;
}


void i2cSlaveWrite(unsigned char bite)
{
SSPCON &= ~0x80;
SSPBUF = bite;
while(SSPCON & 0x80)
    {
    // Collision
    SSPCON &= ~0x80;
    SSPBUF = bite;
    }
}


void ledClear(void)
{
#if 0
unsigned char idxRow;
for(idxRow=0; idxRow<7; idxRow++)
    {
    ledCol1[idxRow] = 0;
    ledCol2[idxRow] = 0;
    }
#else
ledCol1[0] = 0;
ledCol1[1] = 0;
ledCol1[2] = 0;
ledCol1[3] = 0;
ledCol1[4] = 0;
ledCol1[5] = 0;
ledCol1[6] = 0;
ledCol2[0] = 0;
ledCol2[1] = 0;
ledCol2[2] = 0;
ledCol2[3] = 0;
ledCol2[4] = 0;
ledCol2[5] = 0;
ledCol2[6] = 0;
#endif
}


void ledFlip(void)
{
#if 0
unsigned char idxCol;
unsigned char idxRow;
unsigned char bits;

idxCol = 8;
while( idxCol > 0 )
    {
    idxCol--;
    bits = bufCols[idxCol];
    for(idxRow=0; idxRow<7; idxRow++)
        {
        ledCol1[idxRow] = (ledCol1[idxRow] << 1) | (bits & 1);
        bits >>= 1;
        }
    }
idxCol = 15;
while( idxCol > 8 )
    {
    idxCol--;
    bits = bufCols[idxCol];
    for(idxRow=0; idxRow<7; idxRow++)
        {
        ledCol2[idxRow] = (ledCol2[idxRow] << 1) | (bits & 1);
        bits >>= 1;
        }
    }
#else
ledClear();
ledCol1[7] = bufCols[0];
__asm
    BANKSEL _ledCol1
    BTFSC   (_ledCol1 + 7),0
    BSF     (_ledCol1 + 0),0
    BTFSC   (_ledCol1 + 7),1
    BSF     (_ledCol1 + 1),0
    BTFSC   (_ledCol1 + 7),2
    BSF     (_ledCol1 + 2),0
    BTFSC   (_ledCol1 + 7),3
    BSF     (_ledCol1 + 3),0
    BTFSC   (_ledCol1 + 7),4
    BSF     (_ledCol1 + 4),0
    BTFSC   (_ledCol1 + 7),5
    BSF     (_ledCol1 + 5),0
    BTFSC   (_ledCol1 + 7),6
    BSF     (_ledCol1 + 6),0
__endasm;
ledCol1[7] = bufCols[1];
__asm
    BANKSEL _ledCol1
    BTFSC   (_ledCol1 + 7),0
    BSF     (_ledCol1 + 0),1
    BTFSC   (_ledCol1 + 7),1
    BSF     (_ledCol1 + 1),1
    BTFSC   (_ledCol1 + 7),2
    BSF     (_ledCol1 + 2),1
    BTFSC   (_ledCol1 + 7),3
    BSF     (_ledCol1 + 3),1
    BTFSC   (_ledCol1 + 7),4
    BSF     (_ledCol1 + 4),1
    BTFSC   (_ledCol1 + 7),5
    BSF     (_ledCol1 + 5),1
    BTFSC   (_ledCol1 + 7),6
    BSF     (_ledCol1 + 6),1
__endasm;
ledCol1[7] = bufCols[2];
__asm
    BANKSEL _ledCol1
    BTFSC   (_ledCol1 + 7),0
    BSF     (_ledCol1 + 0),2
    BTFSC   (_ledCol1 + 7),1
    BSF     (_ledCol1 + 1),2
    BTFSC   (_ledCol1 + 7),2
    BSF     (_ledCol1 + 2),2
    BTFSC   (_ledCol1 + 7),3
    BSF     (_ledCol1 + 3),2
    BTFSC   (_ledCol1 + 7),4
    BSF     (_ledCol1 + 4),2
    BTFSC   (_ledCol1 + 7),5
    BSF     (_ledCol1 + 5),2
    BTFSC   (_ledCol1 + 7),6
    BSF     (_ledCol1 + 6),2
__endasm;
ledCol1[7] = bufCols[3];
__asm
    BANKSEL _ledCol1
    BTFSC   (_ledCol1 + 7),0
    BSF     (_ledCol1 + 0),3
    BTFSC   (_ledCol1 + 7),1
    BSF     (_ledCol1 + 1),3
    BTFSC   (_ledCol1 + 7),2
    BSF     (_ledCol1 + 2),3
    BTFSC   (_ledCol1 + 7),3
    BSF     (_ledCol1 + 3),3
    BTFSC   (_ledCol1 + 7),4
    BSF     (_ledCol1 + 4),3
    BTFSC   (_ledCol1 + 7),5
    BSF     (_ledCol1 + 5),3
    BTFSC   (_ledCol1 + 7),6
    BSF     (_ledCol1 + 6),3
__endasm;
ledCol1[7] = bufCols[4];
__asm
    BANKSEL _ledCol1
    BTFSC   (_ledCol1 + 7),0
    BSF     (_ledCol1 + 0),4
    BTFSC   (_ledCol1 + 7),1
    BSF     (_ledCol1 + 1),4
    BTFSC   (_ledCol1 + 7),2
    BSF     (_ledCol1 + 2),4
    BTFSC   (_ledCol1 + 7),3
    BSF     (_ledCol1 + 3),4
    BTFSC   (_ledCol1 + 7),4
    BSF     (_ledCol1 + 4),4
    BTFSC   (_ledCol1 + 7),5
    BSF     (_ledCol1 + 5),4
    BTFSC   (_ledCol1 + 7),6
    BSF     (_ledCol1 + 6),4
__endasm;
ledCol1[7] = bufCols[5];
__asm
    BANKSEL _ledCol1
    BTFSC   (_ledCol1 + 7),0
    BSF     (_ledCol1 + 0),5
    BTFSC   (_ledCol1 + 7),1
    BSF     (_ledCol1 + 1),5
    BTFSC   (_ledCol1 + 7),2
    BSF     (_ledCol1 + 2),5
    BTFSC   (_ledCol1 + 7),3
    BSF     (_ledCol1 + 3),5
    BTFSC   (_ledCol1 + 7),4
    BSF     (_ledCol1 + 4),5
    BTFSC   (_ledCol1 + 7),5
    BSF     (_ledCol1 + 5),5
    BTFSC   (_ledCol1 + 7),6
    BSF     (_ledCol1 + 6),5
__endasm;
ledCol1[7] = bufCols[6];
__asm
    BANKSEL _ledCol1
    BTFSC   (_ledCol1 + 7),0
    BSF     (_ledCol1 + 0),6
    BTFSC   (_ledCol1 + 7),1
    BSF     (_ledCol1 + 1),6
    BTFSC   (_ledCol1 + 7),2
    BSF     (_ledCol1 + 2),6
    BTFSC   (_ledCol1 + 7),3
    BSF     (_ledCol1 + 3),6
    BTFSC   (_ledCol1 + 7),4
    BSF     (_ledCol1 + 4),6
    BTFSC   (_ledCol1 + 7),5
    BSF     (_ledCol1 + 5),6
    BTFSC   (_ledCol1 + 7),6
    BSF     (_ledCol1 + 6),6
__endasm;
ledCol1[7] = bufCols[7];
__asm
    BANKSEL _ledCol1
    BTFSC   (_ledCol1 + 7),0
    BSF     (_ledCol1 + 0),7
    BTFSC   (_ledCol1 + 7),1
    BSF     (_ledCol1 + 1),7
    BTFSC   (_ledCol1 + 7),2
    BSF     (_ledCol1 + 2),7
    BTFSC   (_ledCol1 + 7),3
    BSF     (_ledCol1 + 3),7
    BTFSC   (_ledCol1 + 7),4
    BSF     (_ledCol1 + 4),7
    BTFSC   (_ledCol1 + 7),5
    BSF     (_ledCol1 + 5),7
    BTFSC   (_ledCol1 + 7),6
    BSF     (_ledCol1 + 6),7
__endasm;
ledCol2[7] = bufCols[8];
__asm
    BANKSEL _ledCol2
    BTFSC   (_ledCol2 + 7),0
    BSF     (_ledCol2 + 0),0
    BTFSC   (_ledCol2 + 7),1
    BSF     (_ledCol2 + 1),0
    BTFSC   (_ledCol2 + 7),2
    BSF     (_ledCol2 + 2),0
    BTFSC   (_ledCol2 + 7),3
    BSF     (_ledCol2 + 3),0
    BTFSC   (_ledCol2 + 7),4
    BSF     (_ledCol2 + 4),0
    BTFSC   (_ledCol2 + 7),5
    BSF     (_ledCol2 + 5),0
    BTFSC   (_ledCol2 + 7),6
    BSF     (_ledCol2 + 6),0
__endasm;
ledCol2[7] = bufCols[9];
__asm
    BANKSEL _ledCol2
    BTFSC   (_ledCol2 + 7),0
    BSF     (_ledCol2 + 0),1
    BTFSC   (_ledCol2 + 7),1
    BSF     (_ledCol2 + 1),1
    BTFSC   (_ledCol2 + 7),2
    BSF     (_ledCol2 + 2),1
    BTFSC   (_ledCol2 + 7),3
    BSF     (_ledCol2 + 3),1
    BTFSC   (_ledCol2 + 7),4
    BSF     (_ledCol2 + 4),1
    BTFSC   (_ledCol2 + 7),5
    BSF     (_ledCol2 + 5),1
    BTFSC   (_ledCol2 + 7),6
    BSF     (_ledCol2 + 6),1
__endasm;
ledCol2[7] = bufCols[10];
__asm
    BANKSEL _ledCol2
    BTFSC   (_ledCol2 + 7),0
    BSF     (_ledCol2 + 0),2
    BTFSC   (_ledCol2 + 7),1
    BSF     (_ledCol2 + 1),2
    BTFSC   (_ledCol2 + 7),2
    BSF     (_ledCol2 + 2),2
    BTFSC   (_ledCol2 + 7),3
    BSF     (_ledCol2 + 3),2
    BTFSC   (_ledCol2 + 7),4
    BSF     (_ledCol2 + 4),2
    BTFSC   (_ledCol2 + 7),5
    BSF     (_ledCol2 + 5),2
    BTFSC   (_ledCol2 + 7),6
    BSF     (_ledCol2 + 6),2
__endasm;
ledCol2[7] = bufCols[11];
__asm
    BANKSEL _ledCol2
    BTFSC   (_ledCol2 + 7),0
    BSF     (_ledCol2 + 0),3
    BTFSC   (_ledCol2 + 7),1
    BSF     (_ledCol2 + 1),3
    BTFSC   (_ledCol2 + 7),2
    BSF     (_ledCol2 + 2),3
    BTFSC   (_ledCol2 + 7),3
    BSF     (_ledCol2 + 3),3
    BTFSC   (_ledCol2 + 7),4
    BSF     (_ledCol2 + 4),3
    BTFSC   (_ledCol2 + 7),5
    BSF     (_ledCol2 + 5),3
    BTFSC   (_ledCol2 + 7),6
    BSF     (_ledCol2 + 6),3
__endasm;
ledCol2[7] = bufCols[12];
__asm
    BANKSEL _ledCol2
    BTFSC   (_ledCol2 + 7),0
    BSF     (_ledCol2 + 0),4
    BTFSC   (_ledCol2 + 7),1
    BSF     (_ledCol2 + 1),4
    BTFSC   (_ledCol2 + 7),2
    BSF     (_ledCol2 + 2),4
    BTFSC   (_ledCol2 + 7),3
    BSF     (_ledCol2 + 3),4
    BTFSC   (_ledCol2 + 7),4
    BSF     (_ledCol2 + 4),4
    BTFSC   (_ledCol2 + 7),5
    BSF     (_ledCol2 + 5),4
    BTFSC   (_ledCol2 + 7),6
    BSF     (_ledCol2 + 6),4
__endasm;
ledCol2[7] = bufCols[13];
__asm
    BANKSEL _ledCol2
    BTFSC   (_ledCol2 + 7),0
    BSF     (_ledCol2 + 0),5
    BTFSC   (_ledCol2 + 7),1
    BSF     (_ledCol2 + 1),5
    BTFSC   (_ledCol2 + 7),2
    BSF     (_ledCol2 + 2),5
    BTFSC   (_ledCol2 + 7),3
    BSF     (_ledCol2 + 3),5
    BTFSC   (_ledCol2 + 7),4
    BSF     (_ledCol2 + 4),5
    BTFSC   (_ledCol2 + 7),5
    BSF     (_ledCol2 + 5),5
    BTFSC   (_ledCol2 + 7),6
    BSF     (_ledCol2 + 6),5
__endasm;
ledCol2[7] = bufCols[14];
__asm
    BANKSEL _ledCol2
    BTFSC   (_ledCol2 + 7),0
    BSF     (_ledCol2 + 0),6
    BTFSC   (_ledCol2 + 7),1
    BSF     (_ledCol2 + 1),6
    BTFSC   (_ledCol2 + 7),2
    BSF     (_ledCol2 + 2),6
    BTFSC   (_ledCol2 + 7),3
    BSF     (_ledCol2 + 3),6
    BTFSC   (_ledCol2 + 7),4
    BSF     (_ledCol2 + 4),6
    BTFSC   (_ledCol2 + 7),5
    BSF     (_ledCol2 + 5),6
    BTFSC   (_ledCol2 + 7),6
    BSF     (_ledCol2 + 6),6
__endasm;
#endif
}


void ledLightCol(unsigned char idxRow)
{
PORTC &= ~0b111;
PORTA = ledCol1[idxRow];
PORTB = ledCol2[idxRow];
PORTC = idxRow + 1;
}


void main(void)
{
unsigned char idxRow;
unsigned char idxCol;
unsigned char i2cByte;

clockSetup();
pinSetup();
i2cSetupSlave();
timerSetup();

for(idxCol=0; idxCol<15; idxCol++)
    bufCols[idxCol] = 0;

bufCols[0] = 0x1;
bufCols[1] = 0x2;
bufCols[2] = 0x4;
bufCols[3] = 0x8;
bufCols[4] = 0x10;
bufCols[5] = 0x20;
bufCols[6] = 0x40;
bufCols[7] = 0x40;
bufCols[8] = 0x20;
bufCols[9] = 0x10;
bufCols[10] = 0x1;
bufCols[11] = 0x2;
bufCols[12] = 0x4;
bufCols[13] = 0x8;
bufCols[14] = 0x10;
ledFlip();

idxRow = 0;
while(1)
    {
    ledLightCol(idxRow);
    idxRow++;
    if(idxRow > 6)
        idxRow = 0;
    PIR1 &= ~0x02;
    while( !(PIR1 & 0x02) ) //|| (PIR1 & 0x08) )
        {
        if( PIR1 & 0x08 ) // I2C Interrupt
            {
            if(SSPCON & 0x40)
                {
                ledCol1[5] = 0x02;
                // Receive overflow FIXME
                i2cByte = SSPBUF;
                SSPCON &= ~0x40;
                PIR1 &= ~0x08;
                SSPCON |= 0b10000;
                }

            while(SSPSTAT & 0b1) //BF
                {
                //ledCol1[0] = 0x01;
                PIR1 &= ~0x08;
                i2cByte = SSPBUF;
                if( ! (SSPSTAT & 0x20))
                    {
                    idxCol = 0;
                    }
                else if( idxCol < 15 )
                    {
                    bufCols[idxCol++]= i2cByte;
                    if(idxCol == 15)
                        {
                        ledFlip();
                        }
                    }
                else
                    {
                    }
                SSPCON |= 0x10;
                }
            }
        }
    }
}
