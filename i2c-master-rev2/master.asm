;   master.asm - RS232-controlled I2C master
;   Copyright (C) Remy Horton, 2020.
;   SPDX-License-Identifier: BSD-3-Clause

PROCESSOR 12f1840
#include <p12f1840.inc>
    RADIX dec
    CONFIG FOSC=INTOSC, WDTE=OFF
    CONFIG MCLRE=ON
    CONFIG LVP=OFF
    CONFIG PLLEN=OFF

BANK0DATA       UDATA   0x20
buffer          RES     80

SHAREDDATA      UDATA   0x70
byte            RES     1
lenBuffer       RES     1
lenLeft         RES     1


lenExpected     RES     1
cntSent         RES     1
errorcode       RES     1
posError        RES     1

; Entrypoints
STARTUP     CODE    0x0000
    GOTO    entry
    GOTO    $

#include "i2c.asm"
#include "setup.asm"

entry:
    CALL    setupClock
    CALL    setupPins
    CALL    setupRS232
    CALL    setupI2C
begin:
    CALL    rs232Flush
    BANKSEL PORTA
    BCF     PORTA,0

    MOVLW   0x20
    MOVWF   FSR0H
    MOVWF   FSR1H
    CLRF    FSR0L
    CLRF    lenBuffer
mainloop:
    ; Busy wait for RS232
    BANKSEL PIR1
    BTFSS   PIR1,5
    GOTO    mainloop
    BCF     PIR1,5
    ; Read byte
    BANKSEL RCREG
    MOVFW   RCREG
    CALL    inputHack
    ; Store and increment payload counter
    MOVWI   FSR0++
    INCF    lenBuffer,1
    ; Got at least 2 control bytes?
    MOVLW   2
    SUBWF   lenBuffer,0
    BTFSS   STATUS,0
    GOTO    mainloop
    ; Register count valid?
    BANKSEL buffer
    MOVLW   3
    SUBWF   buffer+1,0
    BTFSC   STATUS,0
    GOTO    mainerror
    ; Got all expected register bytes?
    MOVLW   2
    SUBWF   lenBuffer,0
    SUBWF   buffer+1,0
    BTFSC   STATUS,0
    GOTO    mainloop
    ; Check data count in range..
    CLRF    FSR1L
    ADDFSR  FSR1,2
    MOVFW   buffer+1
    ADDWF   FSR1L,1
    MOVFW   INDF1
    ; ..not zero
    ANDLW   0xff
    BTFSC   STATUS,2
    GOTO    mainerror
    ; ..under 140
    MOVLW   141      ; Check..
    SUBWF   INDF1,0
    BTFSC   STATUS,0
    GOTO    mainerror
    ; Got all data if write?
    BTFSC   buffer,0
    GOTO    mainready
    MOVLW   3
    ADDWF   buffer+1,0
    ADDWF   INDF1,0
    SUBWF   lenBuffer,0
    BTFSS   STATUS,0
    GOTO    mainloop
    GOTO    mainready
mainerror:
    ; Report offset of most recent byte as ereor
    MOVLW   0
    CALL    rs232Write
    MOVLW   1
    SUBWF   lenBuffer,0
    CALL    rs232Write
    GOTO    begin

; Control packet received. Time for I2C..
mainready:
    BANKSEL PORTA
    BSF     PORTA,0
    MOVLW   0x20
    MOVWF   FSR0H
    BANKSEL buffer
    BTFSS   buffer+0,0
    GOTO    procWrite
    MOVF    buffer+1,0
    BTFSS   STATUS,2
    GOTO    procWrite
    ; Send I2C addresss (read with no regs)
    CALL    addrHack
    CALL    i2cWaitIdle
    CALL    i2cStartWait
    BANKSEL buffer
    MOVFW   buffer+0
    CALL    i2cSendWait
    ; Error check
    MOVLW   0
    BANKSEL SSP1CON2
    BTFSC   SSP1CON2,6
    GOTO    i2cerror
    ; Setup read loop
    MOVLW   2
    MOVWF   FSR0L
    MOVLW   3
    MOVWF   posError
    MOVIW   FSR0++
    MOVWF   lenBuffer
    MOVWF   lenLeft
    CLRF    FSR0L
    GOTO    i2creadloop

procWrite:
    ; Send I2C addresss.
    ; Reads with register addresses also go through
    ; here as well as writes, so need to smash LSB.
    CALL    addrHack
    CALL    i2cWaitIdle
    CALL    i2cStartWait
    BANKSEL buffer
    MOVFW   buffer+0
    ANDLW   0xfe
    CALL    i2cSendWait
    ; Error check
    MOVLW   0
    BANKSEL SSP1CON2
    BTFSC   SSP1CON2,6
    GOTO    i2cerror
    ; How many registers?
    BANKSEL buffer
    MOVLW   2
    SUBWF   buffer+1,0
    BTFSC   STATUS,0
    GOTO    send2regs
    MOVLW   1
    SUBWF   buffer+1,0
    BTFSC   STATUS,0
    GOTO    send1regs
    GOTO    send0regs
send2regs
    MOVFW   buffer+2
    CALL    i2cSendWait
    MOVLW   2
    BANKSEL SSP1CON2
    BTFSC   SSP1CON2,6
    GOTO    i2cerror
    BANKSEL buffer
    MOVFW   buffer+3
    CALL    i2cSendWait
    MOVLW   3
    BANKSEL SSP1CON2
    BTFSC   SSP1CON2,6
    GOTO    i2cerror
    MOVLW   4
    MOVWF   FSR0L
    MOVLW   5
    MOVWF   posError
    GOTO    procCount
send1regs
    MOVFW   buffer+2
    CALL    i2cSendWait
    MOVLW   2
    BANKSEL SSP1CON2
    BTFSC   SSP1CON2,6
    GOTO    i2cerror
    MOVLW   3
    MOVWF   FSR0L
    MOVLW   4
    MOVWF   posError
    GOTO    procCount
send0regs
    MOVLW   2
    MOVWF   FSR0L
    MOVLW   3
    MOVWF   posError
procCount:
    MOVIW   FSR0++
    MOVWF   lenBuffer
    MOVWF   lenLeft
    ; Read or write?
    BANKSEL buffer
    BTFSC   buffer+0,0
    GOTO    procRead
i2cwriteloop:
    ; Send data byte
    MOVIW   FSR0++
    CALL    i2cSendWait
    ; Error check
    MOVFW   posError
    BANKSEL SSP1CON2
    BTFSC   SSP1CON2,6
    GOTO    i2cerror
    ; Loop check
    INCF    posError,1
    DECFSZ  lenLeft,1
    GOTO    i2cwriteloop
    ; Finish up
    CALL    i2cStopWait
    MOVLW   1
    CALL    rs232Write
    MOVFW   lenBuffer
    CALL    rs232Write
    GOTO    begin

procRead: 
    ; Switch from write to read. Reads without any
    ; register values use a seperate loop setup.
    CALL    i2cRestartWait
    BANKSEL buffer
    MOVFW   buffer+0
    CALL    i2cSendWait
    MOVLW   0xff
    BANKSEL SSP1CON2
    BTFSC   SSP1CON2,6
    GOTO    i2cerror
    MOVLW   0
    MOVWF   FSR0L
i2creadloop:
    CALL    i2cReadByte
    MOVWI   FSR0++
    MOVLW   2
    SUBWF   lenLeft,0
    BANKSEL SSP1CON2
    BCF     SSP1CON2,5  ; ACK
    BTFSS   STATUS,0
    BSF     SSP1CON2,5  ; Last byte, so NACK
    BSF     SSP1CON2,4
    CALL    i2cWaitIdle
    ; Loop check
    DECFSZ  lenLeft,1
    GOTO    i2creadloop
    ; Finish up
    CALL    i2cStopWait
    MOVLW   1
    CALL    rs232Write
    MOVFW   lenBuffer
    MOVWF   lenLeft
    CALL    rs232Write
    ; Send back read data to host
    MOVLW   0
    MOVWF   FSR0L
    MOVIW   FSR0++
    CALL    rs232Write
    DECFSZ  lenLeft,1
    GOTO    $-3
    GOTO    begin

i2cerror:
    CALL    i2cStopWait
    MOVWF   errorcode
    MOVLW   0
    CALL    rs232Write
    MOVFW   errorcode
    CALL    rs232Write
    GOTO    begin

rs232Write:
    BANKSEL TXREG
    MOVWF   TXREG
    BANKSEL PIR1
    BTFSS   PIR1,4
    GOTO    $-1
    RETURN

rs232Flush:
    ; Set timeout timer
    BANKSEL PIR1
    BCF     PIR1,1
    CLRF    TMR2
    MOVLW   30
    MOVWF   PR2
    MOVLW   b'00000111' ; 62.5kHz
    MOVWF   T2CON
    ; Purge loop
    BTFSC   PIR1,5
    GOTO    $+4     ; Data waiting
    BTFSC   PIR1,1
    GOTO    $+6     ; Timeout
    GOTO    $-4
    ; Flush byte
    MOVFW   RCREG
    BCF     PIR1,5
    CLRF    TMR2
    GOTO    $-8
    ;
    BCF     T2CON,2
    RETURN


; Hooks for developing with GPsim
inputHack:
    RETURN
    MOVWF   byte
    MOVLW   '0'
    SUBWF   byte,0
    RETURN
addrHack:
    RETURN
    BANKSEL buffer
    BTFSS   buffer,0
    GOTO    hackWrite
    GOTO    hackRead
hackRead:
    BANKSEL buffer
    MOVLW   63
    MOVWF   buffer
    RETURN
hackWrite:
    BANKSEL buffer
    MOVLW   78
    MOVWF   buffer
    RETURN
hackRS232:
    RETURN
    MOVLW   51
    RETURN

    END
