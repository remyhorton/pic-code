;   setup.asm - Initialisation code
;   Copyright (C) Remy Horton, 2020.
;   SPDX-License-Identifier: BSD-3-Clause

setupClock:
    BANKSEL OSCCON
    MOVLW   b'01100010'     ; 2MHz
;    MOVLW   b'01101010'     ; 4MHz
;    MOVLW   b'01011000'     ; 1MHz
;    MOVLW   b'01010010'    ; 500kHz
    MOVWF   OSCCON
    RETURN
setupPointers:
    ; INDF0
    MOVLB   0
    MOVLW   LOW buffer
    MOVWF   FSR0L
    MOVLW   HIGH buffer
    MOVWF   FSR0H
    CLRF    lenBuffer
    ;INDF1
    MOVLW   LOW buffer
    MOVWF   FSR1L
    MOVLW   HIGH buffer
    MOVWF   FSR1H
    RETURN
setupPins:
    BANKSEL APFCON
    BSF     APFCON,2    ; RA4 -> TxD
    BSF     APFCON,7    ; RA5 -> RxD
    MOVLW   b'11101110'
    BANKSEL TRISA
    MOVWF   TRISA
    BANKSEL ANSELA
    CLRF    ANSELA
    RETURN
setupRS232:
    BANKSEL BAUDCON
    BSF     BAUDCON,3   ; BRG16=1
;;    MOVLW   51
    MOVLW   8           ; BAUD=57k6 (Datasheet p284)
    CALL    hackRS232 
    CLRF    SPBRGH
    MOVWF   SPBRGL
    BCF     TXSTA,4     ; SYNC=0
    BSF     TXSTA,2     ; BRGH=1
    BSF     TXSTA,5     ; TXEN=1
    BSF     RCSTA,4     ; CREN=1
    BSF     RCSTA,7     ; SPEN=1
    RETURN
setupI2C:
    MOVLW   0x27
    BANKSEL SSP1ADD
    MOVWF   SSP1ADD
    MOVLW   b'00101000'
    MOVWF   SSP1CON1
    RETURN

