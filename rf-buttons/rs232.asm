; rs232.asm - RS232 transmission routines
; Copyright (C) Remy Horton, 2019
; SPDX-License-Identifier: BSD-3-Clause

setupRS232:
    BANKSEL BAUDCON
    MOVLW   b'0001000' ; BRG16=1
    MOVWF   BAUDCON
;    MOVLW   68      ; BAUD=57600 (16M/4*(68+1) = 57971)
    MOVLW   17      ; BAUD=57600 (4M/4*(17+1) = 55555)
;    MOVLW   8      ; BAUD=57600 (2M/4*(8+1) = 55555)
;    MOVLW   3      ; BAUD=57600 (1M/4*(3+1) = 62000)
    CLRF    SPBRGH
    MOVWF   SPBRGL
    BSF     TXSTA,2 ; BRGH=1
    BCF     TXSTA,4 ; SYNC=0
    BSF     TXSTA,5 ; TxEN=1
    BSF     RCSTA,7 ; SPEN=1
    ;BSF     RCSTA,4 ; CREN=1
    RETURN

rs232Tx
    ; Wait for empty Tx buffer
    BANKSEL PIR1
    BTFSS   PIR1,4
    GOTO    $-1
    ; Send byte to RS232
    BANKSEL TXREG
    MOVWF   TXREG
    ; Delay to make sure PIR1<4> is valid
    NOP
    NOP
    ; Set Bank0
    MOVLB   0
    RETURN

valToHex:
    brw
    retlw   '0'
    retlw   '1'
    retlw   '2'
    retlw   '3'
    retlw   '4'
    retlw   '5'
    retlw   '6'
    retlw   '7'
    retlw   '8'
    retlw   '9'
    retlw   'a'
    retlw   'b'
    retlw   'c'
    retlw   'd'
    retlw   'e'
    retlw   'f'

txHex:
    GOTO    txHexLo

    MOVLW   0
    BANKSEL CCPR1H
    SUBWF   CCPR1H,0
    BTFSC   STATUS,2 ; Z bit
    GOTO    txHexLo
    MOVLW   '-'
    CALL    rs232Tx
    MOVLW   '-'
    CALL    rs232Tx
    RETURN

    BANKSEL CCPR1H
    MOVLW   0
    BTFSC   CCPR1H,4
    ADDLW   1
    BTFSC   CCPR1H,5
    ADDLW   2
    BTFSC   CCPR1H,6
    ADDLW   4
    BTFSC   CCPR1H,7
    ADDLW   8
    CALL    valToHex
    CALL    rs232Tx

    BANKSEL CCPR1H
    MOVLW   0
    BTFSC   CCPR1H,0
    ADDLW   1
    BTFSC   CCPR1H,1
    ADDLW   2
    BTFSC   CCPR1H,2
    ADDLW   4
    BTFSC   CCPR1H,3
    ADDLW   8
    CALL    valToHex
    CALL    rs232Tx

txHexLo
    BANKSEL CCPR1H
    MOVLW   0
    BTFSC   CCPR1L,4
    ADDLW   1
    BTFSC   CCPR1L,5
    ADDLW   2
    BTFSC   CCPR1L,6
    ADDLW   4
    BTFSC   CCPR1L,7
    ADDLW   8
    CALL    valToHex
    CALL    rs232Tx
    BANKSEL CCPR1H
    MOVLW   0
    BTFSC   CCPR1L,0
    ADDLW   1
    BTFSC   CCPR1L,1
    ADDLW   2
    BTFSC   CCPR1L,2
    ADDLW   4
    BTFSC   CCPR1L,3
    ADDLW   8
    CALL    valToHex
    CALL    rs232Tx
    RETURN
  

txReg   macro   valReg
    BANKSEL valReg
    MOVLW   0
    BTFSC   valReg,4
    ADDLW   1
    BTFSC   valReg,5
    ADDLW   2
    BTFSC   valReg,6
    ADDLW   4
    BTFSC   valReg,7
    ADDLW   8
    CALL    valToHex
    CALL    rs232Tx
    BANKSEL valReg
    MOVLW   0
    BTFSC   valReg,0
    ADDLW   1
    BTFSC   valReg,1
    ADDLW   2
    BTFSC   valReg,2
    ADDLW   4
    BTFSC   valReg,3
    ADDLW   8
    CALL    valToHex
    CALL    rs232Tx
    MOVLW   ' '
    CALL    rs232Tx
    ENDM


txCLRF:
    CALL    rs232Tx
    MOVLW   '\r'
    CALL    rs232Tx
    MOVLW   '\n'
    CALL    rs232Tx
    RETURN
