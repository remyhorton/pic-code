; txunit.asm - RF Buttons transmitter unit
; Copyright (C) Remy Horton, 2019.
; SPDX-License-Identifier: BSD-3-Clause

PROCESSOR 16f630
#include <p16f630.inc>
    RADIX dec
    ; Configuration registers
    __config _CONFIG, _FOSC_INTRCIO & _WDT_OFF

; Data memory
B1DATA      UDATA   0x20
count       RES     1
btnBits     RES     2
btnState    RES     1
btnOldState RES     1
btnToggled  RES     1
frame       RES     16

; Entrypoints
STARTUP     CODE    0x0000
    GOTO    main
    NOP     ; Unused
    NOP     ; Unused
    NOP     ; Unused
    NOP     ; Interrupt

MAINBODY    CODE   0x0005

setupRAM:
    ; Debouncing state
    CLRF    btnState
    CLRF    btnOldState
    CLRF    (btnBits + 0)
    CLRF    (btnBits + 1)
    ; Packet buffer
    MOVLW   0xfe        ; Tx prefix
    MOVWF   (frame + 0)
    MOVLW   0x05        ; Data length
    MOVWF   (frame + 1)
    MOVLW   0x1d        ; Node ID
    MOVWF   (frame + 2)
    MOVLW   0x00        ; Count 1
    MOVWF   (frame + 3)
    MOVLW   0x00        ; Count 2
    MOVWF   (frame + 4)
    MOVLW   0x00        ; Count 1+2
    MOVWF   (frame + 5)
    MOVLW   0xff        ; Check byte
    MOVWF   (frame + 6)
    RETURN

setupTimers:
    ; Timer0 (used for de-bouncing)
    BANKSEL OPTION_REG
    MOVLW   b'01010100'
    MOVWF   OPTION_REG
    ; Timer1 (used for bit Tx duration)
    BANKSEL T1CON
    MOVLW   b'00000001'
    MOVWF   T1CON
    ; Prescale 1:8
    ;BSF     T1CON,4
    ;BSF     T1CON,5
    RETURN

setupPins:
    BANKSEL CMCON
    MOVLW   b'111'
    MOVWF   CMCON
    BANKSEL TRISC
    BSF     TRISC,0
    BCF     TRISC,3
    BCF     TRISA,2
    BANKSEL PORTA
    CLRF    PORTA
    CLRF    PORTC
    RETURN


inputDebounce:
    ; Check whether time to next sample
    BANKSEL INTCON
    BTFSS   INTCON,2
    RETURN
    ; Shift up old samples
    BCF     STATUS,0
    RLF     (btnBits + 0),1
    BCF     STATUS,0
    RLF     (btnBits + 1),1
    ; Sample ports
    BANKSEL PORTA
    BTFSC   PORTA,4
    BSF     (btnBits + 0),0
    BTFSC   PORTA,5
    BSF     (btnBits + 1),0
    ; Check button 1 samples
    MOVFW   (btnBits + 0)
    XORLW   0xff
    BTFSC   STATUS,2
    BSF     btnState,0
    MOVFW   (btnBits + 0)
    ANDLW   0xff
    BTFSC   STATUS,2
    BCF     btnState,0
    ; Check button 2 samples
    MOVFW   (btnBits + 1)
    XORLW   0xff
    BTFSC   STATUS,2
    BSF     btnState,1
    MOVFW   (btnBits + 1)
    ANDLW   0xff
    BTFSC   STATUS,2
    BCF     btnState,1
    RETURN

inputCheck:
    MOVFW   btnState
    XORWF   btnOldState,0
    BTFSC   STATUS,2
    RETURN
    ; Something has changed...
    BANKSEL PORTA
    ANDWF   btnOldState,0   ; Mask out toggle Lo->Hi
    MOVWF   btnToggled
    BTFSC   btnToggled,0
    CALL    inputBtn0
    BTFSC   btnToggled,1
    CALL    inputBtn1
    MOVFW   btnState
    MOVWF   btnOldState
    RETURN

inputBtn0:
    INCF   (frame + 3),1
    INCF   (frame + 5),1
    GOTO    procTx

inputBtn1:
    INCF   (frame + 4),1
    INCF   (frame + 5),1
    GOTO    procTx

procTx:
    ; Calc XOR check byte
    BSF     PORTC,3
    MOVLW   0x00
    XORWF   (frame+2),0
    XORWF   (frame+3),0
    XORWF   (frame+4),0
    XORWF   (frame+5),0
    MOVWF   (frame+6)
    ; Tx frame bytes
    MOVLW   frame
    MOVWF   FSR
    MOVLW   7 
    MOVWF   count
        CALL    procTxByte
        INCF    FSR
        DECFSZ  count,1
        GOTO    $-3
    BCF     PORTC,3
    RETURN

procTxByte:
    BTFSS   INDF,7
    GOTO    $+3
    CALL    sendManc1
    GOTO    $+2
    CALL    sendManc0
    BTFSS   INDF,6
    GOTO    $+3
    CALL    sendManc1
    GOTO    $+2
    CALL    sendManc0
    BTFSS   INDF,5
    GOTO    $+3
    CALL    sendManc1
    GOTO    $+2
    CALL    sendManc0
    BTFSS   INDF,4
    GOTO    $+3
    CALL    sendManc1
    GOTO    $+2
    CALL    sendManc0
    BTFSS   INDF,3
    GOTO    $+3
    CALL    sendManc1
    GOTO    $+2
    CALL    sendManc0
    BTFSS   INDF,2
    GOTO    $+3
    CALL    sendManc1
    GOTO    $+2
    CALL    sendManc0
    BTFSS   INDF,1
    GOTO    $+3
    CALL    sendManc1
    GOTO    $+2
    CALL    sendManc0
    BTFSS   INDF,0
    GOTO    $+3
    CALL    sendManc1
    GOTO    $+2
    CALL    sendManc0
    RETURN

wait250us:
    BANKSEL T1CON
    BCF     T1CON,0
    SUBLW   0xff
    MOVWF   TMR1H
    MOVLW   0xff
    MOVWF   TMR1L
    BSF     T1CON,0
    BANKSEL PIR1
    BCF     PIR1,0
        BTFSS   PIR1,0
        GOTO    $-1
    RETURN

sendManc0:
    BANKSEL PORTA
    BSF     PORTA,2
    MOVLW   4
    CALL    wait250us
    BANKSEL PORTA
    BCF     PORTA,2
    MOVLW   4
    CALL    wait250us
    RETURN

sendManc1:
    BANKSEL PORTA
    BCF     PORTA,2
    MOVLW   4
    CALL    wait250us
    BANKSEL PORTA
    BSF     PORTA,2
    MOVLW   4
    CALL    wait250us
    RETURN

; main()
main:
    CALL    setupTimers
    CALL    setupPins
    CALL    setupRAM
    BANKSEL PORTA
loop:
    CALL    inputDebounce
    CALL    inputCheck
    GOTO    loop

    END     ; End of listing
