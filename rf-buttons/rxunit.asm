; txunit.asm - RF Buttons receiver unit
; Copyright (C) Remy Horton, 2019.
; SPDX-License-Identifier: BSD-3-Clause
PROCESSOR 16f1823
#include <p16f1823.inc>
    RADIX dec
    ; Configuration directives
    ; https://gputils.sourceforge.io/code-examples/configurations.html
    ; https://gputils.sourceforge.io/html-help/PIC16F1828-conf.html
    CONFIG FOSC=INTOSC, WDTE=OFF
    CONFIG MCLRE=ON
    CONFIG LVP=OFF

; Data memory
B1DATA      UDATA   0x20
state       RES     1   ; 0:locked 1:rise/fall 2:lastEdge 3:gotLen
deltas      RES     24
gapthresh   RES     2
bitcount    RES     1
bitdata     RES     1
datalen     RES     1
bytesleft   RES     1
temp        RES     1
index       RES     1
payload     RES     32

; Entrypoints
STARTUP     CODE    0x0000
    GOTO    entry
INTERRUPT   CODE    0x0004
    RETFIE  ; Interrupt

; Sub-modules
include "rs232.asm"
include "rxsetup.asm"
include "macros.asm"

entry
    CALL    setupClock
    CALL    setupPins
    CALL    setupRS232
    CALL    setupTimers
    CALL    edgeDeltasReset
    MOVLB   0           ; Select Bank 0
    GOTO    main

main:
    CLRF    state
loopMain:
    ; Expecting rising edge
    BANKSEL CCP1CON
    CLRF    CCP1CON
    MOVLW   b'00000101'
    MOVWF   CCP1CON
    BANKSEL PIR1
    BCF     PIR1,2
    BTFSS   PIR1,2
    GOTO    $-1
    BANKSEL CCP1CON
    CLRF    CCP1CON
    BANKSEL TMR1H
    CLRF    TMR1L
    CLRF    TMR1H
    BSF     state,1
    CALL    edgeToggle
    ; Expecting falling edge
    BANKSEL CCP1CON
    CLRF    CCP1CON
    MOVLW   b'00000100'
    MOVWF   CCP1CON
    BANKSEL PIR1
    BCF     PIR1,2
    BTFSS   PIR1,2
    GOTO    $-1
    BANKSEL CCP1CON
    CLRF    CCP1CON
    BANKSEL TMR1H
    CLRF    TMR1L
    CLRF    TMR1H
    BCF     state,1
    CALL    edgeToggle
    ; Complete cycle received. Repeat.
    GOTO    loopMain


edgeDeltasReset:
    MOVLW   0xff
    MOVWF   deltas+0
    MOVWF   deltas+1
    MOVWF   deltas+4
    MOVWF   deltas+5
    CLRF    deltas+2
    CLRF    deltas+3
    CLRF    deltas+6
    CLRF    deltas+7
    BTFSS   state,0
    RETURN
    ; If locked, unlock
    BCF     state,0
;    MOVLW   'R'
;    CALL    txCLRF
    RETURN

edgeToggle:
    ; Reset state if delta over 255
    BANKSEL CCPR1H
    MOVF    CCPR1H,0
    BTFSC   STATUS,2
    GOTO    $+4
    BANKSEL deltas
    CALL    edgeDeltasReset
    RETURN
    ; Reset if delta under 20
    BANKSEL CCPR1L
    IfRegOverL CCPR1L,0x20
    GOTO    $+4
    BANKSEL deltas
    CALL    edgeDeltasReset
    RETURN
    ; If locked onto signal, goto data decode
    SetBank0
    BTFSC   state,0
    GOTO    decodeData
decodePrefix
    ; Shift up previous pulses
    MOVLW   deltas+15
    MOVWF   FSR0
    MOVLW   deltas+16
    MOVWF   FSR1
    MOVLW   16
    MOVWF   index
        MOVFW   INDF0
        MOVWF   INDF1
        DECF    FSR0
        DECF    FSR1
        DECFSZ  index,1
        GOTO    $-5

    ; Read in delta
    BANKSEL CCPR1L
    MOVFW   CCPR1L
    BANKSEL deltas
    MOVWF   deltas+0
    ; For Lo->Hi no further processing
    BTFSC   state,1
    RETURN

    IfDiffOverL     deltas+1, deltas+3, 5
        RETURN
    IfDiffOverL     deltas+3, deltas+5, 5
        RETURN
    IfDiffOverL     deltas+2, deltas+4, 5
        RETURN
    IfDiffOverL     deltas+4, deltas+6, 5
        RETURN
    MOVFW   deltas+0
    MOVWF   temp
    BCF     STATUS,0
    RRF     temp,1
    IfDiffOverL     deltas+2, temp,     10
        RETURN
    ; Prefix detected. Go into locked state
;    MOVLW   'L'
;    CALL    rs232Tx
;    MOVLW   ' '
;    CALL    rs232Tx
    BSF     state,0
    BCF     state,2
    BCF     state,3
    CLRF    bitcount
    CLRF    bitdata
    MOVLW   payload
    MOVWF   FSR0
    ; Calc mark threshold
    MOVFW   deltas+2
    MOVWF   gapthresh+0
    LSRF    gapthresh+0,1
    ADDWF   gapthresh+0,1
    ; Calc space threshold
    MOVFW   deltas+1
    MOVWF   gapthresh+1
    LSRF    gapthresh+1,1
    ADDWF   gapthresh+1,1
    RETURN

decodeData
    BANKSEL CCPR1L
    MOVFW   CCPR1L
    BANKSEL deltas
    MOVWF   deltas
    ; Branch on edge type
    BTFSS   state,1
    GOTO    decodeDataFall
    GOTO    decodeDataRise
decodeDataRise
    IfFirstRegLT  deltas,gapthresh+1
    GOTO    decodeDataShort
    ; Rise after long gap -> 1
    LSLF    bitdata,1
    MOVLW   1
    IORWF   bitdata,1
    INCF    bitcount,1
    BCF     state,2
;    MOVLW   'H'
;    CALL    rs232Tx
    GOTO    decodeDataCheck
decodeDataFall
    IfFirstRegLT  deltas,gapthresh+0
    GOTO    decodeDataShort
    ; Fall after long gap -> 1
    LSLF    bitdata,1
    INCF    bitcount,1
    BCF     state,2
;    MOVLW   'L'
;    CALL    rs232Tx
    GOTO    decodeDataCheck
decodeDataShort
    ; Skip if this is border edge
    BTFSC   state,2
    GOTO $+3
    BSF     state,2
    RETURN
    ; Non-border edge
    BCF state,2
    LSLF    bitdata,1
    INCF    bitcount,1
    MOVLW   1
    BTFSC   state,1     ; Skip if falling
    IORWF   bitdata,1   ; Set Data LSB
;    MOVLW   'l'
;    BTFSC   state,1     ; Skip if falling
;    MOVLW   'h'
;    CALL    rs232Tx
decodeDataCheck
    ; Check to see if 8 bits have been received
    IfRegLeL  bitcount,7
    RETURN      ; Less than 8 bits shifted in
    ; Got full 8 bits
;    MOVLW   'B'
;    CALL    rs232Tx
    BTFSC   state,3
    GOTO    decodeDataPayload
    ; Is first byte (i.e. length header)
    BSF     state,3
    MOVFW   bitdata
    MOVWF   bytesleft
    MOVWF   datalen
    CLRF    bitcount
    CLRF    bitdata
    RETURN
decodeDataPayload:
;    txReg   bitdata
    MOVFW   bitdata
    MOVWF   INDF0
    INCF    FSR0,1
    CLRF    bitcount
    CLRF    bitdata
    DECFSZ  bytesleft,1
    RETURN
    ; All payload read. Check sum byte
    MOVLW   payload
    MOVWF   FSR0
    MOVFW   datalen
    MOVWF   bytesleft
    DECF    bytesleft,1 ; omit check from sum
    MOVLW   0
        XORWF   INDF0,0
        INCF    FSR0,1
        DECFSZ  bytesleft,1
        GOTO    $-3
    MOVWF   temp
    SUBWF   INDF0,0
    BTFSC   STATUS,2    ; Z bit
    GOTO    decodeDataOk
    txReg   temp
    txReg   INDF0
    MOVLW   'E'
    CALL    txCLRF
    CALL    edgeDeltasReset
    RETURN
decodeDataOk
    ; print data
    MOVLW   'D'
    CALL    rs232Tx
    MOVLW   ' '
    CALL    rs232Tx
    MOVLW   payload
    MOVWF   FSR0
    MOVFW   datalen
    MOVWF   bytesleft
    DECF    bytesleft,1
    txReg   bytesleft
decodeDataTx
        txReg   INDF0
        INCF    FSR0,1
        DECFSZ  bytesleft,1
        GOTO    decodeDataTx
    MOVLW   ';'
    CALL    txCLRF
    CALL    edgeDeltasReset
    RETURN

    END
