; macros - Assembly macros
; Copyright (C) Remy Horton, 2019
; SPDX-License-Identifier: BSD-3-Clause



; STATUS
; 0 - C 
; 1 - DC
; 2 - Z
; 3 -
; 4 -


SetBank0    macro
    BANKSEL deltas
    MOVLB   0
    ENDM


IfDiffOverL macro   valA,valB,valLimit
    local   noborrow
    MOVFW   valA
    SUBWF   valB,0
    BTFSC   STATUS,0    ; Skip if W bigger (was borrow)
    GOTO    noborrow
    MOVFW   valB
    SUBWF   valA,0
noborrow
    ; W = abs( diff(A,B) )
    SUBLW   valLimit
    BTFSS   STATUS,0
    ;   Skipped if diff(A,B) <= limit
    ENDM


;IfRegUnderL macro   valReg,valLimit
;    MOVFW   valReg
;    SUBLW   valLimit
;    BTFSC   STATUS,0    ;   Skip if W bigger (borrow)
;    ENDM

IfRegLeL macro   valReg,valLimit
    MOVFW   valReg
    SUBLW   valLimit
    BTFSC   STATUS,0    ;   Skip if W bigger (borrow)
    ENDM

IfRegOverL macro   valReg,valLimit
    MOVFW   valReg
    SUBLW   valLimit
    BTFSS   STATUS,0    ;   Skip if W smaller (no borrow)
    ENDM

IfFirstRegLT macro  valRegA,valRegB
    MOVFW   valRegB
    SUBWF   valRegA,0
    BTFSS   STATUS,0    ; Skip if W not bigger
    ENDM

IfRegsEq    macro   valRegA,valRegB
    MOVFW   valRegA
    SUBWF   valRegB,0
    BTFSC   STATUS,2    ; Skip if non-zero
    ENDM

IfRegsDiff  macro   valRegA,valRegB
    MOVFW   valRegA
    SUBWF   valRegB,0
    BTFSS   STATUS,2    ; Skip if zero
    ENDM

IfRegEqL    macro   valReg,valConst
    MOVLW   valConst
    SUBWF   valReg,0
    BTFSC   STATUS,2    ; Skip if non-zero
    ENDM
