; rxsetup.asm - Startup routines
; Copyright (C) Remy Horton, 2019.
; SPDX-License-Identifier: BSD-3-Clause

setupClock:
    ; Datasheet P65 0011 1x00
    ; ABBBBxCCC
    ; +--------- SPLLEN
    ;  ++++----- IRCFFOSC
    ;       +++  SCS
    BANKSEL OSCCON
;    MOVLW   b'00111000' ; 500kHz MF (default)
;    MOVLW   b'01011000' ; 1MHz
;    MOVLW   b'01100000' ; 2MHz
    MOVLW   b'01101000' ; 4MHz
;    MOVLW   b'01110000' ; 8MHz
;    MOVLW   b'01111010' ; 16MHz
    MOVWF   OSCCON
    RETURN

setupPins:
    BANKSEL ANSELA
    CLRF    ANSELA
    CLRF    ANSELC
    BANKSEL TRISC
    BSF     TRISC,0 ; SDA
    BSF     TRISC,1 ; SCL
    BCF     TRISC,4 ; Tx
    BSF     TRISC,5 ; Data In
    RETURN


setupTimers:
    ; Timer0 (used for de-bouncing)
    BANKSEL OPTION_REG
    MOVLW   b'01010100'
    MOVWF   OPTION_REG
    ; Timer1
    ; aabb001
    ; 00       FOsc/4 instruction clock
    ; 01       FOsc system clock
    BANKSEL T1CON
    MOVLW   b'00000001'
    MOVWF   T1CON
    ; Prescale 1:8
    BSF     T1CON,4
    BSF     T1CON,5
    RETURN

setupCapture:
    MOVLW   b'1011'

