; 17seg4.asm - Four module 17-segment display
; (c) Remy Horton, 2018
; SPDX-

    ;list    p=16f1823
    RADIX dec
    PROCESSOR 16f1823
    #include <p16f1823.inc>
    __config _CONFIG1, _FOSC_INTOSC & _WDTE_OFF & _MCLRE_OFF & _PWRTE_OFF & _CP_OFF & _CPD_OFF;
    __config _CONFIG2, _LVP_OFF


; Uninitialised data
B1DATA      UDATA   0x30
segments    RES     16
idx         RES     1
idxLitSeg   RES     1
padd        RES     14
valChipLo   RES     4
valChipHi   RES     4


STARTUP CODE    0x0000
    goto main

Interrupt:
    CODE 0x0004
    nop

; opcodes p311
#include "consts.asm"


setupTimer:
    ; Timer2
    BANKSEL T2CON
    MOVLW   b'00000100'
    MOVWF   T2CON
    MOVLW   0x80
    MOVWF   PR2
    RETURN

setupPins:
    ; Set pins to all digital
    BANKSEL ANSELA
    CLRF    ANSELA
    BANKSEL ANSELC
    CLRF    ANSELC
    ; Port A  01:Out rest:Unused
    MOVLW   b'11111100'
    BANKSEL TRISA
    MOVWF   TRISA
    ; Port C 01:I2C 2345:Output
    MOVLW   b'11000011'
    BANKSEL TRISC
    MOVWF   TRISC
    ; Clear ports (both Bank 0)
    BANKSEL PORTA
    CLRF    PORTA
    CLRF    PORTC
    RETURN

setupI2C:
    MOVLW   b'00110110'
    BANKSEL SSP1CON1
    MOVWF   SSP1CON1
    BSF     SSP1CON2,0  ; Clock stretch
    BSF     SSP1CON3,0  ; Data-hold
    MOVLW   0x02
    MOVWF   SSP1ADD
    RETURN


segsClear:
    BANKSEL segments
    MOVLW   segments
    MOVWF   FSR0L
    MOVLW   16
    MOVWF   idx
segsClearLoop
    CLRF    INDF0
    INCF    FSR0L,1
    DECFSZ  idx,1
    GOTO    segsClearLoop
    RETURN

segsOff:
    BSF     PORTC,2
    BSF     PORTC,3
    BSF     PORTC,4
    BSF     PORTC,5
    RETURN

segsSetRemy:
    MOVLW   ('R' - 'A')
    CALL    LoBitsLet
    MOVWF   (valChipLo + 0)
    MOVLW   ('R' - 'A')
    CALL    HiBitsLet
    MOVWF   (valChipHi + 0)
    MOVLW   ('E' - 'A')
    CALL    LoBitsLet
    MOVWF   (valChipLo + 1)
    MOVLW   ('E' - 'A')
    CALL    HiBitsLet
    MOVWF   (valChipHi + 1)
    MOVLW   ('m' - 'a')
    CALL    LoBitsLet
    MOVWF   (valChipLo + 2)
    MOVLW   ('m' - 'a')
    CALL    HiBitsLet
    MOVWF   (valChipHi + 2)
    MOVLW   ('y' - 'a')
    CALL    LoBitsLet
    MOVWF   (valChipLo + 3)
    MOVLW   ('y' - 'a')
    CALL    HiBitsLet
    MOVWF   (valChipHi + 3)
    RETURN


segsFlip:
    ; All GP registers under 0xff so no need to set FSRxH
    ; Chip 1
    MOVLW   valChipLo
    MOVWF   FSR0L
    MOVLW   segments
    MOVWF   FSR1L
    MOVLW   0x04
    CALL    segsFlipCopyBits
    MOVLW   valChipHi
    MOVWF   FSR0L
    MOVLW   0x04
    CALL    segsFlipCopyBits
    ; Chip 2
    MOVLW   valChipLo + 1
    MOVWF   FSR0L
    MOVLW   segments
    MOVWF   FSR1L
    MOVLW   0x08
    CALL    segsFlipCopyBits
    MOVLW   valChipHi + 1
    MOVWF   FSR0L
    MOVLW   0x08
    CALL    segsFlipCopyBits
    ; Chip 3
    MOVLW   valChipLo + 2
    MOVWF   FSR0L
    MOVLW   segments
    MOVWF   FSR1L
    MOVLW   0x10
    CALL    segsFlipCopyBits
    MOVLW   valChipHi + 2
    MOVWF   FSR0L
    MOVLW   0x10
    CALL    segsFlipCopyBits
    ; Chip 4
    MOVLW   valChipLo + 3
    MOVWF   FSR0L
    MOVLW   segments
    MOVWF   FSR1L
    MOVLW   0x20
    CALL    segsFlipCopyBits
    MOVLW   valChipHi + 3
    MOVWF   FSR0L
    MOVLW   0x20
    CALL    segsFlipCopyBits
    RETURN
segsFlipCopyBits
    ; If bit in FSR0 set, add W to corresponding FSR1
    BTFSC   INDF0,0
    IORWF   INDF1,1
    INCF    FSR1L
    BTFSC   INDF0,1
    IORWF   INDF1,1
    INCF    FSR1L
    BTFSC   INDF0,2
    IORWF   INDF1,1
    INCF    FSR1L
    BTFSC   INDF0,3
    IORWF   INDF1,1
    INCF    FSR1L
    BTFSC   INDF0,4
    IORWF   INDF1,1
    INCF    FSR1L
    BTFSC   INDF0,5
    IORWF   INDF1,1
    INCF    FSR1L
    BTFSC   INDF0,6
    IORWF   INDF1,1
    INCF    FSR1L
    BTFSC   INDF0,7
    IORWF   INDF1,1
    INCF    FSR1L
    RETURN


pulseReset:
    BSF PORTA,1
    BCF PORTA,1
    RETURN

pulseIncrement:
    BSF PORTA,0
    BCF PORTA,0
    RETURN


main:
    CALL    setupTimer
    CALL    setupPins
    CALL    setupI2C
    CALL    segsClear
    CALL    segsSetRemy
    CALL    segsFlip

    CALL    pulseReset
    CLRF    idxLitSeg
loopMain:

    BCF     PIR1,1
    BTFSS   PIR1,1
    GOTO    $-1

    CALL    segsOff
    ; 
    MOVLW   0x10
    SUBWF   idxLitSeg,0
    BTFSS   STATUS,2
    BRA     $+4
    CALL    pulseReset
    CLRF    idxLitSeg
    BRA     $+3
    CALL    pulseIncrement
    INCF    idxLitSeg
    ; 
    MOVLW   segments
    MOVWF   FSR1L
    MOVFW   idxLitSeg
    ADDWF   FSR1L,1
    MOVLW   0xff
    XORWF   INDF1,0
    MOVWF   PORTC

    GOTO    loopMain



    END
