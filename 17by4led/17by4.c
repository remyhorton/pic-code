#define NO_BIT_DEFINES
#include "pic16f1823.h"

/* Configuration word 1 */
__code short __at (_CONFIG1) cfg0 =
	_FOSC_INTOSC
    & _WDTE_OFF  // No watchdog timer
    & _MCLRE_ON  // Reset button enabled
    & _PWRTE_OFF // No power-up timer
    & _CP_OFF & _CPD_OFF; // No code/data protection

/* Configuration word 2
   _LVP_OFF is required by _MCLRE_OFF (Section 7.3 p75) */
__code short __at (_CONFIG2) cfg1 =
    _LVP_OFF;   // Low-voltage programming off

#include "segments.h"
#define USE_ASM


void clockSetup(void)
{
//OSCCON = 0b00101000; /* Fosc = 125kHz */
//OSCCON = 0b00110000; /* Fosc = 250kHz */
////OSCCON = 0b00111000; /* Fosc = 500kHz (default) */
//OSCCON = 0b01011000; /* Fosc = 1MHz */
}

void timerSetup(void)
{
/* Timer2 setup (~2.5ms) */
T2CON = 0b00000100; // 1:1
PR2 = 128;
//0x80;
}

// 180 can see..
// 156
// 146 OK
//

void pinSetup(void)
{
PORTA = 0;
PORTC = 0;
TRISA = 0b00111100; /* RA0-1 output; RA2-5 unused */
TRISC = 0b00000011; /* RC0-1 I2C; rest output */
ANSELA=0;
ANSELC=0;
}

void i2cSetup(void)
{
/* Upper nibble xx1x => Enable MSSP
   Lower nibble 0110 => I2C Slave */
SSP1CON1 = 0b00110110;
SSP1CON2 = 0b1;   // Enable clock stretching
SSP1CON3 = 0b1;   // Enable data-hold
SSP1ADD = 0x02;
}


unsigned char segs[25];

void segsClear(void)
{
//unsigned char idx;
//for(idx=0;idx<17;idx++)
//    segs[idx] = 0;

segs[0] = 0;
segs[1] = 0;
segs[2] = 0;
segs[3] = 0;
segs[4] = 0;
segs[5] = 0;
segs[6] = 0;
segs[7] = 0;
segs[8] = 0;
segs[9] = 0;
segs[10] = 0;
segs[11] = 0;
segs[12] = 0;
segs[13] = 0;
segs[14] = 0;
segs[15] = 0;
segs[16] = 0;
}

void segsOff(void)
{
#ifdef USE_ASM
__asm
    BANKSEL _PORTC
    BSF     _PORTC,2
    BSF     _PORTC,3
    BSF     _PORTC,4
    BSF     _PORTC,5
__endasm;
#else
// 10
PORTC |= 0b00111100;
#endif
}

void segsFlip(void)
{
unsigned char mask;
unsigned char idxSeg;
// 316

mask = 1;
for(idxSeg=0;idxSeg<8;idxSeg++)
    {
    if(segs[17] & mask)
        segs[idxSeg] |= 0b00000100;
    if(segs[19] & mask)
        segs[idxSeg] |= 0b00001000;
    if(segs[21] & mask)
        segs[idxSeg] |= 0b00010000;
    if(segs[23] & mask)
        segs[idxSeg] |= 0b00100000;
    mask <<=  1;
    }
mask = 1;
for(idxSeg=8;idxSeg<16;idxSeg++)
    {
    if(segs[18] & mask)
        segs[idxSeg] |= 0b00000100;
    if(segs[20] & mask)
        segs[idxSeg] |= 0b00001000;
    if(segs[22] & mask)
        segs[idxSeg] |= 0b00010000;
    if(segs[24] & mask)
        segs[idxSeg] |= 0b00100000;
    mask <<=  1;
    }
}

void pulseIncr(void)
{
#ifdef USE_ASM
__asm
    BANKSEL _PORTA
    BSF     _PORTA,0
    BCF     _PORTA,0
__endasm;
#else
// 10
PORTA |= 0x01;
PORTA &= 0xfe;
#endif
}

void pulseReset(void)
{
#ifdef USE_ASM
__asm
    BANKSEL _PORTA
    BSF     _PORTA,1
    BCF     _PORTA,1
__endasm;
#else
// 10
PORTA |= 0x02;
PORTA &= 0xfd;
#endif
}


void main(void)
{
unsigned char idxLitSegs;
unsigned char valByte;
unsigned char idxWrite;

clockSetup();
timerSetup();
pinSetup();
i2cSetup();

segsClear();

segs[17] = LoBitsLet['R'-'A'];
segs[18] = HiBitsLet['R'-'A'];

segs[19] = LoBitsLet['E'-'A'];
segs[20] = HiBitsLet['E'-'A'];

segs[21] = LoBitsLet['M'-'A'];
segs[22] = HiBitsLet['M'-'A'];

segs[23] = LoBitsLet['Y'-'A'];
segs[24] = HiBitsLet['Y'-'A'];

segsFlip();



idxLitSegs = 0;
pulseReset();
while(1)
    {
    segsOff();
    if(idxLitSegs == 16)
        {
        pulseReset();
        idxLitSegs = 0;
        }
    else
        {
        pulseIncr();
        idxLitSegs++;
        }
    PORTC ^= segs[idxLitSegs];

    PIR1 &= ~0x02;
    while( !(PIR1 & 0x02) ) //|| (PIR1 & 0x08) )
        {

        /* Handle incoming I2C transactions */
        if(PIR1 & 0x08)
            {
            PIR1 &= ~0x08;

            /* Write to master not supported */ 

            /* Read bytes from master while bus is busy */
            while(SSP1STAT & 0b1)
                { 
                valByte = SSP1BUF;
                if(SSP1STAT & 0b100000)
                    {
                    /* Data byte. Add to buffer */
                    if(idxWrite < 25)
                        {
                        segs[idxWrite] = valByte;
                        idxWrite++;
                        }
                    if(idxWrite == 25)
                        {
                        segsClear();
                        segsFlip();
                        }
                    }
                else
                    {
                    /* Address byte. Reset offset */
                    idxWrite = 17;
                    }
                SSP1CON2 &= ~0x20;
                SSP1CON1 |=  0x10;
                }
            }
        }
    }
}
