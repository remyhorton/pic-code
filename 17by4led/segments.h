

/* Segment bit-masks */
#define SEG_F  0x01
#define SEG_G  0x02
#define SEG_T  0x04
#define SEG_S  0x08
#define SEG_R  0x10
#define SEG_D  0x20
#define SEG_E  0x40
#define SEG_U  0x80

#define SEG_B  0x01
#define SEG_N  0x02
#define SEG_C  0x04
#define SEG_P  0x08
#define SEG_K  0x10
#define SEG_M  0x20
#define SEG_A  0x40
#define SEG_H  0x80


const unsigned char LoBitsNum[10] = {
    SEG_F|SEG_G|SEG_T|SEG_D|SEG_E,
    SEG_D,
    SEG_F|SEG_G|SEG_E|SEG_U,
    SEG_F|SEG_D|SEG_E|SEG_U,
    SEG_D|SEG_U,
    SEG_F|SEG_R|SEG_E|SEG_U,
    SEG_F|SEG_G|SEG_D|SEG_E|SEG_U,
    SEG_D,
    SEG_F|SEG_G|SEG_D|SEG_E|SEG_U,
    SEG_F|SEG_D|SEG_E|SEG_U,
};

const unsigned char HiBitsNum[10] = {
    SEG_B|SEG_N|SEG_C|SEG_A|SEG_H,
    SEG_C,
    SEG_B|SEG_C|SEG_P|SEG_A,
    SEG_B|SEG_C|SEG_P|SEG_A,
    SEG_C|SEG_P|SEG_H,
    SEG_B|SEG_A|SEG_H,
    SEG_B|SEG_P|SEG_A|SEG_H,
    SEG_B|SEG_C|SEG_A,
    SEG_B|SEG_C|SEG_P|SEG_A|SEG_H,
    SEG_B|SEG_C|SEG_P|SEG_A|SEG_H
};

const unsigned char LoBitsLet[26] = {
    SEG_G|SEG_D|SEG_U,
    SEG_F|SEG_S|SEG_D|SEG_E,
    SEG_F|SEG_G|SEG_E,
    SEG_F|SEG_S|SEG_D|SEG_E,
    SEG_F|SEG_G|SEG_E|SEG_U,
    SEG_G|SEG_U,
    SEG_F|SEG_G|SEG_D|SEG_E,
    SEG_G|SEG_D|SEG_U,
    SEG_F|SEG_S|SEG_E,
    SEG_F|SEG_G|SEG_D|SEG_E,
    SEG_G|SEG_R|SEG_U,
    SEG_F|SEG_G|SEG_E,
    SEG_G|SEG_D,
    SEG_G|SEG_R|SEG_D,
    SEG_F|SEG_G|SEG_D|SEG_E,
    SEG_G|SEG_U,
    SEG_F|SEG_G|SEG_R|SEG_D|SEG_E,
    SEG_G|SEG_R|SEG_U,
    SEG_D|SEG_E|SEG_U,
    SEG_S,
    SEG_F|SEG_G|SEG_D|SEG_E,
    SEG_G|SEG_T,
    SEG_G|SEG_T|SEG_R|SEG_D,
    SEG_T|SEG_R,
    SEG_S|SEG_U,
    SEG_F|SEG_T|SEG_E,
};

const unsigned char HiBitsLet[26] = {
    SEG_B|SEG_C|SEG_P|SEG_A|SEG_H,
    SEG_B|SEG_C|SEG_P|SEG_M|SEG_A,
    SEG_B|SEG_A|SEG_H,
    SEG_B|SEG_C|SEG_M|SEG_A,
    SEG_B|SEG_P|SEG_A|SEG_H,
    SEG_B|SEG_P|SEG_A|SEG_H,
    SEG_B|SEG_P|SEG_A|SEG_H,
    SEG_C|SEG_P|SEG_H,
    SEG_B|SEG_M|SEG_A,
    SEG_C,
    SEG_N|SEG_H,
    SEG_H,
    SEG_N|SEG_C|SEG_K|SEG_H,
    SEG_C|SEG_K|SEG_K,
    SEG_B|SEG_C|SEG_A|SEG_H,
    SEG_B|SEG_C|SEG_P|SEG_A|SEG_H,
    SEG_B|SEG_C|SEG_A|SEG_H,
    SEG_B|SEG_C|SEG_P|SEG_A|SEG_H,
    SEG_B|SEG_P|SEG_A|SEG_H,
    SEG_B|SEG_M|SEG_A,
    SEG_C|SEG_H,
    SEG_N|SEG_H,
    SEG_C|SEG_H,
    SEG_N|SEG_K,
    SEG_C|SEG_P|SEG_H,
    SEG_B|SEG_N|SEG_A,
};

