Microchip PIC firmware
======================

This is a collection of firmware mini-projects that target Microchip PIC
microcontrollers, mainly the ``PIC16F62`` & ``PIC16F88`` and more recently
the ``PIC12F1822/PIC16F182x`` family, which I have used within various
circuits I have built. Significant portions of the code here were already
publicly available on my [electronics website][elec], and for cross-reference
purposes decided that putting all the code into a public repository simplified
things. Note that some of the code here may be work-in-progress.

* **``debounce``:**   _Button press de-bouncer_ ([see article][debounce])
* **``rs232-lcd``:**  _RS232-driven LCD display_  ([see article][rs232-lcd])
* **``i2c-master``:** Serial-driven I2C master device_ ([see article][i2c-master])
* **``i2c-slave``:**  _I2C register-enabled slave_  ([see article][i2c-slave])
* **``17-seg-led``:** _4-way 17-Segment LED display_ ([see article][17-seg-led])
* **``17by4led``:**   _4-way 17-segment LED display_ ([see article][17by4led])
* **``ledmatrix``:** _LED matrix tile_  ([see article][ledmatrix])
* **``lcd-128x64``:** _LCD dot-matrix display_  ([see article][lcd-128x64])
* **``lcd-timer``:** _LCD-based Timer_  ([see article][lcd-timer])
* **``rf-buttons``**: Wireless buttons
* **``relay``**: RS232-controlled power switch  ([see article][relay])
* **``thermo``**: RS232-based thermocouple interface  ([see article][thermo])
* **``triac``**: AC power control using a Triac  ([see article][triac])
* **``i2c-slave-rev2``**: I2C master device (2nd revision)
* **``i2c-master-rev2``**: I2C slave device (2nd revision)
* **``scripts``:** _Various scripts_

[elec]: http://www.remy.org.uk/elec.php
[17-seg-led]: http://www.remy.org.uk/elec.php?elec=1524265200
[debounce]:   http://www.remy.org.uk/elec.php?elec=1524783600
[i2c-master]: http://www.remy.org.uk/elec.php?elec=1540681200
[i2c-slave]:  http://www.remy.org.uk/elec.php?elec=1536966000
[17by4led]:   http://www.remy.org.uk/elec.php?elec=1543104000
[lcd-128x64]: http://www.remy.org.uk/elec.php?elec=1537138800
[ledmatrix]:  http://www.remy.org.uk/elec.php?elec=1543017600
[rs232-lcd]:  http://www.remy.org.uk/elec.php?elec=1512086400
[lcd-timer]:  http://www.remy.org.uk/elec.php?elec=1549756800
[triac]:      http://www.remy.org.uk/elec.php?elec=1599260400
[relay]:      http://www.remy.org.uk/elec.php?elec=1596841200
[thermo]:     http://www.remy.org.uk/elec.php?elec=1596150000