;   rs232.asm - Serial interface routines
;   Copyright (C) Remy Horton, 2019-2021.
;   SPDX-License-Identifier: BSD-3-Clause

#if 0
rs232WriteHex:
    BANKSEL rs232byte
    MOVWF   rs232byte
    SWAPF   rs232byte,1
    MOVLW   0x0f
    ANDWF   rs232byte,0
    CALL    rs232WriteNibble
    BANKSEL rs232byte
    SWAPF   rs232byte,1
    MOVLW   0x0f
    ANDWF   rs232byte,0
    CALL    rs232WriteNibble
    RETURN

rs232WriteNibble:
    BANKSEL TXSTA
    BTFSS   TXSTA,1
    GOTO $-1
    CALL    lookupHexChar
    BANKSEL TXREG
    MOVWF   TXREG
    RETURN

lookupHexChar:
    BRW
    RETLW   '0'
    RETLW   '1'
    RETLW   '2'
    RETLW   '3'
    RETLW   '4'
    RETLW   '5'
    RETLW   '6'
    RETLW   '7'
    RETLW   '8'
    RETLW   '9'
    RETLW   'a'
    RETLW   'b'
    RETLW   'c'
    RETLW   'd'
    RETLW   'e'
    RETLW   'f'
#endif

rs232WriteNL
    MOVLW   '\n'
    CALL    rs232Write
    MOVLW   '\r'
    CALL    rs232Write
    RETURN

rs232Write
    BANKSEL TXSTA
    BTFSS   TXSTA,1
    GOTO $-1
    BANKSEL TXREG
    MOVWF   TXREG
    RETURN

rs232Flush:
    ; Set timeout timer
    BANKSEL PIR1
    BCF     PIR1,1
    CLRF    TMR2
    MOVLW   30
    MOVWF   PR2
    MOVLW   b'00000111' ; 62.5kHz
    MOVWF   T2CON
    ; Purge loop
    BTFSC   PIR1,5
    GOTO    $+4     ; Data waiting
    BTFSC   PIR1,1
    GOTO    $+6     ; Timeout
    GOTO    $-4
    ; Flush byte
    MOVFW   RCREG
    BCF     PIR1,5
    CLRF    TMR2
    GOTO    $-8
    ; Turn timer2 off
    BCF     T2CON,2
    RETURN

