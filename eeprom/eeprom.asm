;   eeprom.asm - Atmel EEPROM programmer
;   Copyright (C) Remy Horton, 2021.
;   SPDX-License-Identifier: BSD-3-Clause

PROCESSOR 16f1829
#include <p16f1829.inc>
    RADIX dec
    CONFIG FOSC=INTOSC, WDTE=OFF
    CONFIG MCLRE=ON
    CONFIG LVP=OFF
    CONFIG PLLEN=OFF

BANK0DATA       UDATA   0x20
buffer          RES     80

SHAREDDATA      UDATA   0x70
memaddr         RES     1
celladdr
lenBuffer       RES     1
lenExpected     RES     1

; Entrypoints
STARTUP     CODE    0x0000
    GOTO    entry
    GOTO    $

#include "i2c.asm"
#include "setup.asm"
#include "rs232.asm"

entry:
    CALL    setupClock
    CALL    setupPins
    CALL    setupRS232
    CALL    setupI2C
    BANKSEL OPTION_REG
    BCF     OPTION_REG,5
begin:
    CALL    rs232Flush
    MOVLW   'O'
    CALL    rs232Write
    CALL    ledOff
    MOVLW   'k'
    CALL    rs232Write
mainstart:
    ; Setup memory pointers
    MOVLW   0x20
    MOVWF   FSR0H
    MOVWF   FSR1H
    CLRF    FSR0L
    CLRF    FSR1L
    CLRF    lenBuffer
mainloop:
    ; Busy wait for RS232
    BANKSEL PIR1
    BTFSS   PIR1,5
    GOTO    mainloop
    BCF     PIR1,5
    ; Read byte and store
    BANKSEL RCREG
    MOVFW   RCREG
    MOVWI   FSR0++
    INCF    lenBuffer,1
    ; Is data byte (first is command)?
    MOVLW   1
    SUBWF   lenBuffer,0
    BTFSS   STATUS,2
    GOTO    recvdata
    ; Is write command?
    MOVIW   INDF1
    SUBLW   'W'
    BTFSC   STATUS,2
    GOTO    recvW
    ; Is read command?
    MOVIW   INDF1
    SUBLW   'R'
    BTFSC   STATUS,2
    GOTO    recvR
    ; Is flash command?
    MOVIW   INDF1
    SUBLW   'F'
    BTFSC   STATUS,2
    GOTO    recvF
    ; Is dump command?
    MOVIW   INDF1
    SUBLW   'D'
    BTFSC   STATUS,2
    GOTO    recvD
    ; Is load command?
    MOVIW   INDF1
    SUBLW   'L'
    BTFSC   STATUS,2
    GOTO    recvL
    ; Is store command?
    MOVIW   INDF1
    SUBLW   'S'
    BTFSC   STATUS,2
    GOTO    recvS
    ; Unknown command
    MOVLW   '?'
    CALL    rs232Write
    GOTO    mainstart

recvW:
    MOVLW   'w'
    CALL    rs232Write
    MOVLW   64
    MOVWF   lenExpected
    GOTO    mainloop
recvR:
    MOVLW   'r'
    CALL    rs232Write
    MOVLW   64
    MOVWF   lenExpected
    MOVIW   FSR0++
    CALL    rs232Write
    DECFSZ  lenExpected,1
    GOTO    $-3
    GOTO    mainstart
recvF:
    MOVLW   'f'
    CALL    rs232Write
    MOVLW   1
    MOVWF   lenExpected
    GOTO    mainloop
recvD:
    MOVLW   'd'
    CALL    rs232Write
    MOVLW   1
    MOVWF   lenExpected
    GOTO    mainloop
recvL:
    MOVLW   'l'
    CALL    rs232Write
    MOVLW   2
    MOVWF   lenExpected
    GOTO    mainloop
recvS:
    MOVLW   's'
    CALL    rs232Write
    MOVLW   3
    MOVWF   lenExpected
    GOTO    mainloop


; Receiving data part of command.
recvdata:
    DECFSZ  lenExpected,1
    GOTO    mainloop
    ; Was data upload?
    MOVLW   'W'
    SUBWF   INDF1,0
    BTFSC   STATUS,2
    GOTO    mainstart
    ; Was Dump?
    MOVLW   'D'
    SUBWF   INDF1,0
    BTFSC   STATUS,2
    GOTO    doDump
    ; Was Flash?
    MOVLW   'F'
    SUBWF   INDF1,0
    BTFSC   STATUS,2
    GOTO    doFlash
    ; Was Load?
    MOVLW   'L'
    SUBWF   INDF1,0
    BTFSC   STATUS,2
    GOTO    doLoad
    ; Was Store?
    MOVLW   'S'
    SUBWF   INDF1,0
    BTFSC   STATUS,2
    GOTO    doStore
    ; Unimplemented command
    MOVFW   INDF1
    CALL    rs232Write
    MOVLW   '!'
    CALL    rs232Write
    CALL    rs232WriteNL
    GOTO    mainstart


doDump:
    MOVLW   65
    MOVWF   lenExpected
    CLRF    memaddr
    ; Set page address
    CALL    i2cWaitIdle
    CALL    i2cStartWait
    MOVLW   0x4e
    CALL    i2cSendWait
    MOVIW   -1[FSR0]
    CALL    i2cSendWait
    CALL    i2cStopWait
    MOVLW   1
    MOVWF   FSR1L
dumpLoop:
    DECFSZ  lenExpected,1
    GOTO    $+2
    GOTO    dumpDone
    CALL    pulseMem
    BANKSEL PORTA
    BCF     PORTA,5
    BANKSEL PORTC
    MOVFW   PORTC
    MOVWI   INDF1
    INCF    FSR1L
    BANKSEL PORTA
    BSF     PORTA,5
    INCF    memaddr,1
    GOTO    dumpLoop
dumpDone:
    MOVLW   '.'
    CALL    rs232Write
    CALL    ledOff
    GOTO    mainstart

doFlash:
    MOVLW   64
    MOVWF   lenExpected
    CLRF    memaddr
    ; Set page address
    CALL    i2cWaitIdle
    CALL    i2cStartWait
    MOVLW   0x4e
    CALL    i2cSendWait
    MOVIW   -1[FSR0]
    CALL    i2cSendWait
    CALL    i2cStopWait
    MOVLW   1
    MOVWF   FSR1L
    ; Set data pins to write
    BANKSEL TRISC
    CLRF    TRISC
flashLoop:
    CALL    pulseMem
    MOVIW   INDF1
    BANKSEL PORTC
    MOVWF   PORTC
    BANKSEL PORTA
    BCF     PORTA,4
    BANKSEL PORTA
    BSF     PORTA,4
    INCF    FSR1L
    INCF    memaddr,1
    DECFSZ  lenExpected,1
    GOTO    flashLoop
flashDone:
    ; Tristate pins
    MOVLW   0xff
    BANKSEL TRISC
    MOVWF   TRISC
    CALL    delay150us
    MOVLW   '.'
    CALL    rs232Write
    CALL    ledOff
    GOTO    mainstart

    MOVLW   '!'
    CALL    rs232Write
    GOTO    mainstart




doLoad:
    ; Setup address lines
    CALL    i2cWaitIdle
    CALL    i2cStartWait
    MOVLW   0x4e
    CALL    i2cSendWait
    MOVIW   -2[FSR0]
    ;IORLW   0x80
    CALL    i2cSendWait
    CALL    i2cStopWait
    MOVIW   -1[FSR0]
    MOVWF   memaddr
    CALL    pulseMem
    ; Read
    BANKSEL PORTA
    BCF     PORTA,5
    BANKSEL PORTC
    MOVFW   PORTC
    BANKSEL PORTA
    BSF     PORTA,5
    CALL    rs232Write
    CALL    ledOff
    goto    mainstart

doStore:
    ; Setup address lines
    CALL    i2cWaitIdle
    CALL    i2cStartWait
    MOVLW   0x4e
    CALL    i2cSendWait
    MOVIW   -3[FSR0]
    CALL    i2cSendWait
    CALL    i2cStopWait
    MOVIW   -2[FSR0]
    MOVWF   memaddr
    CALL    pulseMem
    ; Set data pins to write
    BANKSEL TRISC
    CLRF    TRISC
    ; Write
    MOVIW   -1[FSR0]
    BANKSEL PORTC
    MOVWF   PORTC
    BANKSEL PORTA
    BCF     PORTA,4
    BANKSEL PORTA
    BSF     PORTA,4
    ; Tristate pins
    MOVLW   0xff
    BANKSEL TRISC
    MOVWF   TRISC
    CALL    delay150us
    ; Finished
    MOVLW   '.'
    CALL    rs232Write
    CALL    ledOff
    goto    mainstart


ledOff:
    CALL    i2cWaitIdle
    CALL    i2cStartWait
    MOVLW   0x4e
    CALL    i2cSendWait
;    BANKSEL SSP1CON2
;    BTFSC   SSP1CON2,6
;    GOTO    $
    MOVLW   0xff
    CALL    i2cSendWait
    CALL    i2cStopWait
    return


pulseMem:
    ;BCF     PORTA,0
    ;BTFSC   memaddr,5
    ;BSF     PORTA,0
    BANKSEL PORTA
    ; Shift in Bit 5
    BTFSC   memaddr,5
    GOTO    $+3
    BCF     PORTA,0
    GOTO    $+2
    BSF     PORTA,0
    BSF     PORTA,1
    BCF     PORTA,1
    ; Shift in Bit 4
    BTFSC   memaddr,4
    GOTO    $+3
    BCF     PORTA,0
    GOTO    $+2
    BSF     PORTA,0
    BANKSEL PORTA
    BSF     PORTA,1
    BCF     PORTA,1
    ; Shift in Bit 3
    BTFSC   memaddr,3
    GOTO    $+3
    BCF     PORTA,0
    GOTO    $+2
    BSF     PORTA,0
    BANKSEL PORTA
    BSF     PORTA,1
    BCF     PORTA,1
    ; Shift in Bit 2
    BTFSC   memaddr,2
    GOTO    $+3
    BCF     PORTA,0
    GOTO    $+2
    BSF     PORTA,0
    BANKSEL PORTA
    BSF     PORTA,1
    BCF     PORTA,1
    ; Shift in Bit 1
    BTFSC   memaddr,1
    GOTO    $+3
    BCF     PORTA,0
    GOTO    $+2
    BSF     PORTA,0
    BANKSEL PORTA
    BSF     PORTA,1
    BCF     PORTA,1
    ; Shift in Bit 0
    BTFSC   memaddr,0
    GOTO    $+3
    BCF     PORTA,0
    GOTO    $+2
    BSF     PORTA,0
    BANKSEL PORTA
    BSF     PORTA,1
    BCF     PORTA,1
    ; Toggle Load
    BSF     PORTA,2
    BCF     PORTA,2
    RETURN


delay150us:
    MOVLW   0xff
    SUBLW   75      ; 75 cycles -> 150us
    BANKSEL TMR0
    MOVWF   TMR0
    BCF     INTCON,2
    BANKSEL OPTION_REG
    BCF     OPTION_REG,5
    BANKSEL INTCON
    BTFSS   INTCON,2
    GOTO    $-1
    BCF     INTCON,2
    BSF     OPTION_REG,5
    RETURN


timertest:
    BANKSEL OPTION_REG
    BCF     OPTION_REG,5
timerloop:
    BANKSEL PORTC
    MOVLW   0xFF
    MOVWF   PORTC
    BANKSEL INTCON
    BTFSS   INTCON,2
    GOTO    $-1
    BCF     INTCON,2
    ;BANKSEL PORTA
    ;BSF     PORTA,0
    ;CALL    pulse
    BANKSEL PORTC
    MOVLW   0x00
    MOVWF   PORTC
    BANKSEL INTCON
    BTFSS   INTCON,2
    GOTO    $-1
    BCF     INTCON,2
    ;BANKSEL PORTA
    ;BCF     PORTA,0
    ;CALL    pulse
    GOTO    timerloop

    END
