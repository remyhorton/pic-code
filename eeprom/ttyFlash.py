#!/usr/bin/env python3
## ttyFlash.py - Atmel EEPROM programmer control
## (c) Remy Horton, 2021
##
## SPDX-License-Identifier: BSD-3-Clause

import serial,sys

class FlashError(BaseException):
    def __init__(self,msg=None):
        if msg:
            self.msg = msg
        else:
            self.msg = "Communications error"
    def __str__(self):
        return self.msg


def printUsage(tty):
    print("USAGE: ")
    print("   {0} <tty> flash <filename>".format(sys.argv[0]))
    print("   {0} <tty> dump  <filename> [page]".format(sys.argv[0]))
    print("   {0} <tty> load  <addr>".format(sys.argv[0]))
    print("   {0} <tty> store <addr> <data>".format(sys.argv[0]))

def funcFlash(tty):
    if len(sys.argv) != 4:
        printUsage(tty)
        return
    if sys.argv[3] == '-':
        strInput = sys.stdin.read()
    else:
        with open(sys.argv[3],'rt') as fpInput:
            strInput = fpInput.read()

    data = {}
    for idxLine,strLine in enumerate(strInput.splitlines()):
        values = strLine.split()
        if values == []:
            continue
        if len(values[0]) != 5 or \
            values[0][4] != ':' or \
            len(values) != 17:
            raise FlashError("Error at line {0} of data".format(idxLine+1))
        addr = int(values[0][:-1],16)
        page = addr >> 6
        line = (addr & 0x3f)
        if (addr & 0x0f) != 0:
            raise FlashError("Bad address on line {0}".format(idxLine+1))
        bites = bytearray( [int(i,16) for i in values[1:]] )
        if page not in data:
            data[page] = { line : bites }
        else:
            if line in data[page]:
                raise FlashError(
                    "Duplicated address at line {0}".format(idxLine+1)
                    )
            data[page][line] = bites
    for idPage in data:
        try:
            page = data[idPage]
            bites = page[0]
            bites.extend(page[16])
            bites.extend(page[32])
            bites.extend(page[48])
            data[idPage] = bites
        except KeyError:
            raise FlashError(
                "Data page starting 0x{0:04x} is incomplete".format(idPage<<6)
                )
    for idPage in data:
        print("Flashing page {0}..".format(idPage))
        ttyWrite(tty,data[idPage])
        ttyFlash(tty,idPage << 6)
    print("..done")

def funcDump(tty):
    if len(sys.argv) < 4 or len(sys.argv) > 5:
        printUsage(tty)
        return
    if len(sys.argv) == 4:
        page = None
    elif len(sys.argv) == 5:
        try:
            if sys.argv[4] == 'all':
                page = None
            else:
                page = int(sys.argv[4],16)
        except ValueError:
            raise FlashError('Invalid page number')
    if sys.argv[3] == '-':
        fpOut = sys.stdout
    else:
        fpOut = open(sys.argv[3],'wt')

    if page is None:
        for page in range(0,127):
            ttyDump(tty,page)
            payload = ttyRead(tty)
            printDump(payload,page,fpOut)
    else:
        ttyDump(tty,page)
        payload = ttyRead(tty)
        printDump(payload,page,fpOut)

    if sys.argv[3] == '-':
        fpOut.close()

def printDump(payload,page,fp=sys.stdout):
    for addr,bite in enumerate(payload):
        if addr % 16 == 0:
            if addr != 0:
                fp.write('\n')
            fp.write("{0:04x}: ".format( (page<<6) | addr ))
        fp.write("{0:02x} ".format(bite))
    fp.write('\n')

def ttyRead(tty):
    tty.write(b'R')
    ack = tty.read(1)
    if ack != b'r':
        raise FlashError()
    payload = tty.read(64)
    if len(payload) != 64:
        raise FlashError('Did not get entire EEPROM page')
    return payload

def ttyWrite(tty,data):
    tty.write(b'W')
    ack = tty.read(1)
    if ack != b'w':
        raise FlashError()
    lenWritten = tty.write(data)
    if lenWritten != 64:
        raise FlashError('Did not write entoreEEPROM page')

def ttyDump(tty,addr):
    tty.write(b'D')
    ack = tty.read(1)
    if ack != b'd':
        raise FlashError()
    tty.write( bytearray([addr]) )
    ack = tty.read(1)
    if ack != b'.':
        raise FlashError('Error dumping EEPROM')

def ttyFlash(tty,addr):
    tty.write(b'F')
    ack = tty.read(1)
    if ack != b'f':
        raise FlashError()
    tty.write( bytearray([addr]) )
    ack = tty.read(1)
    if ack != b'.':
        raise FlashError('Error flashing EEPROM')

def funcLoad(tty):
    if len(sys.argv) != 4:
        printUsage(tty)
        return
    try:
        addr = int(sys.argv[3],16)
    except ValueError:
        raise FlashError('Invalid address')
    page = addr >> 6
    line = addr & 0x3f
    tty.write(b'L')
    ack = tty.read(1)
    if ack != b'l':
        raise FlashError()
    print(">>",page,line)
    tty.write( bytearray([page,line]) )
    bite = tty.read(1)
    print("Value at {0:04x}: {1:04x}".format(addr,ord(bite)))

def funcStore(tty):
    if len(sys.argv) != 5:
        printUsage(tty)
        return
    try:
        addr = int(sys.argv[3],16)
    except ValueError:
        raise FlashError('Invalid address {0}'.format(sys.argv[3]))
    try:
        data = int(sys.argv[4],16)
    except ValueError:
        raise FlashError('Invalid data value {0}'.format(sys.argv[4]))
    page = addr >> 6
    line = addr & 0x3f
    tty.write(b'S')
    ack = tty.read(1)
    if ack != b's':
        raise FlashError()
    tty.write( bytearray([page,line,data]) )
    ack = tty.read(1)
    if ack != b'.':
        raise FlashError('Write failed')
    print("Written {0:04x} to {1:04x}".format(data,addr))


listModes = {
    'flash' : funcFlash,
    'dump'  : funcDump,
    'load'  : funcLoad,
    'store' : funcStore,
    }

if __name__ == '__main__':
    if len(sys.argv) < 3:
        printUsage(None)
        sys.exit(1)
    try:
        tty = serial.Serial(port=sys.argv[1],baudrate=57600)
        listModes.get(sys.argv[2],printUsage)(tty)
        tty.close()
    except serial.serialutil.SerialException as ex:
        print(ex)
    except FlashError as err:
        print("Error: {0}".format(err.msg))
        tty.close()
    except FileNotFoundError as err:
        print("Error: {0}".format(err))
    except IsADirectoryError as err:
        print("Error: {0}".format(err))

