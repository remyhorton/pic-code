;   setup.asm - Initialisation code
;   Copyright (C) Remy Horton, 2021.
;   SPDX-License-Identifier: BSD-3-Clause

setupClock:
    BANKSEL OSCCON
    MOVLW   b'01100010'     ; 2MHz
;    MOVLW   b'01101010'     ; 4MHz
;    MOVLW   b'01011000'     ; 1MHz
;    MOVLW   b'01010010'    ; 500kHz
    MOVWF   OSCCON
    RETURN

setupPins:
    BANKSEL APFCON0
    BCF     APFCON0,2    ; RB7 -> TxD
    BCF     APFCON0,7    ; RB5 -> RxD
    BSF     PORTA,4      ; Make sure EEPROM control pins are
    BSF     PORTA,5      ; both set to high (deasserted).
    MOVLW   b'00001000'
    BANKSEL TRISA
    MOVWF   TRISA
    MOVLW   b'01110000'  ; TxD=0; others=1
    BANKSEL TRISB
    MOVWF   TRISB
    MOVLW   0xff
    BANKSEL TRISC        ; Data pins initially tristated
    MOVWF   TRISC
    BANKSEL ANSELA       ; All pins to digital mode
    CLRF    ANSELA
    BANKSEL ANSELB
    CLRF    ANSELB
    BANKSEL ANSELC
    CLRF    ANSELC
    RETURN

setupRS232:
    BANKSEL BAUDCON
    BSF     BAUDCON,3   ; BRG16=1
    MOVLW   8           ; BAUD=57k6
    CLRF    SPBRGH
    MOVWF   SPBRGL
    BCF     TXSTA,4     ; SYNC=0
    BSF     TXSTA,2     ; BRGH=1
    BSF     TXSTA,5     ; TXEN=1
    BSF     RCSTA,4     ; CREN=1
    BSF     RCSTA,7     ; SPEN=1
    RETURN

setupI2C:
    MOVLW   0x04        ; 100kHz (2MHz clock)
    BANKSEL SSP1ADD
    MOVWF   SSP1ADD
    MOVLW   b'00101000' ; I2C master
    BANKSEL SSP1CON1
    MOVWF   SSP1CON1
    CLRF    SSP1CON2
    RETURN
