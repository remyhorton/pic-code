#!/usr/bin/env python3

import thermocouples_reference
typeK = thermocouples_reference.thermocouples['K']

voltdelta = 7.8125 / 1000
#voltdelta = 15.625 / 1000
#voltdelta = 31.25 / 1000
#voltdelta = 62.5 / 1000


print(
''';   volttable.asm - Lookup table for reference temperature conversion
;   Copyright (C) Remy Horton, 2020.
;   SPDX-License-Identifier: BSD-3-Clause
''')

print("lookupRefVolts:")
print("    BRW        ; mV    Celcius")
for idx in range(0,0xff+1):
    millis = idx << 1
    theta = millis / 10
    voltage = (typeK.emf_mVC(theta,Tref=0))
    deltas = round(voltage / voltdelta)

    if deltas < 256:
        print("    RETLW {0:<3}  ; {1:<5} {2:.4f}".format(deltas,theta,voltage))
    elif deltas == 256:
        print("    RETLW 255  ; Saturation")
    else:
        print("    RETLW 255")
 
