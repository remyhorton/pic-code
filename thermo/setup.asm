;   setup.asm - Initialisation code
;   Copyright (C) Remy Horton, 2020.
;   SPDX-License-Identifier: BSD-3-Clause

setupClock:
    BANKSEL OSCCON
    MOVLW   b'01101000' ; 4MHz
    MOVWF   OSCCON
    RETURN

setupPins:
    BANKSEL APFCON
    BSF     APFCON,2
    BSF     APFCON,7
    MOVLW   b'00100111'
    BANKSEL TRISA
    MOVWF   TRISA
    MOVLW   b'00000001'
    BANKSEL ANSELA
    MOVWF   ANSELA
    RETURN

setupADC:
    BANKSEL ADCON1
    ;MOVLW   b'01010011' ; xxxxxxxx:xx000000
    MOVLW   b'11010011' ; 000000xx:xxxxxxxx
    MOVWF   ADCON1
    MOVLW   b'00000001'
    MOVWF   ADCON0
    ; Fixed-voltage reference
    BANKSEL FVRCON
    MOVLW   b'10000001'
    MOVWF   FVRCON

setupRS232:
    BANKSEL BAUDCON     ; All I2C register on same bank
    BSF     BAUDCON,3   ; BRG16=1
    MOVLW   103         ; BAUD=9600 (4M/4*(103+1))
    CLRF    SPBRGH
    MOVWF   SPBRGL
    BCF     TXSTA,4     ; SYNC=0
    BSF     TXSTA,2     ; BRGH1
    BSF     TXSTA,5     ; TXEN=1
    BSF     RCSTA,4     ; CREN=1
    BSF     RCSTA,7     ; SPEN=1
;    MOVLW   'R'
;    MOVWF   TXREG
    RETURN

setupI2C:
    BANKSEL SSP1ADD
    MOVLW   0x09
    MOVWF   SSP1ADD
    MOVLW   b'00101000'
    MOVWF   SSP1CON1
    CLRF    SSP1CON2
    RETURN

setupTimers:
    ; Timer1
    BANKSEL T1CON
    MOVLW   b'00110101'
    MOVWF   T1CON
    RETURN
    ; Timer2 (used for timing)
    MOVLW   b'01111111'
    BANKSEL T2CON
    MOVWF   T2CON
    MOVLW   0xff
    BANKSEL PR2
    MOVWF   PR2
    RETURN

setupInterrupts:
    BANKSEL INTCON
;    BSF PIE1,0  ; TMR1
    BSF INTCON,6 ; PEIE
    BSF INTCON,7 ; GIE
    RETURN
