;   timer.asm - LCD timer (main code)
;   Copyright (C) Remy Horton, 2020.
;   SPDX-License-Identifier: BSD-3-Clause


PROCESSOR 12f1840
#include <p12f1840.inc>
    RADIX dec
    ; Configuration directives
    ; https://gputils.sourceforge.io/code-examples/configurations.html
    ; https://gputils.sourceforge.io/html-help/PIC16F1828-conf.html
    CONFIG FOSC=INTOSC, WDTE=OFF
    CONFIG MCLRE=ON
    CONFIG LVP=OFF

; Data memory (all variables in shared area)

BANK1DATA       UDATA   0x20
remain          RES     2
accum           RES     2
thresh          RES     2
stage           RES     1


SHAREDDATA      UDATA   0x70
refvolts        RES     1
couple1         RES     2
couple2         RES     2
couple3         RES     2
couple4         RES     2
count           RES     1
value           RES     1
flags           RES     1
temp            RES     1




; Entrypoints
STARTUP     CODE    0x0000
    GOTO    entry
;INTERRUPT   CODE    0x0004
;    GOTO    interruptTic


; Sub-modules
include "setup.asm"
include "i2c.asm"
include "rs232.asm"

entry:
    CALL    setupClock
    CALL    setupTimers
    CALL    setupPins
    CALL    setupRS232
    CALL    setupI2C
    CALL    setupADC
    CLRF    FSR0H
    CLRF    FSR0L

    MOVLW   'O'
    CALL    rs232Write
    MOVLW   'K'
    CALL    rs232Write
    CALL    rs232WriteNL

mainloop:
    ; 500ms delay
    ;MOVLW   'M'
    ;CALL    rs232Write
    MOVLW   1
    BANKSEL count
    MOVWF   count
    BANKSEL PIR1
    BTFSS   PIR1,0
    GOTO    $-1
    BCF     PIR1,0
    DECFSZ  count,1
    GOTO    $-5
    ; Get reference voltage
    CALL    getRefTemp
    CALL    lookupRefVolts
    BANKSEL refvolts
;        MOVLW   0
    MOVWF   refvolts



    ; Set Couple Voltage ptr
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ; Read thermocouple 1
    MOVLW   couple1
    MOVWF   FSR0L
    MOVLW   0
    CALL    getCoupleVoltage
    ;    MOVIW   1[FSR0]
    ;    CALL    rs232WriteHex
    ;    MOVIW   0[FSR0]
    ;    CALL    rs232WriteHex
    ;    CALL    rs232WriteNL
    ;    GOTO    mainloop
    MOVFW   refvolts
    CALL    addToIndirect

GOTO skip
    ; Read thermocouple 2
    MOVLW   couple2
    MOVWF   FSR0L
    MOVLW   1
    CALL    getCoupleVoltage
    MOVFW   refvolts
    CALL    addToIndirect
    ; Read thermocouple 3
    MOVLW   couple3
    MOVWF   FSR0L
    MOVLW   2
    CALL    getCoupleVoltage
    MOVFW   refvolts
    CALL    addToIndirect
    ; Read thermocouple 4
    MOVLW   couple4
    MOVWF   FSR0L
    MOVLW   3
    CALL    getCoupleVoltage
    MOVFW   refvolts
    CALL    addToIndirect
skip:

    ;;;;;;;;;;;;;;;;;;;;;;;;;;


;    MOVLW   couple1
;    MOVWF   FSR0L
;    MOVIW   1[FSR0]
;    CALL    rs232PrintHex
;    MOVIW   0[FSR0]
;    CALL    rs232PrintHex
;    CALL    rs232WriteNL

    MOVLW   couple1
    CALL    writeDegreesHex
    MOVLW   couple2
    CALL    writeDegreesHex
    MOVLW   couple3
    CALL    writeDegreesHex
    MOVLW   couple4
    CALL    writeDegreesHex
    CALL    rs232WriteNL
    GOTO    mainloop

    
;    MOVIW   0[FSR1]
;    CALL    rs232WriteDec
;    MOVLW   ' '
;    CALL	rs232Write
;    MOVIW   1[FSR1]
;    CALL    rs232WriteDec
;    CALL    rs232WriteNL
;    GOTO    mainloop


    


addToIndirect:
    ADDWF   INDF0,1
    BTFSS   STATUS,0
    RETURN
    MOVIW   1[FSR0]
    ADDLW   1
    MOVWI   1[FSR0]
    RETURN

getRefTemp:
    BANKSEL ADCON0      ; ADCONx/ADRESx all Bank 1
    BSF     ADCON0,1    ; Trigger one-shot
    BTFSC   ADCON0,1    ; Wait for sample
    GOTO    $-1
    BCF     STATUS,0    ; Put LSB of upper byte into C
    BTFSC   ADRESH,0
    BSF     STATUS,0
    RRF     ADRESL,0    ; Shift it into lower byte
    RETURN

getCoupleVoltage:
;        MOVLW   15
;        MOVWI   0[FSR0]
;        MOVLW   2
;        MOVWI   1[FSR0]
;        RETURN
    MOVWF   flags
    SWAPF   flags,1
    LSLF    flags,1
    ; Trigger ADC read
;    MOVLW   'S'
;    CALL    rs232Write
    CALL    i2cWaitIdle
    CALL    i2cStartWait
    MOVLW   0xd0
    CALL    i2cSendWait
    ;; NAK check
    BANKSEL SSP1CON2
    BTFSC   SSP1CON2,6
    GOTO    $
    ;;
    MOVFW   flags
    IORLW   b'10001011'
    CALL    i2cSendWait
;    MOVLW   'A'
;    CALL    rs232Write
    CALL    i2cStopWait
    NOP
poll:
;    MOVLW   'P'
;    CALL    rs232Write
    CALL    i2cWaitIdle
    CALL    i2cStartWait
    MOVLW   0xd1
    CALL    i2cSendWait
    CALL    i2cReadByte
    MOVWI   1[FSR0]
    CALL    i2cSendACK
    CALL    i2cReadByte
    MOVWI   0[FSR0]
    CALL    i2cSendACK
    CALL    i2cReadByte
    MOVWF   flags
    CALL    i2cSendNAK
    CALL    i2cStopWait
    BTFSC   flags,7
    GOTO    poll
    RETURN
    MOVLW   'P'
    CALL    rs232Write

lookupDegrees:
    ; Sanity check
    MOVIW   1[FSR0]
    SUBLW   15
    BTFSC   STATUS,0
    GOTO    $+6
    MOVLW   HIGH valError
    MOVWF   FSR1H
    MOVLW   LOW valError
    MOVWF   FSR1L
    RETURN
    ; Load index offset
    MOVIW   0[FSR0]
    MOVWF   FSR1L
    BCF     FSR1L,0     ; Don't want LSB
    MOVIW   1[FSR0]
    MOVWF   FSR1H
    ; Add offset to table
    MOVLW   LOW tableDegrees
    ADDWF   FSR1L,1
    BTFSC   STATUS,0
    INCF    FSR1H,1
    MOVLW   HIGH    tableDegrees
    ADDWF   FSR1H,1
        MOVIW   0[FSR1]
        MOVIW   1[FSR1]
    RETURN

writeDegreesHex:
    MOVWF   FSR0L
    CALL    lookupDegrees
    MOVIW   1[FSR1]
    CALL    rs232WriteHex
    MOVIW   0[FSR1]
    CALL    rs232WriteHex
    MOVLW   ' '
    CALL    rs232Write
    RETURN

writeDegreesDec:
    MOVWF   FSR0L
    CALL    lookupDegrees
    BANKSEL remain
    MOVIW   0[FSR1]
    MOVWF   remain
    MOVIW   1[FSR1]
    MOVWF   remain+1
    CLRF    count
    CLRF    flags
    MOVLW   1
    MOVWF   stage
setthresh:
    ; 10000s
    BTFSS   stage,0
    GOTO    $+6
    MOVLW   0x27
    MOVWF   thresh+1
    MOVLW   0x10
    MOVWF   thresh
    GOTO    subtractloop
    ; 1000s
    BTFSS   stage,1
    GOTO    $+6
    MOVLW   0x03
    MOVWF   thresh+1
    MOVLW   0xe8
    MOVWF   thresh
    GOTO    subtractloop
    ; 100s
    BTFSS   stage,2
    GOTO    $+6
    MOVLW   0
    MOVWF   thresh+1
    MOVLW   0x64
    MOVWF   thresh
    GOTO    subtractloop
    ; 10s
    BTFSS   stage,3
    GOTO    $+6
    MOVLW   0
    MOVWF   thresh+1
    MOVLW   0x0a
    MOVWF   thresh
    GOTO    subtractloop
    ; units
    BTFSS   stage,4
    GOTO    $+6
    MOVLW   0
    MOVWF   thresh+1
    MOVLW   1
    MOVWF   thresh
    GOTO    subtractloop
    ; Number fully printed
    MOVLW   ' '
    CALL    rs232Write
    RETURN
subtractloop:
    ; Copy to temp
    MOVFW   remain
    MOVWF   accum
    MOVFW   remain+1
    MOVWF   accum+1
    ; subtract thresh
    MOVFW   thresh
    SUBWF   accum,1
    BTFSC   STATUS,0
    GOTO    noborrow
    ; Borrow from bigh byte
    MOVLW   1
    SUBWF   accum+1,1
    BTFSS   STATUS,0
    GOTO    done
noborrow:
    MOVFW   thresh+1
    SUBWF   accum+1,1
    BTFSS   STATUS,0
    GOTO    done
    ; Still some remainder
    INCF    count,1
    MOVFW   accum
    MOVWF   remain
    MOVFW   accum+1
    MOVWF   remain+1
    GOTO    subtractloop
done:
    ; remainder less than thresh
    BTFSC   flags,0
    GOTO    prefix
    MOVLW   0
    ADDWF   count,0
    BTFSC   STATUS,2
    GOTO    noprint
prefix:
    BSF     flags,0
    MOVLW   '0'
    ADDWF   count,0
    CLRF    count
    CALL    rs232Write
noprint:
    BANKSEL stage
    LSLF    stage,1
    GOTO    setthresh


include "volttable.asm"
include "thetatable.asm"

    END
