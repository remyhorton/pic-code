#!/usr/bin/env python3

import thermocouples_reference
typeK = thermocouples_reference.thermocouples['K']

voltdelta = 7.8125 / 1000


print(
''';   thetatable.asm - Lookup table for volts-to-degrees
;   Copyright (C) Remy Horton, 2020.
;   SPDX-License-Identifier: BSD-3-Clause
''')

print("tableDegrees:")
for deltas in range(0,0x7ff+105,0x2):
    idx = deltas >> 1
    volts = deltas * voltdelta
    theta = typeK.inverse_CmV(volts,Tref=0)
    tempC = round(theta * 10)

    hi = tempC >> 8
    lo = tempC & 0xff

    print("    RETLW {0:<3} ; {1:.4f}mV  {2:.2f}C".format(lo,volts,theta))
    print("    RETLW",hi)
print("valError:")
print("    RETLW 0xff")
print("    RETLW 0xff")

