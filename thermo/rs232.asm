;   rs232.asm - Serial interface routines
;   Copyright (C) Remy Horton, 2019,2020.
;   SPDX-License-Identifier: BSD-3-Clause

rs232WriteHex:
    BANKSEL temp
    MOVWF   temp
    SWAPF   temp,1
    MOVLW   0x0f
    ANDWF   temp,0
    CALL    rs232WriteNibble
    BANKSEL temp
    SWAPF   temp,1
    MOVLW   0x0f
    ANDWF   temp,0
    CALL    rs232WriteNibble
    RETURN

rs232WriteNibble:
    BANKSEL TXSTA
    BTFSS   TXSTA,1
    GOTO $-1
    CALL    lookupHexChar
    BANKSEL TXREG
    MOVWF   TXREG
    RETURN

lookupHexChar:
    BRW
    RETLW   '0'
    RETLW   '1'
    RETLW   '2'
    RETLW   '3'
    RETLW   '4'
    RETLW   '5'
    RETLW   '6'
    RETLW   '7'
    RETLW   '8'
    RETLW   '9'
    RETLW   'a'
    RETLW   'b'
    RETLW   'c'
    RETLW   'd'
    RETLW   'e'
    RETLW   'f'

rs232WriteNL
    MOVLW   '\n'
    CALL    rs232Write
    MOVLW   '\r'
    CALL    rs232Write
    RETURN

rs232Write
    BANKSEL TXSTA
    BTFSS   TXSTA,1
    GOTO $-1
    BANKSEL TXREG
    MOVWF   TXREG
    RETURN

rs232WriteDec:
    BANKSEL value
    MOVWF   value
    CLRF    count
    CLRF    flags
; hundreds
    MOVLW   100
    SUBWF   value,0
    BANKSEL STATUS
    BTFSS   STATUS,0
    GOTO    $+5
    BANKSEL value
    MOVWF   value
    INCF    count,1
    GOTO    $-8
; hundreds done
    BANKSEL count
    MOVFW   count
    ANDLW   0xff
    BTFSC   STATUS,2    
    GOTO    $+4
    BSF     flags,0
    ADDLW   '0'
    CALL    rs232Write 
    BANKSEL count
    CLRF    count
; tens
    MOVLW   10
    SUBWF   value,0
    BANKSEL STATUS
    BTFSS   STATUS,0
    GOTO    $+5
    BANKSEL value
    MOVWF   value
    INCF    count,1
    GOTO    $-8
; tens done
    BANKSEL count
    MOVFW   count
    BTFSC   flags,0 ; Skip check if there was 100's digit
    GOTO    $+4     ;
    ANDLW   0xff
    BTFSC   STATUS,2
    GOTO    $+3
    ADDLW   '0'
    CALL    rs232Write
; units
    BANKSEL value
    MOVFW   value
    ADDLW   '0'
    CALL    rs232Write
    MOVLW   ' '
    CALL    rs232Write
    RETURN

