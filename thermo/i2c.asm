;   i2c.asm - I2C interface routines
;   Copyright (C) Remy Horton, 2019,2020.
;   SPDX-License-Identifier: BSD-3-Clause

i2cWaitIdle:
    BANKSEL SSP1CON2
    BTFSC   SSP1CON2,0
    GOTO    i2cWaitIdle
    BTFSC   SSP1CON2,1
    GOTO    i2cWaitIdle
    BTFSC   SSP1CON2,2
    GOTO    i2cWaitIdle
    BTFSC   SSP1CON2,3
    GOTO    i2cWaitIdle
    BTFSC   SSP1CON2,4
    GOTO    i2cWaitIdle
    BTFSC   SSP1STAT,2
    GOTO    i2cWaitIdle
    ;;MOVLB   0
    RETURN

i2cWaitPending:
    NOP
    BANKSEL PIR1
    BTFSS   PIR1,3
    GOTO    i2cWaitPending
    ;;MOVLB   0
    RETURN

i2cStartWait:
    BANKSEL PIR1
    BCF     PIR1,3
    BANKSEL SSP1CON2
    BSF     SSP1CON2,0
    GOTO    i2cWaitPending

i2cRestartWait:
    BANKSEL PIR1
    BCF     PIR1,3
    BANKSEL SSP1CON2
    BSF     SSP1CON2,1
    GOTO    i2cWaitPending

i2cSendWait:
    BANKSEL PIR1
    BCF     PIR1,3
    BANKSEL SSP1CON2
    BCF     SSP1CON2,6
    MOVWF   SSP1BUF
    GOTO    i2cWaitPending

i2cStopWait:
    BANKSEL PIR1
    BCF     PIR1,3
    BANKSEL SSP1CON2
    BSF     SSP1CON2,2
    GOTO    i2cWaitPending

i2cReadByte:
    BANKSEL PIR1
    BCF     PIR1,3
    CALL    i2cWaitIdle
    BANKSEL SSP1CON2
    BSF     SSP1CON2,3
    CALL    i2cWaitPending
    BANKSEL SSP1BUF
    MOVFW   SSP1BUF
    RETURN

i2cSendACK:
    BANKSEL SSP1CON2
    BCF     SSP1CON2,5
    BSF     SSP1CON2,4
    CALL    i2cWaitIdle
    RETURN

i2cSendNAK:
    BANKSEL SSP1CON2
    BSF     SSP1CON2,5
    BSF     SSP1CON2,4
    CALL    i2cWaitIdle
    RETURN

