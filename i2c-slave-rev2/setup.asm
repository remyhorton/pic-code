;   setup.asm - Initialisation code
;   Copyright (C) Remy Horton, 2020.
;   SPDX-License-Identifier: BSD-3-Clause

setupClock:
#ifdef F1454
    MOVLW   b'11111100'
    BANKSEL OSCCON
    MOVWF   OSCCON
    BANKSEL OSCSTAT
    BTFSS   OSCSTAT,0
    GOTO    $-1
    BTFSS   OSCSTAT,4
    GOTO    $-1
    BTFSS   OSCSTAT,6
    GOTO    $-1
#else
    BANKSEL OSCCON
    MOVLW   b'01100010'     ; 2MHz
;    MOVLW   b'01101010'     ; 4MHz
;    MOVLW   b'01011000'     ; 1MHz
;    MOVLW   b'01010010'    ; 500kHz
    MOVWF   OSCCON
#endif
    RETURN

setupPins:
    BANKSEL APFCON
    BCF     APFCON,2    ; RC4 -> TxD
    BCF     APFCON,7    ; RC5 -> RxD
;    MOVLW   b'00101011'
    BANKSEL TRISC
    BCF     TRISC,3
    BCF     TRISC,4
;    MOVWF   TRISC
;   CLRF    TRISC
    BANKSEL ANSELA
    CLRF    ANSELA
    BANKSEL ANSELC
    CLRF    ANSELC
    RETURN


setupRS232:
    BANKSEL BAUDCON
    BSF     BAUDCON,3   ; BRG16=1
#ifdef F1454
    MOVLW   32
#else
    MOVLW   51
;;    MOVLW   8           ; BAUD=57k6 (Datasheet p284)
#endif
    CLRF    SPBRGH
    MOVWF   SPBRGL
    BCF     TXSTA,4     ; SYNC=0
    BSF     TXSTA,2     ; BRGH=1
    BSF     TXSTA,5     ; TXEN=1
    BSF     RCSTA,4     ; CREN=1
    BSF     RCSTA,7     ; SPEN=1
    RETURN

setupI2C:
    MOVLW   0x20
    BANKSEL SSP1ADD
    MOVWF   SSP1ADD

    BSF     SSP1STAT,7  ; Disable slew
    BCF     SSP1STAT,6  ; Disable SMBus
    MOVLW   b'00000110' ; 7-bit I2C Slave
    MOVWF   SSP1CON1
    BSF     SSP1CON2,0  ; Enable clock stretching
    MOVLW   b'01000011' ; Int on Stop, addr-hold, data-hold
    MOVWF   SSP1CON3
    BSF     SSP1CON1,5  ; Enable
    RETURN

