;   relay.asm - Power relay control
;   Copyright (C) Remy Horton, 2020.
;   SPDX-License-Identifier: BSD-3-Clause

#ifdef F1454
PROCESSOR 16f1454
#include <p16f1454.inc>
#else
PROCESSOR 16f1823
#include <p16f1823.inc>
#endif
    RADIX dec
#ifdef F1454
    CONFIG PLLEN=DISABLED
    CONFIG CPUDIV=CLKDIV6
    CONFIG USBLSCLK=48MHz
    CONFIG PLLMULT=3x
;    CONFIG PLLEN=ENABLED
#endif
    CONFIG FOSC=INTOSC
    CONFIG WDTE=OFF
    CONFIG MCLRE=ON
    CONFIG LVP=OFF
    CONFIG PWRTE=OFF, CP=OFF
    CONFIG BOREN=OFF
    CONFIG CLKOUTEN=OFF
    CONFIG IESO=OFF
    CONFIG FCMEN=OFF
    CONFIG WRT=OFF
    CONFIG STVREN=ON, BORV=LO


BANK0DATA       UDATA   0x20
buffer          RES     80

SHAREDDATA      UDATA   0x70
temp            RES     1
byte            RES     1
flags           RES     1   ; 0:isRead 

; Entrypoints
STARTUP     CODE    0x0000
    GOTO    entry
    GOTO    $

#include "i2c.asm"
#include "setup.asm"
#include "rs232.asm"

entry:
    CALL    setupClock
    CALL    setupPins
    CALL    setupRS232
    CALL    setupI2C
begin:
    CALL    rs232Flush
    CALL    rs232WriteNL
    MOVLW   'O'
    CALL    rs232Write
    MOVLW   'k'
    CALL    rs232Write
    CALL    rs232WriteNL
    CALL    rs232WriteNL
    BANKSEL PORTC
    BCF     PORTC,3
    CLRF    flags
resetBuffer:
    MOVLW   0x20
    MOVWF   FSR0H
    CLRF    FSR0L
mainloop:
    BANKSEL PORTC
    BCF     PORTC,3
    BANKSEL PIR1
    BTFSC   PIR1,3
    GOTO    procI2C
    ; If incoming RS232 data, echo it
    BANKSEL PIR1
    BTFSS   PIR1,5
    GOTO    mainloop
    BCF     PIR1,5
    BANKSEL RCREG
    MOVFW   RCREG
    CALL    rs232Write
    GOTO    mainloop

procI2C:
    BANKSEL PORTC
    BSF     PORTC,3
    ; Clear interrupt flag
    BANKSEL PIR1
    BCF     PIR1,3
    BANKSEL SSP1CON3
    BTFSC   SSP1STAT,4
    GOTO    procStop
    BTFSC   SSP1CON3,7
    GOTO    procAckTime
    BTFSC   flags,0
    GOTO    procRead
    ; All writes are handled within the AckTime routines so SSPBUF
    ; has already been read and BF cleared. All that is left to do
    ; is release the clock line..
    BANKSEL SSP1CON1
    BSF     SSP1CON1,4  ; Release clock
    GOTO    mainloop

procAckTime:
    BTFSS   SSP1STAT,5
    GOTO    procAckAddr
    GOTO    procAckData

procAckAddr:
    MOVFW   SSP1BUF
    ; Note read/write
    BCF     flags,0
    BTFSC   SSP1STAT,2
    BSF     flags,0
;    BSF     SSP1CON2,5  ; Nack
    BCF     SSP1CON2,5  ; Ack
    BSF     SSP1CON1,4  ; Release clock
    MOVWF   byte
    MOVLW   'A'
    CALL    rs232Write
    MOVLW   ':'
    CALL    rs232Write
    MOVFW   byte
    CALL    rs232WriteHex
    CALL    rs232WriteNL
    GOTO    resetBuffer

procAckData:
    MOVFW   SSP1BUF
    MOVWF   byte
    MOVLW   'D'
    CALL    rs232Write
    MOVLW   ':'
    CALL    rs232Write
    MOVFW   byte
    MOVWI   FSR0++
    CALL    rs232WriteHex
    CALL    rs232WriteNL
    BANKSEL SSP1CON1
    BCF     SSP1CON2,5  ; Ack
    MOVLW   0xff
    SUBWF   byte,0
    BTFSC   STATUS,0
    BSF     SSP1CON2,5  ; NACK if incoming is 0xff
    BSF     SSP1CON1,4  ; Release clock
    GOTO    mainloop

procRead:
    MOVLW   'R'
    CALL    rs232Write
    MOVLW   ':'
    CALL    rs232Write
    MOVLW   'a'
    BTFSC   SSP1CON2,6
    MOVLW   'n'
    CALL    rs232Write
    CALL    rs232WriteNL
    ; If master NACK'd the last byte no more should be sent.
    ; In practice detection of stop bit happens first.
    BTFSC   SSP1CON2,6
    GOTO    mainloop
    ; Send next byte
    BANKSEL SSP1BUF
    MOVFW   SSP1BUF
    MOVIW   FSR0++
    MOVWF   SSP1BUF
    BSF     SSP1CON1,4  ; Release clock
    GOTO    mainloop

procWrite:
    MOVFW   SSP1BUF
    CALL    rs232WriteHex
    MOVLW   'D'
    CALL    rs232Write
    CALL    rs232WriteNL
    BANKSEL SSP1CON1
    BCF     SSP1CON2,5  ; Ack
    BSF     SSP1CON1,4  ; Release clock
    GOTO    mainloop

procStop:
    MOVLW   'P'
    CALL    rs232Write
    CALL    rs232WriteNL
    GOTO    mainloop

    END
