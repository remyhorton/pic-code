#define NO_BIT_DEFINES
#include "pic16f630.h"

#define NUM_BUTTONS 6

__code short __at (_CONFIG) cfg0 =
	_FOSC_INTRCIO &     // RA4 & RA5 are I/O
	_WDT_OFF &          // Disable watchdog timer
	_MCLRE_OFF &        // Pin 4 is input
	_PWRTE_OFF &        // Disable powerup timer
	_CP_OFF & _CPD_OFF; // Disable code & data protect


void main(void)
{
unsigned char buttonSamples[NUM_BUTTONS];
unsigned char buttonOutput = 0;
unsigned char rawPortA;
unsigned char rawPortC;
unsigned char idxButton;

/* Set pins RA0/RA1/RA2 set to digital I/O */
CMCON |= 0b111;

/* Upper pins input, lower pins output */
TRISA=0b111000;
TRISC=0b111000;

/* Software-changable chip options
   0x80   Disables pull-up on PORTA
   0b100  TMR0 prescale of 1:32 */
OPTION_REG = 0b10000100;

for(idxButton=0; idxButton<NUM_BUTTONS; idxButton++)
    buttonSamples[idxButton] = 1;

while(1)
    {
    TMR0 = 0;
    INTCON &= ~0x04;
    while( ! (INTCON & 0x04) );

    /* Shift existing samples down one, losing MSB */
    for(idxButton=0; idxButton<NUM_BUTTONS; idxButton++)
        buttonSamples[idxButton] <<= 1;

    rawPortA = PORTA;
    rawPortC = PORTC;

    /* Add current port state to samples */
    if( rawPortA & 0b100000 )
        buttonSamples[0] |= 0x01;
    if( rawPortA & 0b010000 )
        buttonSamples[1] |= 0x01;
    if( rawPortA & 0b001000 )
        buttonSamples[2] |= 0x01;
    if( rawPortC & 0b100000 )
        buttonSamples[3] |= 0x01;
    if( rawPortC & 0b010000 )
        buttonSamples[4] |= 0x01;
    if( rawPortC & 0b001000 )
        buttonSamples[5] |= 0x01;

    /* For pins with all samples the same, change state */
    if( buttonSamples[0] == 0xff )
        buttonOutput |= 0x01;
    else if( buttonSamples[0] == 0 )
        buttonOutput &= ~0x01;
    if( buttonSamples[1] == 0xff )
        buttonOutput |= 0x02;
    else if( buttonSamples[1] == 0 )
        buttonOutput &= ~0x02;
    if( buttonSamples[2] == 0xff )
        buttonOutput |= 0x04;
    else if( buttonSamples[2] == 0 )
        buttonOutput &= ~0x04;
    if( buttonSamples[3] == 0xff )
        buttonOutput |= 0x08;
    else if( buttonSamples[3] == 0 )
        buttonOutput &= ~0x08;
    if( buttonSamples[4] == 0xff )
        buttonOutput |= 0x10;
    else if( buttonSamples[4] == 0 )
        buttonOutput &= ~0x10;
    if( buttonSamples[5] == 0xff )
        buttonOutput |= 0x20;
    else if( buttonSamples[5] == 0 )
        buttonOutput &= ~0x20;

    /* Write new pin state. */
    PORTA &= 0b11111000 | (buttonOutput & 0b111);
    PORTA |= buttonOutput & 0b111;
    PORTC &= 0b11111000 | (buttonOutput >> 3);
    PORTC |= buttonOutput >> 3;
    }
}
