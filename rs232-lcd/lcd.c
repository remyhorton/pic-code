/*
    rs232-lcd: RS232-driven LCD display

    (C) Remy Horton, 2017-2018
    SPDX-License-Identifier: BSD-3-Clause
*/
#define NO_BIT_DEFINES
#include "pic16f88.h"

__code short __at (_CONFIG1) cfg0 =
	_FOSC_INTOSCIO          // Internal oscillator & no clock pins
    & _WDT_OFF              // Disable watchdog
    & _PWRTE_OFF            // Disable power-up timer
    & _CP_OFF & _CPD_OFF    // Disable copy-protect
    & _MCLR_OFF & _LVP_OFF; // Disable MCLR and Low-power programming


void clockSetup(void)
{
OSCCON = 0b01011000; // 2MHz main oscillator
while( ! (OSCCON & 0b100) );
}

void timerSetup(void)
{
// Timer0
OPTION_REG = 0b00000111;
}

void lcdDelayMS(void)
{
// Supposedly 1.6ms
INTCON &= ~0b100000;
TMR0=253;
INTCON &= ~0b100;
INTCON |= 0b100000;
while(!(INTCON & 0b100));
INTCON &= ~0b100000;
}

unsigned char iTic;
void lcdDelay(void)
{
__asm
MOVLW 6 // 6=46ms 3=38ms
MOVWF _iTic
tic:
    NOP
    DECFSZ _iTic,1
    GOTO    tic
__endasm;
}

void pinSetup(void)
{
TRISA = 0b00100000;
TRISB = 0b11001111;
//RB2 overridden by Rx
ANSEL=0;
}

void rs232Setup(void)
{
TXSTA = 0b00100100;
RCSTA = 0b10010000;
SPBRG = 12; // 9600 BUAD
}

void lcdBangNibble(unsigned char bits)
{
PORTA=0b10000000 | bits;
lcdDelayMS();
PORTA=0b00000000 | bits;
lcdDelayMS();
}

void lcdSetup(void)
{
// Make sure device in 8-bit mode
lcdBangNibble(0b0011);
lcdBangNibble(0b0011);
lcdBangNibble(0b0011);

// Set to 4-bit
lcdBangNibble(0b0010);
lcdBangNibble(0b0010);
lcdBangNibble(0b1000);

// Display on
lcdBangNibble(0b0000);
lcdBangNibble(0b1100);

// Clear
lcdBangNibble(0b0000);
lcdBangNibble(0b0001);

// Entry mode
lcdBangNibble(0b0000);
lcdBangNibble(0b0110);
}

void lcdWrite4(unsigned char letter)
{
unsigned char lo = letter & 0b1111;
unsigned char hi = letter >> 4;
PORTA=0b11000000 | hi;
lcdDelay();
PORTA=0b01000000 | hi;
lcdDelay();
PORTA=0b11000000 | lo;
lcdDelay();
PORTA=0b01000000 | lo;
lcdDelay();
}

void rs232Write(const unsigned char bite)
{
TXREG = bite;
while(! (PIR1 & 0b00010000));
}


void main(void)
{
unsigned char iByte;
unsigned char iButtons;
unsigned char iPresses;
unsigned char iPrevButtons = 0;

clockSetup();
pinSetup();
rs232Setup();
timerSetup();
lcdSetup();

lcdWrite4('R'); lcdWrite4('e'); lcdWrite4('m'); lcdWrite4('y');

iPrevButtons =  PORTB & 0b11001000;
iPrevButtons |= PORTA & 0b00100000;
while(1)
    {
    iButtons  = PORTB & 0b11001000;
    iButtons |= PORTA & 0b00100000;
    if( iButtons != iPrevButtons)
        {
        iPresses = 0;
        iPresses |= (iButtons & 8 )  ? 0b0001 : 0;
        iPresses |= (iButtons & 64)  ? 0b0010 : 0;
        iPresses |= (iButtons & 128) ? 0b0100 : 0;
        iPresses |= (iButtons & 32)  ? 0b1000 : 0;

        rs232Write('\n');
        rs232Write('\r');
        rs232Write('B');
        rs232Write(':');
        rs232Write( '0' + iPresses);
        iPrevButtons = iButtons;
        }

    if( PIR1 & 0b100000 )
        {
        iByte = RCREG;
        PIR1 &= ~0b100000;
        if( iByte == '\r' )
            {
            lcdBangNibble(0b0000);
            lcdBangNibble(0b0010);
            continue;
            }
        if( iByte == '$' )
            {
            lcdBangNibble(0b0000);
            lcdBangNibble(0b0001);
            continue;
            }
        if( iByte == '�' )
            {
            lcdBangNibble(0b1100);
            lcdBangNibble(0b0000);
            continue;
            }
        lcdWrite4(iByte);
        }
    }
}

void intr(void) __interrupt
{
}
