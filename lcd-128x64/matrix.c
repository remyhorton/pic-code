/*
   LCD 128x64 dot-matrix display
   Copyright (C) Remy Horton, 2018.
   SPDX-License-Identifier: BSD-3-Clause
*/

#define NO_BIT_DEFINES
#include "pic16f1828.h"

/* Configuration word 1 (word 2 uses defaults)
   https://gputils.sourceforge.io/html-help/PIC16F1828-conf.html
 */
__code short __at (_CONFIG1) cfg0 =
	_FOSC_INTOSC
    & _WDTE_OFF  // No watchdog timer
    & _MCLRE_ON  // Reset button enabled
    & _PWRTE_OFF // No power-up timer
    & _CP_OFF & _CPD_OFF; // No code/data protection


void clockSetup(void)
{
//OSCCON = 0b0 1011 010; // 1MHz
//OSCCON = 0b0 1100 010; // 2MHz
OSCCON = 0b01101010; // 4MHz
//OSCCON = 0b0 1110 010; // 8MHz
//OSCCON = 0b0 1111 010; // 16MHz
}


/* Pin mappings:
        R/W RA0
        Rst RA1
        R/S RA2
        CS1 RA4
        CS2 RA5
        E   RB7
*/
void pinSetup(void)
{
TRISA = 0b00001000;
TRISB = 0b01010000;
TRISC = 0b00000000;
ANSELA=0;
ANSELB=0;
ANSELC=0;
}


/***************************************************************************
 * LCD control functions
 ***************************************************************************/

void pulseEnable(void)
{
__asm
    // At 16MHz, instruction time is 250nS.
    BSF _PORTB,7
    NOP
    BCF PORTB,7
__endasm;
}

void lcdSetup(void)
{
// Reset
PORTA = 0b00000000;
PORTB &= ~0x80;

// Enable chip 1
PORTA = 0b100010;
PORTC = 0b00111111;
pulseEnable();

// Enable chip 2
PORTA = 0b010010;
PORTC = 0b00111111;
pulseEnable();
}


void lcdSetChip1(void)
{
PORTA &= 0b001111;
PORTA |= 0b100000;
}


void lcdSetChip2(void)
{
PORTA &= 0b001111;
PORTA |= 0b010000;
}

void lcdSetOffset(unsigned char idxCol)
{
PORTA &= 0b111010; // Clear RS & RW
PORTC = 0b01100000 | idxCol;
pulseEnable();
}


void lcdSetCol(unsigned char idxCol)
{
PORTA &= 0b111010; // Clear RS & RW
PORTC = 0b01000000 | idxCol;
pulseEnable();
}


void lcdSetRow(unsigned char idxPage)
{
PORTA &= 0b111010; // Clear RS & RW
PORTC = 0b10111000 | idxPage;
pulseEnable();

}


void lcdWrite(unsigned char pixels)
{
PORTA &= 0b111110; // Clear RW
PORTA |= 0b000110; // Set RS
PORTC = pixels;
pulseEnable();
}


void lcdFill(unsigned char pattern)
{
unsigned char idxCol;
unsigned char idxRow;

lcdSetChip1();
for(idxRow=0;idxRow<8;idxRow++)
    {
    lcdSetRow(idxRow);
    lcdSetCol(0);
    for(idxCol=0;idxCol<64;idxCol++)
        lcdWrite(pattern);
    }
lcdSetChip2();
for(idxRow=0;idxRow<8;idxRow++)
    {
    lcdSetRow(idxRow);
    lcdSetCol(0);
    for(idxCol=0;idxCol<64;idxCol++)
        lcdWrite(pattern);
    }
}


unsigned char iCol;
unsigned char iRow;

void lcdClear(void)
{
unsigned char idxCol;
unsigned char idxRow;

lcdSetChip1();
for(idxRow=0;idxRow<8;idxRow++)
    {
    lcdSetRow(idxRow);
    // lcdSetCol(0);
    PORTA &= 0b111110; // Clear RW
    PORTA |= 0b000110; // Set RS
    PORTC = 0;
    for(idxCol=0;idxCol<64;idxCol++)
        pulseEnable();
    }

lcdSetChip2();
for(idxRow=0;idxRow<8;idxRow++)
    {
    PORTA &= 0b11111010;
    PORTC = 0b10111000 | idxRow;
    __asm
        CALL _pulseEnable
        BSF PORTA,2 // Set RS
        CLRF PORTC  // Zero Port c
        MOVLW 64    // Countdown loop
        MOVWF _iCol
        clearloop:
            BSF PORTB,7
            NOP
            BCF PORTB,7
            NOP
            DECFSZ _iCol,1
            GOTO clearloop
    __endasm;
    }
}


/***************************************************************************
 * I2C functions
 ***************************************************************************/

void i2cSetupSlave(void)
{
SSP1CON1 = 0b00100110;
SSP1CON2 = 0b1;  // Enable clock stretching
SSP1CON3 = 0b11; // Enable address-hold & data-hold
SSP1ADD = 0x02;
}

void i2cSlaveWrite(unsigned char bite)
{
SSP1CON &= ~0x80;
SSP1BUF = bite;
while(SSP1CON & 0x80)
    {
    // Write collision
    SSP1CON &= ~0x80;
    SSP1BUF = bite;
    }
}


/***************************************************************************
 * Entrypoint
 ***************************************************************************/

void main(void)
{
unsigned char i2cByte;
unsigned char i2cAddr[2];
unsigned char i2cAddrCnt = 0;

pinSetup();
i2cSetupSlave();

//PORTB = 0;

lcdSetup();
lcdClear();

lcdSetChip1();
lcdSetCol(60);
lcdSetRow(6);
lcdWrite(255);
lcdWrite(255);
lcdWrite(255);
lcdWrite(255);
lcdWrite(255);
lcdWrite(255);
lcdWrite(255);
lcdWrite(255);

while(1)
    {
    if( PIR1 & 0x08 )
        {
        if(SSP1CON & 0x40)
            {
            /* Receive overflow */
            i2cByte = SSP1BUF;
            SSP1CON1 &= ~0x40;
            PIR1 &= ~0x08;
            SSP1CON1 |= 0b10000;
            continue;
            }

        if(SSP1STAT & 0x04)
            {
            /* I2C Read. Not supported so NACK the address */
            PIR1 &= ~0x08;
            SSP1CON2 |= 0x20;
            SSP1CON1 |= 0x10;
            continue;
            }

        while(SSP1STAT & 0b1)
            {
            /* Incoming I2C write */
            PIR1 &= ~0x08;
            i2cByte = SSP1BUF;
            if(! (SSP1STAT & 0x20))
                {
                /* Device address. */
                i2cAddrCnt = 0;
                SSP1CON2 &= ~0x20;
                }
            else if(i2cAddrCnt == 0)
                {
                /* First register byte */
                i2cAddr[i2cAddrCnt++] = i2cByte;
                SSP1CON2 &= ~0x20;
                }
            else if(i2cAddrCnt == 1)
                {
                /* Second register byte */
                i2cAddr[i2cAddrCnt++] = i2cByte;
                if( i2cAddr[0]==0xff && i2cAddr[1]==0xff )
                    SSP1CON2 &= ~0x20;
                else if( i2cAddr[0] > 7 || i2cAddr[1] > 0x80 )
                    SSP1CON2 |= 0x20;
                else
                    {
                    if( i2cAddr[1] & 0b01000000)
                        {
                        lcdSetChip2();
                        i2cAddr[1] &= 0b00111111;
                        }
                    else
                        lcdSetChip1();
                    lcdSetRow( i2cAddr[0] );
                    lcdSetCol( i2cAddr[1] );
                    SSP1CON2 &= ~0x20;
                    }
                }
            else if( i2cAddr[0] == 0xff && i2cAddr[1] == 0xff )
                {
                /* Special commands (clear/fill/scroll) */
                if(i2cByte == 0)
                    lcdClear();
                else if(i2cByte == 1)
                    {
                    //FIXME: Scroll
                    }
                else
                    lcdSetOffset(i2cByte);
//                    lcdFill(i2cByte);
                SSP1CON2 &= ~0x20;
                }
            else
                {
                /* Pixel data */
                lcdWrite( i2cByte );
                SSP1CON2 &= ~0x20;
                }
            SSP1CON1 |= 0x10;
            }
        }
    }
}

